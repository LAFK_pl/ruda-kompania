# Rozdział II - zlecenie Radagasta - Elendilmir

Opisuje spotkanie w Puszczańskim Dworze, dołączenie Valayi córki Drumby, Herefry z Rohanu i jej druha Ernwara Puszczanina i wspólny wyjazd do Rosgobel, gdzie drużyna przyjmuje zlecenie od Radagasta Burego, by odnaleźć zastępstwo zniszczonej Latarni Baltiego - a także dowiedzieć się od staruszka Vigola co stało się w Smajlach wiele, wiele lat temu. Jest wymordowana krasnoludzka karawana, są upiory, bitwa na polach Gladden, przeprawa przez Anduinę, kamienne trolle, hobbicia norka, księżniczka Arnoru i Gondoru, spalony (przez smoka lub inne monstrum) krasnoludzki ród Rugnisson a całość zamyka turniej zagadek i rozmowa z Radagastem.


## Sesja 1: Puszczański Dwór, kiedy?

Herefra by uporać się z hańbą podejmuje tarczę Malaryka

Ernwar opuszcza Puszczę przewidując ruch Herefry i chcąc samemu odkryć skarby.

Öri i Myrtle ruszają dalej w drogę.

Gundin jest tam aby...?

Valaya jest tam w sprawie Latarni Baltiego, o której zniszczeniu się dowiaduje podczas pierwszej poważniejszej rozmowy.

**Ujawnione zostaje:**

- dla Valayi: rodowa przynależność Gundina! Powiedziano by złodziej ale to słowo krasnoludy zaczerpują z ludzkiego. Po ichniemu, złodziej zbyt jest zbliżony do słowa "smok".
- dla pozostałych: że Gundinowy ród postrzegany jest jako pechowy i on chce to zmienić
- że Öri pozostawił Puszczanom w prezencie włócznie dobrej roboty
- że Myrtle nie może doczekać się pójścia na szlak (może dlatego, że Puszczanie męczą ją o opowieść o Brunhildzie i Nazgulu. 7 razy pod rząd.)
- że Ernwar jest ubogi a jego strój to łata na łacie (a sporo rzeczy możnaby już wymienić, jak by chłopak miał na co)
- że rudy jest popularnym kolorem w rozmaitych odcieniach. Dość rzec, że jedynie Myrtle ma ciemne a Herefra złote włosy.
- że Herefra uważa swą porażkę za wielką hańbę a swą przysięgę rycerza traktuje poważnie oraz że uważa, że zawiodła <tu imię>, pozwalając na triumf Malaryka
- że Gundin i Öri żywią duży szacunek, lękiem podszyty, wobec Valayi
- że Latarnia Baltiego odpędzała Mrok od osiedla z wszelkimi tego konsekwencjami (rozmowa Radagast, Valaya, Ernwar o usunięciu pory roku i wpływie na całość sezonów)
- że Latarnia Baltiego to relikt poprzedniej Ery, wykonana była przez elfiego kowala o wielkich umiejętnościach i wiedzy


Zostaje ustalone, że trzeba Latarnię naprawić albo zastąpić.

Są ogary i nieogary i patrząc na Odiego i Smyka jasne jest, który jest którym.

Radagast zabiera wszystkich do siebie - pewne wątpliwości czy tak liczna ekipa może być ciągnięta przez jednego konika rozwiewa... konik prychając wojowniczo i pogardliwie zarazem.

Ernwara wzrokiem odprowadza matka... czyżby "sekretne" opuszczenie domu nie było aż tak sekretne jak chłopak się spodziewał?


## Sesja 2: u Radagasta w Rosgobel

Drużyna przygotowuje kolację, poznaje Toma Bombadila, wymienia podarki i dowiaduje się że osty z Cirith Ungol nie są dobrym dodatkiem do ciasteczek.


**Ujawnione zostaje:**

* że Tom Bombadil jest skrzatem i ma błękitny kołpaczek i mówi do rymu
* że wiewiórki potrafią przygotować kolację niemal dorównując umiejętnościom krasnoludom i kompanii
* że Ernwar musi nauczyć się więcej o uprzejmościach, by rozumieć pewne zdania - a raczej prawdziwe za nimi intencje, i że jego podarki będą później
* że Myrtle umie dobrze gotować i robi śliczne ciasteczka
* że osty z Cirith Ungol są potencjalnie śmiertelne i głupio jest wmieszać je do jedzenia, bo nawet krasnolud może stracić... brodę
* że Ernwar umie leczyć (oraz że athelas w jego rękach zdawało się działać) - choć jedynie zapobiega utracie brody i minimalizuje to do jej przerzedzenia, zatem ratowanie życia Öriemu może nie być traktowane tak poważnie
* drużynie o co chodzi z Herefrą i tarczą Malaryka i że Herefra uważa się za shańbioną
* że podarkami dla Radagasta są np. ciekawa huba, narąbanie drwa na opał, zadbanie o stworzenia w jego stajn itp.
* że Valaya potrafi dawać podarki, naprawdę potrafi

Sesja zakończyła się na podarkach Valayi.


## Sesja 3, u Radagasta w Rhosbobel, wymarsz i upiory, dzień później i dalej

Poranek u Radagasta, śniadanie i wymarsz, podróżowanie i przygody z nim związane: podarta spódniczka Myrtle, Ernwar biorący na siebie za dużo. A także wymordowana strzałami z zasadzki krasnoludzka karawana, i upiory owąż masakrą przyciągnięte. 

Bitwa na polach Gladden, Isildur (Gundin) i jego synowie oraz ich giermkowie Atkila (Myrtle) i Ethelmo (Ernwar).


**Ujawnione zostaje:**

* że podarki Gundina są praktyczne i mimo krótkiej znajomości - wciąż dobrze dobrane
* że z krasnoludzką matroną nie nalezy się spierać (ujawnia Gundin)
* że Herefra kładzie Ernwara w walce - i to nawet po ciężkim treningu
* GRACZOM: że sprawa Złotej Jagody rozstrojonej perspetkywą opuszczenia Śródziemia sprowadza Toma Bombadila do Radagasta
* że Ernwar tylko by się krzątał po tym obejściu
* że Herefra uwielbia konie i jej nowy rumak ją strasznie cieszy
* że w domu Radagasta czyszczone są sprzączki i buty, zapasy pojawiają się przy koniach, itp. (a Ernwar może coś wiedzieć o tym)
* że ojciec Öriego posłał wojów by go szukali i owi wojowie wymordowani zostali w zasadzce
* że upiory mogą sprawić, że ujrzy się dawną historię i w 'walce' samemu się człek pobije



> 1PC za chęć wymordowania orków (Ernwar)
> 7 PD

## Sesja 4, po starciu, podróż, przeprawa przez Anduinę, TROLLE!

**Po starciu**

1. Ernwar leczy Gundina, Herefra czuwa, Öri przeszukuje miejsce walki
2. Gundin +2 Wt, rana opatrzona, pozostali + Serce (zwykłe) + 2 Wt odzyskują. Ernwar PD bo Wyciąganie Choroby.
3. Próba rozpoznania kurhanu: nie należy do kogoś znacznego, wiele więcej nie wiemy (słabe rzuty Ernwara i Öriego i Valayi).

**Podróż**

1. Podróż: Valaya 11, bez roli, Herefra i Ernwar 6 (poniżej 14)
2. Przewodnik Öri spowalnia grupę a zwiadowczyni Myrtle idzie sprawdzić teren


**Przeprawa**

1. Piękny dąb, puste i bez świateł osiedle: żerdzie rozjechane przez wóz? Myrtle rzuca: trzeba pomóc
2. Ernwar znajduje bród, dostrzega ślady wozu co odjechał z kopyta
3. Gundin ratuje Ernwara przeprawiającego się z liną, chwała Aulemu za krasnoludy
4. Myrtle podkrada się pod okno i dostrzega przedpokój ozdobiony dywanem, pospiesznie zrolowany jakby ktoś się na nim przewrócił albo go wykrzywił, korytarzyk praktycznie pozbawiony ozdób, żyrandoli, 
5. Posrebrzany grzebień, okruchy żółtego sera, korytarzyk prowadzi dalej, krasnoludy idą na przedzie, Myrtle druga, 
6. Wóz nierówno obciążony, odjeżdżał w popłochu
7. Dziadku jesteś tu? - Myrtle Tuk, Gundin zakrywa jej usta
8. Orki? odpowiadają "cicho, ktoś tu jest", ale odpowiada też dziadek, tylko nieco dziwnie "jestem tu, jestem uwięziony"
9. Ernwar dołącza do eksploratorów
10. Zapach padliny, coś dużego miota kamieniami, "wróg u wejścia!" ostrzega Herefra (3 punkty nienawiści na przerzuty MG) i dostaje kamieniem, lina pęka, ale Herefra łapie jej koniec, 13 obrażeń
11. Ernwar pędem do wejścia, głaz spada na norkę, grad odłamków zasypuje Belerianda, `6 obrażeń, pada`
12. Gundin spluwa na ręce, chwyta młot, "nie takie rzeczy się robiło!" i się zaczyna przebijać, Öri czuwa z tyłu by nas nic nie dopadło
13. Ernwar szuka okien - wśród pyłu (punkt Nadziei), punkt Nienawiści i przerzut
14. Kamienny troll - nikt nie będzie tak wołał na Smydla
15. Jeden sus: 25m, ueeee, mokre - Herefra: a widziałeś potwora w wodzie? i biegnie po łuk i strzały
16. Ernwar: Gundinie, Myrtle, Öri,  znalazłem okno! Myrtle: to przechodź! 
17. Gundin potężnym ciosem młota kruszy kamień, cios za ciosem, ale niemal przy okazji rozłupał czaszkę Myrtle `Punkt Cienia`
18. Öri przyjmuje postawę obronną, czeka na stwora
19. Ernwar zwinnie przesmykuje się i widzi sytuację w rzece! `Punkt Cienia: moja wina, jakbym został na czatach u wejścia nic takiego by się nie stało`
20. Ernwar wyciąga Smyka i krzyczy Öri, ale pojawia się potwór, któollry wbija się przez podłogę, Öri chytrze rzuca się do przodu lecz zostaje złapany za stopę i jak lalka, uderza w strop, `10 obrażeń`
21. Gundin okiem kamieniarza, uderzenie z bioder, kamień w drobny mak! Punkt Drużyny.
22. Myrtle próbuje zrzucić chodnik na łeb trolla ale rzut nie jest udany, chodnik ześlizguje się po trollu
23. Valaya rozpala ognisko, trolll ueeee, to nie w porządku, obraca się od ognia
24. Herefra fantastycznie strzela z łuku, 13 obrażeń
25. troll skacze, 13m, punkt Nienawiści, doskoczył, kto teraz jest tępa pała?
26. Öri użyty jako maczuga uderza w Gundina, 6 obr
27. Kolejny troll się przebija spod podłogi
28. Gundin zagrzewa do walki: te głupkowate trolle nie mają z nami szans, nie takie rzeczy się przechodziło, to nic w porównaniu z odnoszeniem latarni Baltiego do Dol Guldur
29. Myrtle raz jeszcze próbuje z kocykiem, troll odgania jak muchę, ale hobbitka turla się pod wymachem, bierze w pędzie dywanik, skacze z nim i zakłada go na głowę trolla po czym zeskakuje obok, -2k6 do następnej tury Myrtle
30. potężny cios Öriego, topór runiczny, wspaniały cios, posoka tryska, troll opada na kolana, 15 obr + 10 obr + rana i zmęczenie (uaaaaa, zrobiłem sobie kuku)
31. Odo szarpie trolla za stopę
32. "zostaw Herefrę w spokoju!" Ernwar strzela, 14 obrażeń, Smyk pilnuje pana
33. odpowiadając na "kto jest tępą pałą?" Herefra zimno cedzi: wciąż Ty! i tnie go pod kolanem! Wirując, znajduje się przed trollem, który pada na ręce. Ostrze miecza, intensywnie błyszcząc, wbija się prosto w oko trolla, który pada martwy!
34. Nasze leże, wynocha, jedzenie - spod podłogi wyskakuje następny troll, potężny cios pięści trafia Myrtle, troll ją chwyta!
35. Gundin: arrrgh! Ty paskudna poczwaro! celuje w rękę, Punkt Drużyny, zraniony troll wypuszcza Myrtle
36. Öri skacze na trolla i zdziela go toporem w łeb, odcinając mu ucho (to było moje ulubione!)
37. Odo wyje bo jest zadowolony, że się udało
38. Myrtle atakuje mieczem, dźgając trolla, zadaje obrażenia, posoka tryska, nawet do ust, takie barbarzyńskie zachowanie nie przystoi hobbitce z rodu Tuk!
39. Ernwar strzela: D12: 10, D6: 6,6, 5,4, strzała prosto w kartoflany pysk, 21 obrażeń i rana
40. Za Smydla za Fydla dostaniesz od Hydla! Troll próbuje uderzyć Myrtle! Punkt Nienawiści, trafia, ale większość impetu poszła w podłogę, której dalszy kawałek rozstrzaskał - 6 obrażeń, Myrtle czuje zmęczenie
41. Gundin: zakańcza walkę powalając ostatniego trolla
42. Kiedy zwłoki leżą u stóp, hobbita w norce nie ma, dokładnie przeszukując miejsce, w jednym z dawno zapomnianych pomieszczeń, pomiędzy jedną a drugą szczotką, zużytymi swetrami a czapkami, znajdujecie trzy dziwne konstrukcje: świece na cynowych podstawkach, jak, jakby samoróbkowe kaganki, ale jedna z nich posiada dodatkowy element, wspaniałej roboty diadem z przejrzystym, białym kamieniem, ewidentnie elfickiej roboty, bardzo, bardzo stary, Ernwarze: masz poczucie jakbyś już to kiedyś widział - i widzieliście to w swojej wizji - miał go na głowie Isildur


>  Od MG: 15 PRozwoju i 9 PD - Ernwar +1
> 1PC, 10PS. 

## Sesja 5, po starciu, Vigol ze Sparerowych Smajli, Adelina Otarin 

Drużyna odnajduje Elendilmir i odkrywa, że ten reaguje na Ernwara. Spotkanie z Vigolem ze Spacerowych Smajli i z Adeliną Otarin z Gondoru i Arnoru.

Diadem z białego złota zwieńczony białym, mlecznym kamieniem.
Ernwar i Öri znajdują. Ernwar odkrywa, że jakby powietrze się elektryzowało. Szkicuje skarb.

Ekipa zbiera się i podziwia. 

To chyba to, możemy wracać? Po co tu przybyliśmy, pyta Gundin.

Öri rozpoznaje kowalstwo Numenoru. Elendilmir, ostatnio miał go Isildur. Kamienia Elfów wieńczącego diadem nie dało się zreplikować nawet królom krasnoludów. Legendy krążą o mocach tego przedmiotu, ale Öri wie, że ma on zdolność działania na światło i widzenia skrytych przed wzrokiem śmiertelnych rzeczy.

światło od Ernwara, Ernwar "skąd to", Gundin wskazuje lustro.
rodzice, od ojca: Esthelmo z bitwy = na jego ramieniu ręka Isildura

>  Zmęczony

Herefra: jestem królową! Ładny profil, ale kamień z powrotem mleczny.

Herefra próbuje nałożyć diadem Öriemu, ten cofa się szybko i nakłada hełm, dopiero wtedy czuje się bezpieczny.

Valaya: widziałaś wiele artefaktów, topór Durina, pierścień krasnoludów (szukałaś go, jako jedna z kilkorga, niestety zawiodłaś), patrząc na diadem: znalazłby się w takim towarzystwie jako całkiem niepośledni.

Valaya wyciąga chustę i zawija koronę w nią. Chowa ją do sakwy mówiąc: ten przedmiot ma ogromną moc i to, że zareagował tak na Ernwara może przysporzyć nam wiele dobra, jak i wiele kłopotów.

Öri słysząc, że przedmiot tak zareagował, rozważa klęknięcie. Bystrooka Myrtle dostrzega, że krasnolud może przykładać inną wagę do Ernwara. Może potrzebować rozmowy albo czynów by ta zmiana została utrwalona. Nie widziała hobbitka dotąd by komuś prócz ojca Öri okazywał taki szacunek. 


Gundin jak zawsze trzeźwy przypomina, że coś jeszcze należałoby zrobić.

Trzeba poszukać dziadka! - nalega Myrtle 

Przede wszystkim to jego własność, a my zachowujemy się jakby należało do nas - mówi Herefra

Krótka rozmowa co dalej i czemu dziadek zostawił diadem i co z nim

Öri niczym wyżeł doprowadza do ukrytego pomieszczenia, za dywanikiem "proszę pukać"

Wrażenie, jakby coś zepsuł, mechanizm działa, drzwi otwierają się

> Öri punkt rozwoju za zagadki

Znajdujemy piernaty, marmoladę agrestową i brakujące sztućce od kompletu, dziecięcą ręką malowana rodzina hobbicka, wujaszek Vigol 

Myrtle: Vigol to stare, hobbickie, dość egzotyczne imię, 

> takie nasze Abraham

Vigol ma brodę! Czy hobbity ją mają?


> 2+Serce Wt jeśli nie ranni. 24-6=18; 18+7=25


Smyk łapie trop. Ten wiedzie do wozu. 

Drużyna decyduje, że rusza rankiem.

Jedna z pięciu spiżarni nie została przejedzona. Gundin znajduje piwo! Myrtle znajduje marmoladę!

Drużyna po wesołej kolacji zasypia. 

Rano ekipa się zbiera, zastanawia się co zrobić z norką, posprzątać, zostawić zapłatę, zabrać rzeczy dziadkowi? Decyzja zostaje złożona na ręce Myrtle. 

Myrtle kompletuje własny komplecik sztućców z dziadkowych

> Punkt Cienia: też takie chcę, złożę sobie z tych 

Ta w końcu zawija w serwetkę rzeczy i chowa jak były, decydując się zabrać obrazek dla dziadka.

Ogary prowadzą, podejmując trop. Podróż: bez większych perypetii, Ernwar jak zwykle z tyłu. 

Wiedzie przez odnogę Anduiny i pola Isildura. Widać nawet w oddali monument po tej bitwie, ogary nawet prowadzą w pobliże! Widać także pod flagą Gondoru oddział, wyróżnia się - może dowódca? - kobieca postać. A kilometr na zachód jest niewielkie obozowisko, pranie na motyce i kosie. 

Solidna niziołcza broda wyziera spod nisko zwieszonego namiotu w odpowiedzi na okrzyknięcie się kompanii.

Poplamiona kamizelka (sosem, żurawiną, błotem), rudo-siwy hobbit, Vigol ze Spacerowych Smajli!

Hobbit ma monokl! :-) 

Öri - rzucając ucho trolla pod nogi hobbita: Nie masz już lokatorów.

Vigol - patrząc przez monokl: to wygląda jak... stary pasztet!

Dopiero jak Öri rozmawia o domu, Myrtle śpiewa piosenkę o powrocie do domu, Herefra wsadza dziadka na konia i zabiera go siłowo to sprawy idą do przodu.

> Myrtle: punkt cienia przy piosence, gdy dziadek ją ignorował i zaśpiewała mu złośliwie 

Gondorska rycerka ma rude włosy, bujne loki

Valaya i Ernwar rozpoznają herb Gondoru na proporcu, szachownicę granatu w polu herbu oznaczającą że ród z Arnoru pochodzi. Złocenia w okręcie na przedzie szachownicy: ród posiada tytuł. 

Lustig, Surnir i Woronc w służbie Otarin z Gondoru, panny Adeliny, córy Eradura, majordomusa zamku na Pelargirze. 

Rozpoznają Öriego jako szlachetnie urodzonego.

Woronc jest w gorącej wodzie kąpany. Lustig go hamuje. 

Panienka nadjeżdża, stary rycerz próbuje dogonić.

Ernwar rzuca: czy będziemy mówić o trollach i wymienia Öriego, Myrtle, Gundina i Herefrę jako zabójców trolli.

Öri: nie ma potrzeby, pokonaliśmy je, ale mogą być inne

Gundin: to tylko trolle!



Adelina Otarin: 15 lat, rude loki, urodziwa, rubinowe kolczyki w uszach, pełna życia, niewielka rubinowa łezka w kolii.

Orszak na wycieczce krajoznawczej.

Öri ponownie robi wrażenie imieniem!

Drużyna ostrzega przed orczą zasadzką

Adelina zaprasza w gości

Vigol mówi o borsukach, żart Adeliny wywołuje śmiech Öriego ale Myrtle Tuk nie takie już słyszała i łapie intencję dziewczyny

Ernwar i Adelina są bardzo podobni, pytanie o to, czy gondorscy rycerze trafili do Puszczy, ród Beleriandu i stracona kraina, jak Arnor

> Myrtle oddając sztućce traci jeden punkt Cienia
>
> 7PD i 10 PR

## Faza Drużyny u Vigola

Kompania jedzie do Vigola wraz z Gondorczykami.

| Skarby od trolli: 10 (jak goblińska horda!) - ujęte wcześniej 10PS

Vigol nas podejmuje, płacimy mu każde (Poziom Społeczny-1) skarbów

Wszyscy nadzieję odnawiają na max.

>  Ernwar do wydania: Skarby: 9 PD: 25 PR: 64

### Gundin

1. PD: 30 + 16 = 46 => wydane 42, zostało 4

2. PR: 39 + 25 = 64 => wydane 58, zostało 6.

3. Punkty skarbu = 9 

4. PW = MAX

**rozwój**: 

młot 2 -> 3 (-6PD) 

młot 3 -> 4 (-10PD) 

odwaga 4 -> 5 (-16PD)  - wezmę keen (ostry) do mojego młota 
mądrość 1 -> 2 (-4PD) - Dark for Dark Business (z Companiona) - na spostrzegawczość w ciemnościach, rzucam dwa razy k12 (noc, podziemia, itp) 
mądrość 2 -> 3 (-6PD) - Technika Durina - +3 do parowania w podziemiach  

**Umiejki:**

czujność (0 -> 1): -4PR  
czujność (1 -> 2): -8PR 
podziw (0 -> 1): -4PR 
podziw (1 -> 2): -8PR 
leczenie (1 -> 2, ulubiona): -6PR 
rzemiosło (3 -> 4): -16PR 
podróżowanie (2 -> 3): -12PR

### Valaya

1. PD: 31+9+7=47 do wydania 16, wydane 16 
2. PR: 40+15+10=65, wydane 57, zostaje 5.
3. Punkty skarbu = 10

**rozwój**, 

Mądrość: +1=4 (koszt 10PD) + Specjalność Zaklęcia Strażnicze [Broken Spells] (jedno zaklęcie: Tajemniczości) 

Odwaga: +0=2 (koszt 0) 

Broń: Topór +1 = 3 (koszt 6PD)

**Umiejki**: 

Podziw 1 ->2 i 2->3  (koszt 8 PR+12 PR) 

Wiedza 2->3 i 3->4 (koszt 9 PR+12PR) 

Kurtuazja 1->2  (koszt 8PR) 

Czujność 1->2 (koszt 8PR) 



### Herefra

1. PD: 37 => wydane , zostało 

2. PR: 59 => wydane , zostało .

3. PS 10 

4. PW = MAX

**Rozwój**:  

Odwaga 4 -> 5, nagroda do miecza Okrutny  

Mądrość 2 -> 3, cnota Żywotność (koszt )

**Herefra** rozwój:  Odkrywanie 1-> 2 (8) Wiedza 1-> 2 (8) Perswazja 0 -> 1 (4) Kurtuazja 1 -> 2 (8) Wojaczka 3 -> 4 (20) Zagadki 0 -> 1 (4)

### Óri

(obecne): PD=50 PR:66 Punkty skarbu= 17

**rozwój** wydawanie PD( 50 do wydania) 

Mądrość +1 =4(koszt 10PD) > otucha 

Mądrość +1 =5(koszt16PD) > wytrenowanie --> podziw 

Topory +1 =5(koszt 16PD) 

zostaje 7PD 

Odo --> wsparcie (1PD) --> Podziw (3cia kość) 
Odo daje mi 2 nadziei

**Óri** wydawanie PR(66 do wydania) Podziw na 4-->12PD Rzemiosło z 3 na 5 -->27PD Odrywanie na 3-->9PD Perswazja na 1 -->4PD Czujność na 3-->12PD Suma 64 na 66 zostaje 2PD

### Myrtle

(52PD) Odwaga i Mądrość na 5, Miecz na 4, Łuk na 3

### Ernwar

1. PD: 7+7+1+9=24PD 
2. PR: 39+15+10=64 
3. PS 10

**Ernwar** wydaje PD: Odwaga +1: 3 (**nie wiem jeszcze co**, koszt 4) Mądrość +1: 4 (Tamujący zaśpiew, Ernwar znajdzie w bagażach zapakowane przez matkę, koszt 6) Ogar +1: Trenuję Smyka by nękał nieprzyjaciół (koszt 2)

**Ernwar** wydaje 64 PR: Odkrywanie na 5: koszt 20 44 Podróżowanie na 2: koszt 4+8=12 32 Śpiew na 3: koszt 12 20 Leczenie na 4 (ulubione): 12 8 Wojaczka na 2: koszt 8 0 = wszystko wydane


### Tunelowanie

1. Herefra gubi się, idąc w prawo, dociera do uskoku, wiedzione przez... rżenie?

2. Gundin rozpala pochodnię, Erwar pomaga

3. Gundin wyczuwa nosem klejnoty

4. Ernwar odpala pochodnie od Gundinowej

5. Gundin odkrywa krasnoludzkie szczątki i ślady żłobień po runach

6. Szyfr podobny do tego Bardinssonów, niebosiężny sufit, jak katedra, ślady po czerwiu co wkopał się jeszcze głębiej, krasnoludzkie i większe szkielety, ułożone na kopcu zrobionym na grzędę, tak by jego środek był wgłębiony, jakby coś na nim leżało przez jakich czas, na zmarłych są zbroje, broń, pierścienie, diademy. Główny postument: bazaltowy blok oświetlony pojedynczą kolumną pomarańczowo-żółtego światła (nie słonecznego). Została złota patera, w obwodzie mniejsza niż przeciętna krasnoludzka tarcza.

7. Ernwar ze Smykiem przyglądają się grzędzie. Gundin wskazuje tunel w ziemi i mówi: czerwie. Ale Ernwar nie rozumie odpowiedzi. Ogląd napawa trwogą i Ernwara ogarnia strach. Ściany tunelu nie to nie te, co przyjmują na siebie wielkie bulwiaste ciało, trudne do rozróżnienia. Nie, podłużne pręgi dziwnie przypominające... ślady skrzydeł, które musiały być potężne by w skale zostawić... może wytopić swoje ślady. 

8. > Ernwar punkt cienia.

9. Sala ma mnóstwo złota na ścianach, podłodze, sufitach. Gundin: zawalmy to przejście by ich pochować. Herefra: rytuał pogrzebowy. Myrtle: zaśpiewajmy. Drużyna śpiewa chórem. 

10. > Chór odejmuje 1 PC: Myrtle, Herefra, Gundin, Ernwar. 

11. Drużyna przechodzi po zmarłych patrząc by ustalić kim byli, znaleźć dobre bronie. Gundin patrzy na to ściskając w rękach topór, Ernwar podchodząc ze znalezioną broszą na której Myrtle dopatrzyła się mapy, pyta co się stało. 

12. Gundin wręcza skarb Ernwarowi mówiąc "warte to więcej niż niejedno królestwo waszych królów" i każe mu przyobiecać, że jak znajdzie potomków rodu Rugnisson, przekaże im rzecz z powrotem. 

13. > Brosza ma formę Gwiazdy Północy, z wyrytą na niej mapą wyspy. Numenoru?  40PS

14. Öri zamyka się w komnacie rzemieślniczej, mimo że najpierw mówił "pójdziemy w tunele" i nawet się odgrażał.

15. Herefra znajduje topór, stylisko z czerwonego drewna, owinięta dokoła egzotyczną, niebieskawą w kolorycie skórą. Głownia topora to dzieło sztuki krasnoludzkiego rzemiosła: ostre kąty żeleźca, coś co byłoby elipsą jeśli powstałoby u ludzi, tu jest wydłużonym rombem. Rozszerzające się w kierunku obu ostrzy zaś zwężające do środka żeleźce przedstawia scenę batalistyczną. Ogień wytrawiony na stylisku wyrzygiwany jest przez wielkiego jaszczura a w lewej dolnej części sceny widać krasnoludzką falangę napierającą z dobytą bronią. To nie ceremonialny topór ale narzędzie wojny, ale ryty są dokładne i niemal słychać bitwę. 

16. > Topór z rytem. 60PS.  

17. Szkic bitwy zyskuje uznanie Gundina: tak, tak, może być.

18. Öri znajduje zastosowanie dla topazu jaki dostał od Valayi.

19. Valaya wykorzystuje okazję by podejść do Öriego i zapytać, czemu po zawaleniu wieży Czarnoksiężnika w Dol Guldur nie wrócił do domu. Öri odpowiada, że ma marzenie, by dom krasnoludów był ich domem znowu. Moria powinna zostać odratowana.

20. Zdenerwowana Valaya: [![img](https://media.discordapp.net/attachments/689040722715738182/693456043941953556/angry_valaya.jpg?width=291&height=300)](https://cdn.discordapp.com/attachments/689040722715738182/693456043941953556/angry_valaya.jpg)  

21. Öri przekonuje Valayę, mówiąc o zjednoczeniu wszystkich krasnoludów. To nie jest prywatne marzenie, lecz szansa dla wszystkich krasnoludzkich plemion.

22. Ernwar pokazuje Herefrze nową taktykę: Smyk jej pomaga nękając, Ernwar wspiera strzałami. Herefrę to rozchmurza. Ernwar uświadamia sobie, że nie wziął pod uwagi jej walki konnej.

23. Öri i Valaya przypatrują się znaleziskom: Dagor Deothrin - Wojna Gniewu, to scena z topora. Brosza faktycznie przedstawia Numenor.

24. Vigol proponuje zagadki

    1. Co do norki się wprosiło 
       z drapaniem borsuka 
       i ciekawsko zagadało 
       że czegoś tam szuka?
    2. Lata bez skrzydeł, 
       opada bez strachu 
       Nie czuć jej smaku ani zapachu 
       Znika bezgłośnie w cieple dłoni 
       Kolejne jej siostry niebo roni
    3. Niby to wielce daleko 
       A na wyciągnięcie dłoni 
       Norko, góro, lesie, rzeko 
       Sekrety twe róża odsłoni
    4. Dziadku, jajko się gotuje 8 minut. W ile minut ugotujesz 4 jajka?
    5. Co jak wiosło w jeziorze przebiera 
       Ale nie za wielkim 
       Innym razem w dłoni bohatera 
       Pokroi w plasterki 
       To znów sieciarza lewicą 
       Nakarmi cię całą ławicą
    6. Słodki niczym miód,
       Na puchu to cud, 
       Omeny z bliska i z dala przynosi 
       Największego siłacza z nóg potrafi skosić.

25. Dziadunio odpowiada, że jajka się smaży i w drugiej próbie i z podpowiedzią by z kuchni wyszedł odgaduje, że sen. Punktacja 2 : 3 dla drużyny, dziadunio wygrywa świecę a drużyna: Elendilmira.

>  12 PD i 16 PR


## Rozmowa z panną Adeliną, po uczcie u Vigola

pachnidło: jakby zioła, ale jest coś subtelniejszego, drzewo sandałowe, cierpki i słodki owoc

jakie jej włosy są w dotyku? delikatniejsze od znanych Ernwarowi tkanin, ale dokładnie jakby się spodziewał

wyścig do konia

szamotanina przy koniu, cokolwiek ona chce, muszę to pokrzyżować

Ernwar osadza konia, Adelina jednak zrywa go do jazdy, ciągnąc Ernwara po ziemi. Przez moment wygląda to groźnie, lecz w końcu poturbowany chłopak jest wciągany (trochę z własną pomocą) na konia. 

**Ujawnione zostaje:**

1. że Ernwar najpewniej wywodzi się - po stronie ojca - od Ethelma, który krewnym był Isildura
2. że ród Otarin, a zatem i Adelina, również od Ethelma się wywodzą - co czyni dwójkę młodych kuzynostwem, co przyjmują z mieszaniną żalu, zaskoczenia i radości
3. że Adelina chce wyruszyć na szlak i zazdrości kompanii wolności - w czym Ernwar obiecuje jej pomóc

Młodzi wymieniają się podarkami.

## Sesja 7, powrót ze Smajli

Valaya rozmawia z Vigolem o jego zaginionym przodku, zaskakując wszystkich, wliczając Vigola pamięcią swoją i Radagasta.  Z Vigola pamięcią gorzej, zatem trzeba było nieźle go podpytać i przebrnąć przez kilka dygresji.

Wreszcie jednak, Ruda Drużyna słyszy o urodzinach Deagola. I wygnaniu innego hobbita: Seagola lub Meagola? Smeagola. Obaj byli dobrymi przyjaciółmi, choć mało lubianymi. W urodziny wyruszyli razem nad Anduinę i nie wrócili... albo raczej wrócił tylko jeden. Nikt nie wie co stało się z drugim, ale dziadunio Vigol przekonany jest, że tam stało się coś niedobrego. Smeagol został wygnany. Przez dwa następne pokolenia działy się w Smajlach złe rzeczy. Martwe ryby na środku korytarzy, mokre ślady stóp, i rzeczy gorsze, o których małym hobbitom nie opowiadało się.

Wypoczęta Kompania opuszcza Spacerowe Smajle, ale nie wcześniej niż Myrtle i Vigol wymienili się dżemem, co u hobbitów jest oznaką niemałej zażyłości.

Wyjaśnia się zagadka zniknięć Öriego. Kuźnia!

Pojawia się zagadka, która nie umyka uwagi Herefry i Valayi. Jeszcze podczas tej jedynej nocy w której Spacerowe Smajle gościły kompanię i orszak Adeliny, w trakcie uczty był moment kiedy brakowało Ernwara i Adeliny, a nazajutrz Adelina ma opaskę Ernwara a Ernwar chustę Adeliny na... kostce i jakiś pierścień na palcu?

Pożegnanie tej dwójki wydawało się... wyjątkowe i coś skrywające. To również nie umknęło oczom trójki rycerzy, którzy chłodno pożegnali się z kompanią, a zwłaszcza z Ernwarem. Rycerz Surnir wymienił SZORSTKI uścisk dłoni z chłopakiem. 

Zaintrygowana i chętna na plotki Herefra postanawia wziąć przyjaciela na spytki!

Po krótkiej rozmowie, kompania decyduje powrócić do Radagasta. Gościńcem, dla odmiany. 

Herefra dopada Ernwara i zaczynając od sójki w bok pyta "jak tam ta dziewczyna", co chłopaka wpędza w niemalże popłoch. Wymiana zdań na początku nie wygląda dobrze dla chłopaka, ale ten spytał: to co za to dostanę?

Stanęło na nauce jazdy (przy okazji wyszło na jaw, że Ernwar jeździł już konno i niemal skręcił kostkę). Ernwar po chwili zastanowienia się, przystaje na to i ujawnia, że Adelina jest jego kuzynką... ale w tym samym momencie Herefra oburza się, że ta propozycja powinna rzucić Ernwara do jej stóp w wyrazie wdzięczności. Ernwar błyskawicznie wykorzystuje okazję, rzuca iż podziwia honor Rohanu i że jest pewien, że Herefra dotrzyma obietnicy, by szybko uciec na polowanie, chwilowo głuchnąc na ścigające go okrzyki "oszust". Herefra zwraca się do pozostałych z pytaniem "co on powiedział?". Valaya okazuje się mieć czułe uszy i wyłapuje nie tylko to, co zostało powiedziane, ale też to, co z tego wynika: że Gondor może chcieć ponownie sięgnąć po rzeczy do niego nie należące i decydować o nich.  Herefra na moment jest ogłuszona rewelacją a nawet ją odrzuca, dopiero spokojne stwierdzenie Valayi, że jeśli Ernwar tak rzekł, to przecież nie kłamał. Dziewczyna skonfrontowana z tym zeskakuje z konia i potrząsa krasnoludką, prosząc by ta na pewno porozmawiała z Ernwarem bo ta zła dziewczyna go wykorzysta. Valaya  - znowu spokojnie - odpowiada, że Ernwar niekoniecznie się da wykorzystać, ale Herefra kwituje to okrzykiem "mówimy o Ernwarze! pewnie mu opowie jakąś rzewną historię i tyle!" Valaya, sama już chcąc rozmówić się z Ernwarem, zgadza się.

Herefra wskakuje na Aroda i odjeżdża by uspokoić emocje, nieświadoma spoczywającego na sobie przenikliwego spojrzenia krasnoludki. Valaya wprawnie czyta w młodej dziewczynie niczym w księdze: Rohanka uważa, że zły świat próbuje zabrać jej przyjaciela. 

Nadarza się okazja do rozmowy między Orim a Valayą. Krasnoludy w krótkich zdaniach uzgadniają, że reakcja Elendilmira jest ważką kwestią, że warto skierować Ernwara na dobrą drogę, jeśli skierowany jest na złą a nade wszystko, że młodzieńcowi potrzebne są lekcje etykiety. Valaya zgodziła się na rolę nauczycielki, stwierdzając, że będzie to dobrą okazją do spędzenia większej ilości czasu z Puszczaninem. 

Polowanie z Myrtle okazuje się bogate w przygody. Strzała wypłasza wiewiórkę z norki, ta poirytowana zaczyna gryźć 'najeźdźcę'. Ernwar oferuje, że można ją upolować, ale hobbitka woli by ładna wiewiórka została tylko przepłoszona. Polowanie na króliki nie kończy się sukcesem, Ernwar ma mętlik w głowie i chyba, Myrtle strzela lepiej, choć też niecelnie. Oboje zgadzają się, że następnym razem. Nauka strzelania natomiast jest owocna, podobnie rozmowa, w której Myrtle uświadamia Ernwara o jakości życia wśród hobbitów, klaruje mu czym jest dżem, gdy pamiętając scenę jaką szkicował pyta jej co to takiego ten dżem. Nawet - poproszona przez Puszczanina - obiecuje, że jak będą owoce, to nauczy go taki robić a podczas kolacji poczęstuje go. 

Przedmiot: kryształ, pięknie obrobiony który jest w samym centrum naramiennika, który jest zrobiony z najznamienitszych metali, tak, by chronić ramię właściciela kiedy ten strzela z łuku. Kształty nie odbiegają zbytnio od zdobień noszonych przez Oriego, widać krasnoludzkie runy wokół kryształu. Szlif z góry na bok. Osadzenie jest celowe, uwypukla kryształ, by bardziej rzucał się w oczy. Runy tchną majestatem, potęgą, ciężarem historii. Ernwar zamiera w szoku, nie wiedząc co powiedzieć. Przymierzywszy cudo, młodzian szybko dostrzega, że nowy naramiennik pozwala strzelać nawet rannemu. 

Myrtle pytaniem "a dla mnie co wykułeś?" wprawia Öriego w zakłopotanie. 

Herefra, widząc reakcję Ernwara okazuje mu miłosierdzie i nie droczy się z nim więcej na temat kuzynki. Tego wieczoru.

U Radagasta - Tom Bombdadil.

Radagast wygląda jakby nie wstawał z krzesła od momentu opuszczenia Rosgobel.

Podczas rozmowy okazuje się, że czarodziej już zna wiele faktów z wyprawy.



Ernwar uśmiecha się, słysząc te słowa. Po dwakroć: raz na wieść, że Radagast będzie czuwał nad bezpieczeństwem Puszczańskiego Dworu, a raz na to, że czarodziej rzekł "Eryn Galen", a nie Mroczna Puszcza.

Moria bezpośrednio? Moria przez Erebor?

**Koniec drugiego rozdziału!**

>  PD 5, PR 7.
