# Rozdział IV: Moria

w którym drużyna wyrusza z Ereboru, trafia do Puszczańskiego Dworu, dzieli się opowieściami, spotyka Thikkonrila i Tharantraska Kurdinssonów, Herulinga i Hobura Bardinssona i szukając - udanie! - wrót do Morii potyka się z Gwathrainem po raz ostatni, by następnie w trzewia obecnie okupowanej stolicy krasnoludów zejść, szukając młodego Kurtiego, topora Durina, Lazurowej Komnaty i prawdy o rodzie Bardinsson.

Tu także drużyna zwiedza Khazad-Dum, ratuje Kurtiego, odkrywa historię upadku krasnoludzkiej stolicy, bierze udział w weselu, odwiedza Mitrylową Kuźnię, zwiedzać próbuje dzielnice Hartowaną, zwiedza dzielnicę Szlachetną, po śladach starych pism idąc łączących rady Oriego oraz Gundina oraz potyka się z orczym wodzem Księżycogryzem w zrabowane krasnoludzkie srebro od stóp do głów przyobleczonym by w końcu przemyślnością krasnoludów z Gór Błękitnych wyniesionymi zostać na przełęcz Kharadras. 


## Sesja 1: podróż do Puszczańskiego Dworu

wątek drzewka hobbiciego i jak Herefra je niemal pocięła w zawziętości i złości

łaska Daina – specjał Durina / Drumila? - przenośna kuźnia

podczas postoju, Gorwin i Korwin, śpiewają pieśń bohaterom
Rozmowa o młocie Gundina, krztusi się Ernwar słysząc o Eru i Aulem
Valaya poznaje kawałek historii
Ernwar ujawnia, kim są Eru i Aule
rozmowa o przodkach i tajemniczości, schodzi na pana Oina
Ori ujawnia co widział! Ernwar: to dlatego ślady… i zamyka się
Ernwarze? Pyta surowo, acz uprzejmie Ori
Ernwar opowiada co wyśledził 
Gundin zły na przodków
Co Hobur (brat) wie tak naprawdę? Czemu prastary młot jest w jego rękach a nie Hobura?

Wątek z Arciriasem
przypałętał się nocą, bo 11tka
torturowany, strzęp człowieka, niemal zabity przez wartowniczkę Herefrę – czuwanie jest ważne, bo inaczej śmierć niewinnym
Arcirias dowodził orkami! 1. Orcze tortury Czarnych Rąk dla dowódców co zawiedli; 2. Orcza buława namiestnicza w rękach
Nie mogłem nad nim zapanować, nie da się nad nim zapanować, to on zabiera, to nie jemu zabierają
Pocieszających słów Ernwara nie słyszy (nie jesteś już sam, my nie byliśmy sami i odepchnęliśmy go) bo umiera
Ślady szabli orczych na ciele, płytkie i długie cięcia, palony, pazur Gwaithrana – rana nie goiła się – policzek, żuchwa, czoło
przybył z PłnWsch, ekipa idzie sprawdzić tropy, dochodzą do parowu, widać ślady pożogi, truchła orcze, niewiele więcej
śladów rytuału przyzwanie Gwaithrana brak

Puszczański Dwór: Ernwar i Herefra w drodze dzielą się opowiescią o wiszeniu w kokonach, nóż w cholewie się przydaje
totemy Radagasta, niektóre ze śladami zębów i potrzaskane i widać, że reperowane
Eratha, Adalryk? I Hildura (zwłaszcza mała Hildura) witają Herefrę radośnie
list Oriego o krasnoludzkie wsparcie dla Puszczan
Ernwar oddaje długi – Herefra mówi „nie odwlekaj jedź do rodziny”, młodzian jedzie
Ernwar pokazuje zza pleców Herefry, że przemalowała tarczę (Eratrze)
Ernwar śpiewa o Herefrze i Kamiennych Trollach, o pokonaniu Legolasa w Sądzie Durina

rozmowa Eratha – Herefra: E. Mówi, że celem wyprawy jest wiedzieć kiedy wrócić, pyta po co to H., H., płacze, że oni jej potrzebują, że popatrz na nich
E.: zatrzymaj się jak będzie Ci dobrze. Wróć, będziemy czekać. Dziewczyny w naszym wieku to już tylko odwiedzanie grobów.

Ernwar odwiedza rodzinę: nakłada matce koronę, moment jak z bajki
rozdaje prezenty wszystkim, śpi w domu, opowiada (ale nie wszystkim wszystko)
ŚWIECI ELENDILMIREM
2jka najstarszych i matka słyszą o pełnej klątwie smoka
rodzino: musicie się rozejść, smok Was znajdzie
w nocy Ernwara budzi sen i łoskot i jakby oddech Gwaithrana

Ermwara nocą budzi łopot skrzydeł Gwathraina. Mlodzian nie czekając popędza rodzinę do Rhosgobel, pod ochronę Rhadagasta Burego.

## Sesja 2: Adelina i Herefra ruszają w pościg!

Adelina i Herefra gnają za Herulingiem i Tharantraskiem. Ci ostro poganiają konie, zatem droga nieprosta jest.

Dochodzi do spotkania i poznają się z Thikkonrilem. Ten ścina się z Adeliną... w obronie Valayi.

Na drużynę spada Gwaithran!

## Sesja 3 (25 VIII) wejście i komnata-ogród

- podróż do Drugiego Stopnia, gdzie zaginął Kurti
- walka z Gwathranem u wrót
- Hobur zamyka wrota - spieszmy się!
- komnata-ogród, rozmowy, medykament Ernwara, posiłek Myrtle
- zagadka otwierająca drzwi, rurki co układają się w kształt topora jednoręcznego i jednoostrzanego

łuki: Adelina, Myrtle, Ernwar
defensywa: Thikkuril, 
straż przednia: Herefra, Ori, Gundin

aura Elendilmira, 2PN by atakować wszystkich
salwa rani ale lekko, Ernwar obrażenia, ale PN i tylko 5
ogień parzy i przeraża (Heruling, Hobur)
ogon w Valayę, zasłania Thikkuril, niemal strącony z urwiska
Valaya biegnie go ratować, w połowie do smoka, gra na harfie pieśń, onieśmiela, utrata 4PNś
Heruling z niedowierzaniem na siostrzenicę, więc jednak to prawda
Heruling Rohan – idzie za przykładem H. - potężny cios, odcięte skrzydło, G. przyziemia!
Gundin kruszy kawał skały by przygrzmocić nim smokowi, płynna lawa wylewa się z paszczy
zamierz się na mnie jeszcze raz tą starą pałą
kiepski dobór słów mości smoku, oj kiepski
Tharantrask rzuca topór i rzuca się na odsiecz bratu
smok praska szponami Myrtle i Ernwara (fatalny rzut :D:D)
zdziera Elendilmira, ale… SMYK APORTUJE!!
Ori: idę po Ciebie i w tors, gdzie widział rękę poprzednio
potężne uderzenie! żałobny dźwięk złamanego dzwonu, 
cielsko traci spoistość, mimo że nie jest w środku kopalni, broczy juchą
Thikkonril miota toporem
Ernwar strzela – potworna rana, drżenie i jedna łapa szponiasta opada
pełne nienawiści spojrzenie smoka
Nie wierzę w przebaczenie
Twoja krew mnie tutaj doprowadziła i ty za to zapłacisz
biczowy ogon trzaska w stronę Oriego, który nie zdąży zareagować
Herefra rzuca się do ataku by zdążyć przed smokiem
Herefra stara się odciąć ogon!
gilotynowy wyrotek pędzi – H. Odcina go, powietrze tylko Ori owiewa
zwykły kundel
zgubiła Cię Twoja pazerność
jeżeli on was… patrzy na Elendilmir
Ernwar kłania się mu z szacunkiem, lecz zimno
zanim siły go opadną jego ostatnie żywe oko i jadowite spojrzenie, na Ernwar
uciekajcie głupcy
Gwathran się boi
nawet kiedy Herefra zadała mu ostatni cios nie czuł lęku, ale teraz go czuje, broczy rubinową lawą krwi
zewłok leży między nami
Ori stoczyłby się w przepaść jakby oberwał
szczelina, kieszeń, nieopodal serca, skóra wężowa jakby niewyprawiony płaszcz
nie widać szkieletu ale wygląda jakby coś było w środku smoka
Ernwar zaczyna szukać, ale Hobur wcześniej już aktywuje mechanizm zamykania się drzwi! szybko, do środka
po co ta walka (Adelina) wiem jak Ci na tym zależy
kolejno odlicz, Ernwar raz, Smyk dwa
Myrtle – kwiatek jak zawilec, gwiazdka o delikatnych płatkach, odpycha mroki kopalni
schody i korytarze, wiele tarasów, poniżej, zanim jesteśmy w stanie zobaczyć ciemność w dole
słyszycie echo, może Waszej walki, może czegoś innego
odległy dźwięk kapiącej wody zniekształcone przez echo
i jeszcze coś, wygląda na to, że ten szyb ukrywa wiele
ale na ten moment nie widzicie więcej smoków, przeciwników, tylko ciemność trzymana na wodzy przez delikatny, świecący kwiat
Myrtle 3, Hobur 4, Gundin 5, Ernwar leczy Myrtle
Hobur podchodzi do Gundina: to Ty? Gundin krzywi twarz, no, to Ty, było trochę gorąco z przodu, a umyłbyś się, kwituje brat

piękna ławka, nadkruszona, donicę, kiedyś coś tu rosło, nitki chodników ciągną się z 4 punktów jak na formę która nie trzyma się na niczym… co byście widzieli
jak na wielkim kawałku skału wiszącej olbrzmiej studni, pośrodku niczego, tylko kolejne pułapy schodów
Thikkonril poleczony, on i Valaya
Herefra i Heruling, cali
Adelina, rozgląda się E. Brawo, A. sam sobie dobrze poradziłeś
Ernwar leczy się sam
czuć tąpnięcie
platformy tańczą! Ruszają się same, poziomy zamieniają się wzajem, schody niczym wielkie żurawie opuszczają dzioby aby znaleźć się w nowej pozycji
Hobur zaczyna liczyć, mażąc coś na glinianej tabliczce, skoro mówi, że chwilę mu to zajmie to zajmie mu to dłużej

krasnoludzkie informacje i wiedza: Valaya i Ori, są na nieskończonych schodach, coś się nie zgadza
czy ten tunel jest krasnoludzki (Gundin): orcza robota, grubo ciosana, jest bezpieczny natomiast wygląda jakby orki nie miały tyle siły i zaparcia co krasnoludowie, tunel musiał być roboty od dawna i przez hordę tego tałatajstwa
Ernwar zajmuje się swoim pancerzem
widać kolczyk – czy to Kurtiego? - Valayo, czy Twój kruk mógłby to zgarnąć? Pewnie tak!

Myrtle gada z Hoburem: w ogóle załóż brodę jak do mnie mówisz
hobbity nie noszą brody
to bardzo chamsko z Waszej strony

Loki wraca z zapinką do brody, z niewielkim rubinkiem – Kurti jak w mordę strzelił
Gundin – czy on zostawia ślady

poraniony smok, potracił moce (tors, ogon, cienistość skrzydeł, trujące wyziewy bo pysk)
my na platformie, wejście – występ skalny, pół
Ernwar pyta krasnale
Ori znajduje uchwyty, ale bez liny ryzykownie; Myrtle znajduje dłuto i młot – kamienną zagadkę, ale dopiero przywołany Gundin identyfikuje to jako kamienną zagadkę
bardziej skomplikowane kamienne zagadki mają prowadnice i mechanizmy, wymagają precyzji, działają na fasadę kamienia konkretną
spokojnie Myrtle, wiem co z tym zrobić. Valayo!
ale popatrz, może to schody…
a Ty Valayo co o tym sądzisz? PT 15, układasz w praktyce, nie w teorii

uchybić nie mam zamiaru, ale czy ranniście?
bitwa to dla mnie nie pierwszyzna, obejdzie się, dzięki
Valaya ostukuje schody wysuwa się trzpień, kolejnym ruchem trzpień zostaje nabity i obrócony – kolejne kamienne płytki rozsuwają się i ażurowa struktura mostu wysuwa się tworząc graniasty łuk
bardziej to szkielet mostu aniżeli most – płytki się spasowały czasem, ale szerokość to pięć piędźwi
wicher świszcze między elementami konstrukcji

dalsze leczenie: Ori, Valaya, samemu

komnata porośnięta roślinnością, ślady zniszczeń, jagoda (świeża)
niewielkie gliniane dzbany, część zachowana, mosiężne drzwi, co zdają się cykać jak świerszcz
Gundin: światło od góry spływa przez kryształ, odbija się od jakiegoś płynu, pachnie fermentacją, czas na kubek i test, solidne krasnoludzkie piwo - choć stare
pokazuje Oriemu – co pokazujesz puste! Nalej! Gundin nalewa
Hobur: cykanie, za dwie godziny się otworzy. Albo za dwa dni. Ale raczej za dwie godziny.
ruchomy element drzwi to zegar – zatrzymany na dziesiątej
krasnoludzki ogród – Ernwar krząta się by przyrządzić leczniczy kordiał

Hobur przez dwie rundy zamykał bramę


10PD i 12 PR
za ubicie smoka: 2 PN

## Sesja 4 (8 IX): wejście do Morii

Mechaniczny świerszcz
antałek piwa
Moria pocięta żłobami z ogniem
Rytuał Ernwara - przyrządzanie kordiału

Róża Wiatrów - Beleghost, Nogrod, Lazurowa Komnata - element w Khazad Dum krasnoludów z Gór Błękitnych

Gundin negocjator zachęca brata Mitrylową Kuźnią, by ten powiedział co wie
Sekrety Bardinssonów opowiedziane przez Hobura Oriemu: Pierwszy krasnolud spoza rodu jaki pozna te prawdy!

1. Czcigodna Ostoja
2. Szlachetna
3. Płycej jest Hartowana
4. Królewska Szychta i grodzie
5. serce kopalni: Miro mir, Zwierciadlane Jezioro z legendy o Durinie

sprzeczka z Myrtle za jej pytanie o to jezioro, że ona nie wie kim jest Durin jaki brak respektu z kim podróżujesz Ori


Myrtle znajduje fajkowe ziele co pachnie podobnie do Hoburowego
    okazuje się, że ten takie Kurtiemu sprzedał

Kolejny trop - Kurti jest ranny!

Ernwar pyta Herefry, gdzie jej zdaniem w Morii siedzą orki. Ta zastanawia się, ale podaje pytanie do Gundina. Jego wiedza o tunelach i sługach Cienia mówi, że stworzenia będą unikać wysokich i przestrzennych hal, ale nie gdzie się osiedliły.

Ori oznajmia, że młody Kurti jest priorytetem misji. Thikonril dziękuje i obiecuje odpłatę!

Ernwar tytułuje Adelinę księżniczką, ta mu się odwzajemnie mówiąc o królewskim tytule i pytając co dalej, co wielce płoszy chłopaka, który uważa, że się nie nadaje. Ori: dobry król nie chce być królem. Ernwar w popłochu zmienia temat.

Ernwar i Ori: to na grób ojca tytuł --> ??

Ernwar spoglądając na drużynników i niespodziewanych sojuszników zastanawia się nad klątwą Gwathrana, obawiając się o losy Herulinga, Thikonrila, Hobura i Adeliny, zachowuje jednak te myśli dla siebie.

## Sesja 4 (? IX): trolle

- podróż, zasadzka, ranny Ernwar
- jak odbić Kurtiego? Planowanie
- atak z obu stron - jak uniknąć rwetesu?
- generał wroga (Ori)
- orczy szaman, bojowe trolle, trolle i inna hałastra
- Smyk rzucony o ścianę!

## Sesja 5 (31 X): Lazurowa Komnata

Bitwy ciąg dalszy!
Myrtle rozcina więzy młodemu krasnoludowi po nieudanej próbie z mydłem. Razem kombinują jak zamek skruszyć czy strzaskać. Złotowłosa Rohirrimka komenderuje:

- Ernwarze! Wykończ tego przy Valayi! Valaya! Zamek! 

Puszczanin chrypiąc "Rohan" wciąż nieswoim gardłem powala jednego z dwu trolli strzałem z Szmeru Cisu. Valayi niewiele brakuje by odemknąć cholerną kłódkę, pęka jej improwizowane narzędzie. Broniący jej przed popalonym trollem bojowym Thikkonril próbuje wrazić mu ostrze w paszczę - śmiertelny cios wykorzystujący, że troll jest obalony. Niestety! Bestia zębami łapie stal, tak mocno, że ta aż jęczy!

Herefra i Gundin biją się z dwoma trolami każde, każde z nich przy tym obrywa! Herefrze w sukurs przychodzi Ernwar dobijając ranioną przez nią maszkarę, Herefra słaniając się na nogach odrzuca tarczę i kładąc wszystko na szalę dobija drugą. Gundin wpierw umiejętnie unika ciosów swoich trolli a wreszcie je wymanewrowuje tak, by same się raziły, co okazuje się spektakularnym sukcesem. Krasnolud zabija oba nie musząc nawet machnąć swym zacnym młotem.

Ori krzyczy "teraz!" do Hobura, ten detonuje ładunek zasypując 6 trolli w korytarzu, ale jednocześnie odcinając drogę pozostałym, co budzi w nich wściekłość zrodzoną z desperacji. Nie mając odwrotu - szarżują! Heruling razi je gradem strzał, by spowolnić ich bieg i sprawić, że obrońcy nie muszą walczyć z wieloma naraz. Tharantrask wdaje się w walkę z nimi, rani jednego lecz sam kilkoma ciosami sprowadzon jest do nieprzytomności. Ori chwyta topór Brunhildy i mocarnym atakiem kładzie jednego trolla i rzuca wysokim łukiem ładunek, by skruszyć stalaktyty by przysypały cztery pozostałe, które cały czas odciągał tez Odo - ogar oraz nieprzytomny Tharantrask na szczęście unikają zasypania.

Adelina rani orczego kapłana, ten pada, lecz wietrząc śmierć zrywa się i roztrąca grupę Oriego biegnąc ku wyjściu! "Odo, goń! Łucznicy, brać go! Hobur, równowartość jego głowy w złocie jeśli go zabijesz!" rzuca Ori w narastającej desperacji - wie, że ucieczka stwora sprowadzi im na kark całe plemię a być może też Księżycogryza!
Pies rzuca się w pogoń, Adelina strzałem na słuch powala sługusa mroku, Heruling dobija... NIESTETY! Przeszywa jedynie czerwoną pelerynę orka! Hobur w pośpiechu wyszukuje... procę! Załadowawszy ją kamieniem wywija z wigorem i kompletnie chybia. Dołącza ostatni łucznik: Ernwar! Wpierw rzuca się do biegu by z innej części jaskini dobiec do miejsca, gdzie strzał będzie możliwy. W biegu szepcze "Elendilmirze, ukaż ukrytego wroga" i strzałę z haczykami i grotem krasnoludzkim dobiera, co na smoka miała być. Wreszcie, z poślizgiem zatrzymując się, strzela, rozdzierając gardziel kreaturze. Puszczanin przyszpila orka jeszcze drugim strzałem, dla pewności.

Drużyna bohaterskim wysiłkiem pokonuje przeciwników, wybijając ich do nogi, by wieści o ich pobycie w Morii się nie rozniosły. Ernwar leczy drużynników i sojuszników, psy i zratowanego - nie dowierzającego swemu szczęściu - Kurtiego, tak zaaferowany, że nie ma czasu dobrze zaleczyć swoich własnych ran. Gundin dopiero, na słowa Herefry, usadza go i leczyć zaczyna. Co zabawne, Beleriand najpierw sądzi, że Gundin jeszcze jaką ranę odkrył i zaleczyć ją przyszedł, dopiero po chwili rozumiejąc jak się sprawy mają. Bardinsson opatruje chłopaka i tamuje krew. Jak tylko opatrunki zostają zmienione, młody dziękuje mu i rusza krzątać się dalej, tym razem maskując ślady, by ci co masakrę odkryją nieprosto mieli w ustaleniu sił i środków za nią stojących. Zbiera też i oddaje właścicielom sprzęt, głównie strzały.

> Leczenie: Gundin -PN, Ernwar -8PN.
> Walka: +PN za sukces.

Ori znajduje list, który wielce go trapi, w sekrecie więc zanosi go Valayi.

Ernwar dopiero podczas nagradzania Smyka uświadamia sobie, że już czas jakiś z Elendilmirem chodzi, robi więc fałszywe ślady by ewentualnych nadnaturalnych tropicieli z tropu zbić. Mówi o tym Oriemu i Herefrze, bardziej tej drugiej bo krasnolud znowuż w naradach z Valayą.

Dalsze przeprawy przez Morię okupione są zmęczeniem, a jedno niespodziane starcie z orkami wielce alarmuje drużynę, gdyż dwa z nich pierzchają mimo ich starań. Wieści zostaną gdzieś poniesione! 

W długim i prostym i pozornie bez końca korytarzu, Ori i Valaya - po kolejnych naradach - zatrzymują drużynę i ujawniają list, od razu mówiąc, że jest on oszczerczy. Dortonda z Hartowanych hańbą tam okrywa Bardiego ze Szlachetnych, bo posterunek opuścił i siedem wozów dziedzictwa wywiózł. Gundin jest wściekły, Hobur tak samo. Ernwar punktuje, że list przecie to tylko słowa Hartowanej i że wg jej słów ona również mogła otworzyć Szychtę.


[Zaślubiny w Khazad-Dum kiedy go Morią jeszcze nazywano](pieśni/Zaślubiny_w_Morii.md)

## Sesja 6 (15 XI): Spełniona przepowiednia i Księżycogryz - FINAŁ

1. co z tą kuźnią? Kujmy! Ernwar: wtedy orki nam na głowy zlecą i misję poddajemy.
2. Myrtle znajduje okruch mitrylu
3. Herefra odkrywa patrole i podłapuje ich liczność i liczebność
4. Ori i Ernwar szkicują Kuźnię, by ją odtworzyć. Ori szczęśliwy i już nie nieszczęśliwy, -PC
5. decyzja gdzie dalej - do dzielnicy Hartowanej, potem do znaków.
6. udaje się do momentu - bitwa, błysk Elendilmira, kłopoty na głowie, trza się przyczaić i godzinę się traci
7. dziwna narośl na podłodze - krwią broczy, zaschłą, wyziewy paskudne jakie
8. Hartowana to legowisko orcze! wycofujemy się!
9. Dzielnica Szlachetna - czujki i przepatrywacze! Czujki zwodzą patrole i kupują czas
10. Przepatrywacze: Bardinsson znajduje wozy ciągnięte mimo braku torów, w których mnóstwo skarbów wciąż siedzi. Beleriand znajduje list niepodpisany, na wyśmienitym pergaminie, świetnie zachowany, widać, że z pietyzmem aż napisany. 
11. Ujawnienie losu "zdrajcy", zwaśnionego z Bardim ze Szlachetnych brata, co syna swego Oriego i 7 wozów dziedzictwa wyprawił. Ujawnienie królobójstwa, walk co rok trwały i zaślepienia króla Naina co nawet Thraina na śmierć skazał rozkazami.
12. Gundin: myśmy są kontent, Ori, możemy wracać.
13. Zdrowy rozsądek wygrywa - wracamy.
14. Zasadzka w drodze - ranna Adelina! Zniszczony mechanizm otwierający drzwi!
15. Ernwar ukazuje rzeczy ukryte, rozwiązuje zagadkę: potrzeba szafy i mocy by ją "zasilić"! Kto pójdzie po szafę?
16. Ernwar i Myrtle i Hobur przekradają się po szafę opodal Mitrylowej Kuźni. Gundin szykuje miejsce by szafę podłączyć miast starego, zniszczonego mechanizmu.
17. Ernwar poświęca Elendilmira! Tyś jedyny stratny - mam kompanów - Valaya by lepiej tego nie powiedziała (ale nie mów jej, że to powiedziałem)
18. Szaleństwo Herefry, zmysły zmącone Cieniem - chce się zemścić za wszystkie zła i krzywdy... na Księżycogryzie! 
19. Walka z orczym wodzem i jego kohortami: strzał Ernwara przeszywa rękę (30 obr i czarna strzała) i wytrąca topór, Herefra podaje go Oriemu, ten się nim broni i rzuca mieszanką (10 obr), Gundin powala wroga na ziemię, Ernwar przeszywa gardło czarną strzałą (40 obr), Herefra przyszpila go mieczami do ziemi - choć jedynie unieruchamiając bo mitrylowy pancerz nie czyni rzeczy prostymi - Ori obcina mu głowę.
20. Ucieczka lazurową windą przed nadciągającym Balrogiem!

Opis krasnoludzkiego szkieletu w źródełku, zasłaniającego topór i pierścień niczym ślubny.
