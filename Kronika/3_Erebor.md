# Rozdział III: Erebor

w którym drużyna debatuje czy do Morii od razu czy przez Erebor, a podjąwszy decyzję do Ereboru się udaje. Tam ściera się z Oinem Groinssonem, krasnoludem o bohaterskiej przeszłości, jednym z Trzynastki Thorina Dębowej Tarczy a ojcem Oriego i zleceniodawcą wyprawy do Morii, osobą hardą i o nie lada brzemieniu. Ori i Myrtle jako bohaterowie wraz z kompanami podjęci są ucztą przez samego Daina II Żelazną Stopę. Na tejże uczcie cała sala klęka przed dwójką bohaterów, Valaya oficjalnie ogłoszona jest rajczynią Daina, zaś losy wyprawy do Morii ma zdecydować Sąd Durina.

## Którędy do celu?

Po otrzymaniu Elendilmira u Radagasta, drużyna decyduje o drodze do Morii. Przez Erebor, czy nie?

Przy większym drzewie, z gościńcem na widoku, nieopodal skrzyżowania.

Öri prosi kompanów o radę: do Ereboru, czy bezpośrednio do Morii? Po krótkiej naradzie, wszystko wskazuje na Erebor.


## Sesja 1: przez Dahl do Ereboru

Kompania wkracza do Dahlu, Öri i Myrtle są witani jak bohaterowie. W Ereborze - domu Öriego - wita ich szereg wojów i Gardak, majordomus Oina, czekający na Öriego. Równo przystrzyżona czupryna, szamerunek majordomusa. Valaya prowadzi część drużyny na zakupy... i lekcje ogłady. Te sprowadzają się przede wszystkim do powściągliwości w okazywaniu emocji.

Stargard (Stormgardu?) - Kamienny Dwór, rynek wyrzeźbiony w żywej skale, klejnoty złapane w skale, numerowane tunele odchodzące odeń w sześć stron, prowadzące doń schody, niesamowicie głębokie, na których można złapać zadyszkę. 

Ernwar zawczasu ustala z kompanią, głównie z Valayą i Örim, czy nosić i ujawniać koronę. Decydują, że należy skarb okazać, lecz nie demonstrować i raczej nie zakładać. W rozmowie z Valayą Ernwar mówi, że jego dziedzictwo nie jest niczym szczególnym (co wywołuje prychnięcie Öriego), lecz bogatszy o doświadczenia i mając w pamięci tak dworskie opowieści jak i zachowanie Öriego, Valayi a jeszcze bardziej Adeliny i Gondorczyków, Ernwar stanowczo mówi, że jest Puszczaninem i jako taki tu jest, niewprost i wprost dając do zrozumienia, że jego strój nie powinien być szlachecki. 

Herefra - respektując odebrane lekcje - prosi Valayę o dobór stroju dla _młodej damy_. Krasnoludka jednak drąży temat i ujawnia, że bycie wojowniczką nie jest niczym złym! Uradowana Herefra dobiera zatem strój pod to, co ma... a przy tej okazji pyta z wyraźnym dyskomfortem, czy to dobry moment, by odkryć tarczę. Nie umyka uwagi Valayi, że największy dyskomfort czuje... sama Herefra. 

Valaya dobiera suknię.

Gundina przymierzanie okazuje się ciekawe: krawiecki terminator pyta, jakim rodem tenże się legitymuje. Nie pomaga nawet gdy Gudin cicho szepcze do czeladnika, że ma wyglądać jak dobry sługa, ani gdy Herefra postuluje, że wojownicza księżniczka potrzebuje wojowniczego księcia... Sytuację ratuje Myrtle, mówiąc, że ten tu krasnolud cuda tworzy z kamienia. 

Öri dostaje bileciki za suknię Herefry (skarb) oraz strój Gundina (gratis).

Valaya i Ernwar płacą za siebie, ona skarb, on sporą sumę, ale niecały skarb.

Rezultat to kawał dobrej roboty, kompania prezentuje się wybornie. 

> 3d6 do zużycia na testy podczas rozmów z Oinem.


Gardak obawia się, że Dain, król krasnoludów, jest o krok od oficjalnego zakazu odzyskiwania Morii.

Wieści z Dol Guldur wróciły, Dain Żelazna Stopa **osobiście** wspomniał imię Öriego (ku dużej satysfakcji samego panicza). Opowieści - w kilku wersjach, bo brakło ich z pierwszej ręki - czekają na zatwierdzenie przez Wielkiego Skrybę.



Oin jest bogatym krasnoludem: 

cyfra 1, solidne wrota, winorośle strzegą murów do majątku Oina, dziedziniec, hol. 

Solidna służba, działa jak dobrze naoliwiona machineria. Znak wielu powtórzeń i ćwiczeń, znak, że Oin czuwa.

Kominek na całą ścianę - istna ściana ognia. Nad tym skrzyżowane topory, plecionki z orczych włosów, tarcze herbowe.

Widać dywan szyty złotą nicią. Dębowy, piękny stół.



Oin: poważna twarz, wykuta ze skały jakby, szafirowe spojrzenie, srebrny grzebień we włosach.

Witajcie w dworze dziedzica Daina, dziedzica pierwszego z krasnoludów, dalekiego kuzyna obecnego króla, nie wspominając już o kuzynie Thorinie. Wejdźcie, goście, witam Was w naszych progach, na naszym dworze. Doświadczcie naszej gościnności. Przynieście - radosne, mamy nadzieję - nowiny.

Oin rozpoznaje Myrtle, zauważa Valayę, lecz - możliwe, że celowo - nie adresuje jej. Zachęca syna by przedstawił kompanię. Öri wskazuje na Valayę, licząc... że ta przedstawi się sama!

- Valaya, córka Badoka i Drumby, z Eredruin. Oin wita ją, ujawniając, że jest doradczynią króla. Podkreśla, że ambicja to dobra cecha, zwłaszcza gdy dobrze wymierzona. Na odpowiedź "mądre słowa", mówi "cieszę się, że pochwalasz me słowa w moich własnych progach". By powstrzymać dalszą potyczkę słowną, syn gospodarza wskazuje na następną osobę: Gundina!
- Na imię mi Gundin panie - mówi tenże, licząc, że z uprzejmości zostanie pominięte pytanie o jego nazwisko. Oin melodyjnie powtarza Gundin, sucho kontynuując - "nie pochodzisz z Ereboru. Mówże, skąd pochodzisz. Może jeszcze kaptur założysz!" Gundin próbuje ratować się, podając, że z Gór Szarych, lecz Oin naciska "dokładniej!", na co krasnolud, schylając głowę, cicho rzecze "z Bardinssonów". To wywołuje aż tik nerwowy na twarzy gospodarza i choć uprzejmość dyktuje chłodne słowa powitania, powtórzeniu "z Bardinssonów" towarzyszy pełne złości spojrzenie ku synowi! Konflikt wzmaga się gdy Valaya ledwo rękę kładąc na ramieniu Gundina wywołuje furię aż u gospodarza. Jednak Ori - wzmocniony tym gestem - nie daje się uciszyć i dokańcza zdanie mimo niemałej presji.
- Herefra, córa tej i tej :-) - słowom towarzyszy wojskowy ukłon. Oin zaskakuje tradycyjnym pozdrowieniem Rohanu: niech moje jadło będzie Twoim jadłem, moje stadniny Twoimi! To pozdrowienie zamierzone na efekt (o czym wie Valaya), lecz uznające szlachetny ród Herefry i pozdrawiające ją jakby to uczynił suzeren do wasala.
- Ernwar, z Beleriandów, z Puszczańskiego Dworu - gest oddania czci gospodarzowi z **iglishmêk** - słyszałem wiele opowieści o Tobie panie, lecz wszystkie bledną wobec tego jakim świadectwem dla Ciebie jest Twój syn Ori, panie - obiecujące słowa wywołuja mile nastrajają Oina Gloinssona lecz sprawa nie kończy się tutaj. Przeszywające spojrzenie Oina natomiast sprawia, że młodzian w panice myśli, że pies coś znowu pogryzł - co niemal okazuje się prawdą. Smyk bowiem wywąchał lokaja, który właśnie wrócił z kuchni. Oin rzuca z pewnym niesmakiem, że gościna ludzi ma swoje wyzwania, na co młodzian kraśnieje, co pogłebia się gdy gospodarz dorzuca, że krew pierwszych ludzi rozrzedła. 

> Ernwar: Stracony PN.


Ujawnione zostaje, że:

1. w Dahl pamiętają kompanię, nawet kwiecie pod nogi sypią. Öri, Myrtle przyciągają spojrzenia. Spojrzenia szukają też pozostałych, Herefra brana za elfkę, Gundin za Öriego. Ogary są głaskane: Smyk poddaje się zabiegom obcych ludzi, a Odo wyniośle je ignoruje.
2. Dahl rozbudowuje się z gruzów, Valaya widzi bazaltowy blok ze swego królestwa, to jej dzieło! Skromna jak zawsze, krasnoludka decyduje nie zwracać na to uwagi kompanii: dowiedzą się w swoim czasie.
3. Oin posłał ludzi w poszukiwaniu syna, wg Gardaka, z niepokoju 
4. Oin jest krasnoludem o którym ciężko rzec czy jest rozgniewany czy niezadowolony - groźnym
5. krasnoludzcy mistrzowie rzemiosła mają inne podejście do klientów, wpierw się rozglądnij, czuj się jak u siebie, dopiero po krótkiej rozmowie przejdźmy do interesów
6. zmartwiony Gardak obawia się o to jak działania sponsora wyprawy, ojca Oina zmienią w relacjach z koroną, (punkt drużyny w teście na intuicję Öriego)
7. że Valaya jest doradczynią króla!
8. że Oin jest w istocie groźnym - i obytym - krasnoludem, którego słowa potrafią być ostre.

> PD i PR po następnej


## Sesja 2: u Oina

Podział drużyny! Oin proponuje gościnę Myrtle i Ernwarowi (na wieść, że on ma magiczny przedmiot przydatny w wyprawie). Resztę... uprzejmie zaprasza do gospody. Herefra wciąga w sprawy Ernwara, ten zaś odbiera całą sprawę jako... fakt, że krasnolud nie ma dość dużo pokoi dla dużej ilości niezapowiedzianych gości.  


Królewscy posłańcy: ciężkie, srebrem okute zwoje składają na ręce Öriego i Valayi.

Zaproszenia dla Öriego Oinssona i kompanii. Wraz ze świtą, czyli można zabrać na uroczystość - kogo się żywnie podoba. Pismo król podpisał własnoręcznie! Valaya widzi również drobną runę - wezwanie dla niej o stawienie się w dowolnej chwili lecz możliwie szybko.



\- Ernwarze, uważaj na siebie - mówi Herefra, patrząc na młodziana ponuro - Nie ufam temu kransoludowi. On Cię wykorzysta i będzie chciał zwrócić przeciwko reszcie. Już zaczął nas dzielić.

\- Sądzisz? Bo  pewnie postawiliśmy go w złej sytuacji i biedny pokojów nie miał. Popatrz ile nas jest. U mnie w domu na pewno nie dałoby się nas podjąć w jednej izbie, a pan Oin na pewno chciałby dać każdemu pokój... nie, komnatę. No i... nie zapowiedzieliśmy się wcześniej. To pewien kłopot dla gospodarza.

\- Ernwarze... - Herefra wywraca oczami. - Bzdura! Chce mieć Ciebie bo tylko Ty możesz korzystać z mocy artefaktu. Reszta nie jest mu potrzebna i chce się nas pozbyć.

\- To mu się nie uda Herefro - młodzian popatrzył na Rohirrimkę spokojnie - W żaden sposób.

Po chwili ciszy dodaje:

\- Żywię szacunek do pana Oina, jest bohaterem. Ale to nie dla niego wyruszyłem na szlak.

\- Dobrze. Nie pozwolę Ci pójść w niebezpieczeństwo beze mnie. - Herefra mocno ściska Ernwara za ramię i odchodzi.

To chyba ja Tobie... spóźniona riposta zawisła na ustach Puszczanina.

* * *

Myrtle - z hobbickim i zgoła nie-Tukowskim rozsądkiem - decyduje, że po takim starciu lepiej będzie NIE myszkować po dworze. Ale znajduje placek z gruszką w swym pokoju i to nastraja ją lepiej. 

Ernwar prosi o lekcje pisma runicznego by rozumieć go więcej. 

* * *

Herefra wciąż kipi złością, choć po rozmowie z Valayą - już mniej.

Gospoda jest zacnym lokalem i są tam zarezerwowane 3 pokoje, choć lepiej byłoby nazwać je komnatami - znać, że nie poskąpiono funduszy. Część gospody jest jeszcze nieodbudowana i pokazuje, że Erebor niedawno został odzyskany. Nie jest to lokal samych arystokratów, jest w sali wspólnej typowo krasnoludzka arenda do zapasów, jest tarcza do rzucania toporkami i tor dla świnek morskich.

Gospodarz osobiście wita gości i zaręcza, że postara się zarobić na zapłatę jaką otrzymał. 

Gundin daje każdemu chwilę by odpocząć a potem puka do każdego z pytaniem czy mają ochotę na ???  jakiś trunek ??? . 

Herefra zaglądając do konia spotyka człowieka w kapeluszu o szerokim rondzie - nietypowe dla Ereboru. W stajni jest też drugi koń tak wspaniały jak Arod. Klamerki popręgu i inne detale osprzętu wskazują na podobne rzemiosło i zdobienia jakie Herefra pamięta wśród rzeczy swej matki.

Gundin zostaje wyzwany na pojedynek na picie. 

W piwach: Kucykowy przysmak, Rubinowy Stout, Piwo z Kruszonego Kamienia,  Miodusznik,  Piwo siwobrode, Węglowa polewka.

Z mocniejszych: Puszczański miód, Dalijska Trzcinówka, Gondorska brandy, Przepalanka, Muchomorek, Muchomorek Sromotny, Rysi Spacer, Absynt Źródlany, Dech Durina, Runiczny Destylat.

Gundin zaczyna od Węglowej polewki, Undur od Przepalanki. Gundin przeskakuje na Muchomorka, Undur rzuca, że jak na poważnie, to na Absynt (choć z niepewnością).

Absynt okazuje się mocnym trunkiem. Undur wycofuje się nawet zgrabnie, mówiąc, że daje kompanowi fory, bo ten z paniami przyszedł i powinien dobrze wypaść.

Herefra wyraża obawę o to, że Undur zaraz straci pracę, ale Valaya niewzruszona stwierdza, że to jego problem i że jest dorosłym krasnoludem. 

W dyskusji wychodzi jednak... że zamawiany jest Runiczny Destylat. 

Ten pojawia się na paterze, w zalakowanym gąsiorze. Pieczęć pamięta jeszcze króla Thraina, widać, że trunek jest mocarny: tworzono go alchemicznie!

\- To chyba drogie? - szepcze Herefra na ten widok.

\- Owszem, to jest drogie - tym samym niewzruszonym tonem odpowiada Valaya

\- Czyli, nam nie wypada?

\- Nam nie. Im, owszem.

Destylat kładzie obu panów! Undur rzuca uprzejmie (niemal w stronę obu pań):



Herefra skrycie nalewa sobie odrobinę trunku. Sam zapach odurza i otumania. Herefra próbuje i...! Zanosi się głośnym śmiechem i niemal z zaśpiewem rzuca: Fajne to!

Gundin stwierdza, że to dobry moment, aby rzucać toporkami do tarczy! Wstaje, zatacza się nieco, dobywa toporka, rzuca... i trafia niemal w głowę właściciela lokalu! 

Valaya stwierdza, że to dobry moment by zakończyć przyjęcie. Herefra pyta: Ale... wrócisz do mnie? Valaya - wciąż spokojnie - Owszem Herefro.

Właściciel lokalu zwalnia obszczymordę, podczas gdy Herefra jest prowadzona do alkierza przez Valayę - na plus dla Rohanki należy rzec, że pamięta wciąż o tym, by poprosić starszą towarzyszkę o to, by zajrzała do jej konia.

\- Bracia krasnoludowie, wstyd to i sromota nieuznawać bohaterów waszego i nie tylko waszego, siedliszcza! Zdrowie kompanii co pokonała hultaja Malaryka! Sława bohaterom! Sława krasnoludom i sława mistrzom koni Rohanu! - w te słowa odzywa się człowiek, toast wznosząc i patrząc na Herefrę, która czerwienieje. 

Wśród sprzeczek ktoś się odzywa zgrabniej, dziękując Herulingowi z Dunharrow, oraz bohaterom, którzy ocalili sojusz.

Dziewczyna na skraju płaczu, mocno ściska ramię Valayi

\- Proszę, zabierz mnie stąd - szepcze.

Valaya czyni to bez zbędnych ceregieli.

* * *

Pan Oin pomiędzy istną baterią listów trudzi się nad planem, kiedy do jego pokoju wchodzi jego pierworodny. 

\- Synu, co w sformułowaniu tajemnica było niepojęte? Ile osób wie, o naszym przedsięwzięciu? 

\- Tylko moi towarzysze.

\- O kim mówisz? O towarzyszach, co zabrali sekret do grobu? O tych, co są na dworze króla elfów? O tych przybłędach, których przyprowadziłeś do naszego domostwa? Rozumiem Valayę. To przebiegła kobieta. Niemal mi cię odebrała!

\- Ale kiedy?

\- Nie czułeś jej dłoni na ramieniu, kiedy dziś chciała obrócić Cię przeciw mnie? Öri, jeżeli my nie będziemy zgodni, to już nic mi nie pozostanie.

Oin chwali syna jako przywódcę i podkreśla, że ludzie przezeń wybrani na pewno posiadają użyteczne przymioty. Chwali dobór Myrtle, ponieważ każda wyprawa potrzebuje złodzieja, a krasnoludy są zbyt honorowe na to.

\- Mówisz, że znasz swoich przyjaciół? Dlaczego zatem... Bardinsson? 

\- Nie mów ojcze, że wierzysz w to, co mówią o Bardinssonach?

\- Jak cię przekonał?

\- Zasadniczo nie musiał mnie przekonywać. Jego czyny mówią za siebie.

\- A czego chce w zamian? Cień Bardinssonów to złowróżbny omen. Już raz w przeszłości położył się na naszym rodzie.

Gdy Ori odpowiada, że nie wie, jego ojciec podkreśla, że to ważna rzecz i sugeruje iż chciwość Bardinssonów może być znacząca. Rozmowa schodzi na Radagasta i to, co mogli mu na osobności powiedzieć towarzysze. 
Ernwara, jego korzenie i artefakt. Öri opisuje chłopaka jako Puszczanina, lecz Oin wychwytuje, że artefakt działa w jego rękach i pyta jak to możliwe. Próbuje też zidentyfikować przedmiot po opisie.

\- Pies Sarumana, Arcirias, wypytuje o to, co ze sobą przywiozłeś. Pora posprzątać po swoim bałaganie. Synu, Arcirias musi zniknąć.

Oin nie wierzy w niemieszanie się czarodziejów - zwłaszcza Sarumana! Radagasta nieco mniej, lecz też zauważa, że to najpewniej Bury czarodziej wskazał trop prowadzący do przedmiotu.

Öri nie wytrzymuje i nieco podnosi głos, cedząc: 

\- Czy bitwa 3 armii niczego nas nie nauczyła? Nie można odrzucać przyjaciół!

\- Synu \- kontruje surowo Oin - Zająłem się tym. Mamy przyjaciół. Lojalnych, nie tych co chcieliby klęski naszej wyprawy. 

Także na temat króla ojciec i syn mają różne zdania. Oin szydzi, że król boi się odzyskania Khazad Dum i że jest przeciwny. Kiedy syn pyta go czy zapytał wprost, Oin z cieniem pogardy mówi, że owszem, ale że widzi dokąd zmierza ta rozmowa. Chłodno i stanowczo mówi synowi, żeby sam zapytał króla, lecz jednocześnie obarcza go odpowiedzialnością za ewentualny zakaz królewski!

Öri opuszcza komnaty ojca ścigany jego cieniem i pełen wewnętrznego sprzeciwu. Wie dobrze, że nie zostanie zabójcą.

* * *

Zgłodniała Myrtle znajduje drogę do kuchni, gdzie zapoznaje się z cokolwiek zaskoczonym kuchmistrzem. Temu z pewnym trudem udaje się powstrzymać młodą hobbitkę przed szarogęszeniem się... by potem samemu musieć zrobić: pikantne kuleczki orzechowe, herbatę z kon... kon... końfiturą?, bułeczki z serem, podgrzewane i może jeszcze dwa dania.

Czekając na dania, Myrtle widzi fresk, przedstawiający... hobbita! Początkowa radość jednak powoli ustępuje, hobbit na obrazku obwieszony jest przedmiotami, ma garbaty nos, chciwy grymas... Panna Tuk marszczy nosek, co podchwytuje kuchcik:

 \- Czy coś się przypala?

\- Czemu macie taki dziwny obrazek?

\- A, to ważny element kultury krasnoludzkiej! Bo czasem, jak trzeba coś komuś odebrać, to, no, wiadomo, nie wypada, to nieładne. Ale są takie stworzenia... - kuchcik dostrzega z kim rozmawia i wymawia się bułeczkami.



Ujawniono:

1. że Oin ma temperament i hardy kark krasnoludzki
2. że Dain II Żelazna Stopa szybko reaguje na sprawy
3. że Herefra ma temperament, a Oin - bardzo ciężkie spojrzenie, które czuje się nawet na plecach
4. że posłańcy królewscy widzą w Örim bohatera
5. że mężczyźni - jeśli ufać słowom Valayi - potrzebują dłuższego czasu by dojść do pewnych spraw, a im starsi są, tym dłuższy to czas. I - na szczęście dla kompanii - że król Dain młodszy jest wiekiem od pana Oina.
6. Że pan Oin wierzy, że król mu nie sprzyja, Valaya uważa, że "bierze sprawy zbyt do siebie".
7. Że Bardinsson zna Undura Bonssona, honorowego obszczymordę, któremu raczej można zaufać, który za odźwiernego robi. 
8. rozmaite krasnoludzkie trunki: Dech Durina, Kruszyna Kamienia i kilka innych :-)
9. że niektóre budynki jeszcze miejscami są w ruinach w Ereborze
10. po raz kolejny - że krasnoludzka etykieta należy do skomplikowanych
11. że spokój Valayi ma rozmiar przynajmniej jeziora. I że ma ona wprawę w pacyfikowaniu pijanych krasnoludzkich mężczyzn... lecz czy należy myśleć o tym w kontekście króla? 
12. że Oin wierzy w zdolności przywódcze syna, jego dobór kompetentnych ludzi, ma natomiast wątpliwości co do ich lojalności, zwłaszcza Valayi i Gundina!
13. że rody Öriego i Gundina mają wspólną przeszłość, ponurą, może nawet waśń
14. że Oin jest politykiem: wie o koneksjach Sarumana i Gondoru, nakłania syna do zabójstwa sługi czarodzieja by kupić czas by wyprawa mogła wyruszyć, chce zabrać tylko niektórych, dokładnie jak przewidywała Herefra.

> PD i PR: 13 i 16.



## Sesja 3: dzień przed ucztą króla Daina

Valaya spotyka się z królem odpowiadając na jego wezwanie. Spotkanie w ukrytej bibliotece. Król ma ranną rękę, którą zdaje się testować. Częstuje Valayę miodem i osobiście jej nalewa. 

Valaya pyta o ranę, król odpowiada, że zamierzył się na swą starość. 

\- Wciąż walczysz panie - mówi Valaya.

Córka Badoka uzyskuje gratulacje króla za przyprowadzenie doborowej kompanii, władca również wyraża powściągliwie zadowolenie, że Oin - co nie w jego stylu - siedzi cicho i nie prowokuje zamieszek na ulicach a potem każe sobie radzić. Czy Öri jest krasnoludem, którego potrzebuje Erebor? Valaya bez wahanie potwierdza!

\- Myślisz, że nadałby się na dyplomatę?

\- Hmmm, patrząc jak rozmawiał ze swoim ojcem, musiałby się jeszcze wiele nauczyć.

\- Jeśli pod odpowiednim nauczycielem...

\- Czy planujesz panie zaproponować mu jakąś posadę?

\- To zależy, ale tak, myślałem o tym. Jak ustawiliście stosunki z ludźmi z Puszczy?

\- Odniosłam wrażenie, że część z nich była niepocieszona tym, że odchodzi.

W dalszej rozmowie Valaya przestrzega władcę przed planem Öriego na wyjście spod cienia swego pana ojca, dowiadujemy się też, że dzielnica Kruszonego Kamienia to sprawka Valayi i Öriego, i że na balu oficjalnie Valaya zostanie ogłoszona rajczynią.

Oinssonowie planują wyprawę i Valaya proponuje by wesprzeć Öriego jako dowódcę tejże wyprawy. Pierwszy pod Górą dzieli się nowiną, że jakiś młodzik z okolic Valayi wybrał się na Khazad Dum, ale plany Oinssonów są dlań nowiną. Oboje dyskutują realia wyprawy do Morii. Dain nie lekceważy orczych plemion i przypomina rzeź jaką orki sprawiły ich przodkom. Valaya przywodzi wspomnienie Azoga, zabitego przez samego Daina i porównuje cios młodego krasnoluda wtedy, do Öriego teraz.

\- Thrain był dobrym krasnoludem Valayo. Honorowym, uczciwym. Wielu można znaleźć takich i teraz. Ale zaślepionym. Moria jest stracona, Valayo. Gdybyś wtedy była u mojego boku, wiedziałabyś o tym.

Mimo tych słów, Valaya oręduje za sprawą młodego Öriego, próbując wyjednać u króla, by przynajmniej nie zabraniał młodemu i nie obciążał go królewskim zakazem. 

\- Nie wszystkie dary nam ofiarowane, dane były ze szczerego serca. Siedem takich wątpliwych prezentów spadło na nasze barki. Ufam, że wiesz o czym mówię. Do dzisiaj się zastanawiam, czy to smród zdrady, którą dało się wyczuć od jednego z siedmiu. Czy to chciwość naszych praojców, w kopaniu niżej, i niżej. [...] W środku, było bardzo ciemno. To było najciemniejsze miejsce, jakie widziałem. Sam widziałem cały hufiec krasnoludów. Jeden z nich był moim kuzynem. Braliśmy udział w wielu bitwach razem, ale na ten widok oni... odwrócili się i pierzchli. W tym mroku Valayo... ten mrok był niesamowicie gorący. Czasami mam wątpliwości. Valaya, nie wiem, co to było. Czasem, jak mam zbyt dużo czasu i obowiązki - lub niestrawność, wiesz, mam swoje lata - król zaśmiał się słabo - ale ja jeszcze nie zwariowałem. Zazwyczaj to, co widzą moje oczy, to tam jest. Jeśli mówię Ci, że tam był mrok, gorący, nie! gorętszy niż środek kuźni, to uwierz mi, nawet gdy sobie sam nie dowierzam... Ale ja dwa razy widziałem w życiu smoka. I nie był to przyjemny widok, ale to... może Durin dałby radę. Może tylko sam Aule???. Wiem jedno Valayo. Ja do Morii nie poślę ani jednego krasnoluda. Możesz masz rację. Może Öri jest prawdziwym bohaterem. Ale... co jeżeli jest po prostu porządnym krasnoludem. Ja nie posłałbym tam swojego syna. Tobie bym tego nie doradzał. A co jeżeli rzesze młodych, podatnych na wpływy, krasnoludzkich umysłów, zainspirowanych famą Oriego co ruszył na Morię, pójdą za nim? Jak znam starego Oina, on by im nie zabronił. 

\- Oin nie. Öri nie wysłałby nikogo na śmierć, wiedząc, że to ich tam spotka.

\- Tyle mogę Ci powiedzieć Valayo. 

\- On nie chce się wykazać. On chce, jak sam to pięknie mówi, przywrócić nasz dom.

Król zgadza się wysłuchać Öriego i spotkać się z nim, lecz wyraża też niepokój postawą młodych, obawia się naraz istnej parady wypraw by odbić Khazad Dum. Rozmowa wraca do młodzika z Gór Błękitnych: Kurti, syn Urdany, siostry Thikkurila, który, wg słów króla, przejawił się już w życiu Valayi. Tharantrask, wuj Kurtiego, jest w mieście i szuka kompanii.  Król przestrzega, że jeśli osobiście zainteresuje się tą sprawą, będzie musiał wydać wyrok, do czego niedawno się skłaniał, wybierając między ciężkimi robotami, więzieniem albo... najsroższą z kar: nakazem chodzenia w czerwonej, stożkowatej (o zgrozo!) czapce!

Król składa na barki rajczyni rozmowę z Tharantraskiem, ujawniając, że rezyduje on w zajeździe... który dla gości wynajął Oin! Valaya z cieniem uśmiechu odpowiada, że chyba da radę go znaleźć. Władca i doradczyni dzielą się jeszcze kilkoma chwilami powściągliwego, krasnoludzkiego humoru i zażyłości. Wielu krasnoludów i nie tylko krasnoludów - będąc świadkami - pozazdrościło by Valayi na pewno.

* * *



Gundin Bardinsson zdecydowanie nie mógł być trzeźwy. Nie mógł, ponieważ w zajeździe, w jego komnacie, ubrany jak jaki mieszczek, a nie wielki pan, jest pan Oin!

\- Trzeźwyś, Bardinsson - chłód i pogarda biją aż z głosu "mieszczanina" - byśmy pogadali otwarcie? Jaka jest Twoja cena!

\- Cena? Ale za co?

\- Oszczędź mi. Powiedz ile. Za milczenie.

\- Moja waga w złocie, a chudy nie jestem! - Gundin zaczyna coś mówić, ale przestaje.

\- Mów, mów, ciekawym.

\- Miałem rzec, że nawet wyperswaduję Öriemu wyprawę do Morii, ale tu akurat nasze interesy są takie same. 

\- Dlaczego miałbym wyperswadowywać synowi wyprawę, która da nam tak wiele?

\- Mogę nawet zająć się Valayą dla Ciebie panie, za darmo. 

\- Co masz na myśli?

\- Bruździ Wam przecie panie?

\- Jak byś się nią zajął?

\- No... myślałem, że mnie oświecisz.

Oba krasnoludy mierzą się wzrokiem lecz to Gundin pierwszy spuszcza wzrok. Majestat Oina nie jest bynajmniej lekki!

Oin zgadza się na targ i żąda podpisu Gundina na kontrakcie. Wobec świadka - krasnoludzkiego bankiera!

\- Jeśli sądzisz - syczy nieco tylko się chwiejąc Gundin - że to sprawi, że przestanę szukać przyczyn upadku mej rodziny, to się grubo mylisz!

W rozmowie ujawnia się  powiązanie między Oinssonami i Bardinssonami! Oin nawet wyraża wdzięczność za wspaniałomyślny akt przodków Gundina i mówi, że zdaje sobie sprawę z tego jak konsekwencje zostały Oinssonom oszczędzone, przypomina jednak, że to Bardinssonowie przyszli do ich Hal z propozycją i oskarża młodego Bardinssona, że ten chce wykorzystać ten sekret jako broń, ponieważ nie jest prawdziwym mężczyzną.

Dain II Żelazna Stopa wydaje ucztę na cześć bohaterów odpowiedzialnych za klęskę Nieprzyjaciela w Dol Guldur.

Myrtle i Öri rozmawiają o fresku i nie tylko. Öri na początku nie wie nawet skąd taka rozmowa i podejrzewa, że Myrtle może jest głodna? Lecz kiedy hobbitka opowiada o 'portrecie', krasnolud przypomina sobie. Fresk Triumf Kordera nad niegodnymi z Dali, opowiadający o tym, jak pradziad Korder wynajął hobbita by pognębić swych krewnych. Fresk, który urągliwie przedstawia hobbita i który nie podobał mu się jeszcze jak był dzieckiem, ale który mu 'wytłumaczono'.

Öri podnosi się z miejsca. Idzie do kuchni. Wraca z freskiem i kuchmistrzem, któremu przykazał przynieść swej przyjaciółce ciasteczka ze śmietaną. 

Fresk znajduje swe przeznaczenie w piecu, a Öri przeprasza Myrtle i tłumaczy jej pewne niefortunne przekonanie panujące wśród niektórych krasnoludów. O braku honoru wśród hobbitów. 

\- Każdy krasnolud, który Ci uchybi, będzie miał ze mną do czynienia - rzekł Öri poważnie, klękając przed hobbitką. 

Myrtle odetchnęła głęboko i przytuliła się do krasnoluda, który poczuł bezpośredniość i bliskość, dotąd nie łączoną z... domem. Może te korytarze powinny ujrzeć więcej szczerości, uścisków i bezpośredniości?

* * *

Ernwar wywiaduje się o rodzie, którego broszę posiada. Temat jednak nie jest w smak tutorowi i dopiero przebicie się przez kilka zmian tematów pomaga. Rugnissonowie są podobno przeklęci - ich koniec ma być w ogniu. Niegdyś znaczny ród, wojowników, nawet książąt. Klątwa sprawiła, że krasnoludy niechętnie o nich mówią nawet i nie dają im schronienia. Ostatni ich potomek tydzień temu dotarł do Ereboru... lecz nie został przyjęty. Pasterze przyszli niemal równo z nim, skarżąc się na płomienie, które strawiły ich stada. Król Dain wypłacił rekompensatę z własnej kieszeni, pożałował biedaka. Potomek Rugnissonów został wyekwipowany i wyruszył dalej. Dokąd? Może powiedzieć Rejestr Gości Ereboru w komnacie Mazarbul, komnacie pamięci.

Młodzian zamyśla się... lecz w mroczne miejsca wiodą go jego wspomnienia. Opowieści o głosie Glaurunga, klątwa Turina Turambara... Czy to klątwa Balroga, a może smoka? Obie pasują. Smoki potrafią zmienić przeznaczenie ludzi. Balrogi... to potężne istoty, starsze niż niemal wszystko inne. 

Obyty krasnolud zasugerował młodemu Puszczaninowi, by sprzedał rzeczy od Rugnissonów, bo nawet one potrafią ściągnąć płomienny koniec na swoich właścicieli.

Wymienione zostały uprzejmości, pożegnania i ukłony. Pamiętając otrzymane lekcje młodzian skłania się niżej: z racji na wiek, nauki, w podzięce i za brodę.

Tutor spojrzał na ludzkiego młodzieńca przychylnie, jak nauczyciel w chwili pedagogicznego triumfu, w jego oku dało się dostrzec ten rzadki błysk. Ten moment był ostatnim, kiedy obaj się widzieli. Następnego dnia, nowy tutor z przykrością poinformował młodziana, że jego poprzednik... zginął w pożarze.

* * *



Pamięta skrzypce. Pamięta zimę. Mroźną, surową zimę. 

Palili czym popadnie, by się ogrzać. Skrzypki... skrzypki dały kilka godzin gorąca.

Czy to sen? Skrzypce wciąż grają?

Herefra siłą woli zmusza się do powstania, siłując się z... kacem. 

Próbuje przyzwać sen - każde wspomnienie rodziny jest wszak tak cenne. 

Pamięta ręce swej matki. Jej kości policzkowe. Niewiele jednak więcej.

Wzdychając, może wskutek snu a może widząc ile klamer mają jej buty i ogarnąwszy się wychodzi na wspólną salę. Spotyka Valayę, wciąż słysząc skrzypce - gra drugi Rohirrim w gospodzie, pan Heruling.



Herefra podchodzi do ich stolika. Spojrzeniem Mistrz Koni przepłasza swego kompana i zaprasza ją do stolika.

\- Jestem Herefra,
\- Wspaniałe imię. Nie dziwię się Heruwin, że wybrała je dla Ciebie. Ale, nie wszystkim się podzieliła. Nie zareagowałaś na własne nazwisko rodowe. Dunharrow, nic Ci nie mówi? Czyż nie pochodzisz z tamtąd?

Herefra z zaskoczeniem wita informację. Wszak całe lata, gdy skarżyła się na to, że dzieci śmieją się, że nie jest stąd, słyszała "teraz już tak". 

Ten wieczór okazuje się pełen nowin. Jej matka nazywała się Heruwin. Jej ojciec: Dunheort. I oto ktoś... kto ich znał.

\- Szczerze powiedziawszy, liczyłem na to. Coś jednak musiało zostać ze starego Rohanu. Nie można tak odciąć się od własnego dziedzictwa. To widać, jesteś wszak Rohirrimką. Jesteś stworzona do siodła. Miecza. Włóczni. Ty też nie umrzesz przy kominku. I to jest coś, co niewiele osób może powiedzieć z dumą. 

\- Rodzice opuścili mnie już jakiś czas temu. Mówiłeś o dziedzictwie, ale ciężko stracić coś, czego nie do końca się nawet ma. Mówiłeś o... okolicznościach w jakich rodzice opuścili dom. Jak to wyglądało z Twojej perspektywy?

\- Z mojej perspektywy? - gorzko odrzekł mężczyzna - Dziecino, zebrałem burzę, którą zasiali oni. Jak już powiedziałem, byliśmy w dobrej komitywie. Ale dobrze. Stary Raekhost nie zasługiwał na siedem ostrzy w swej gwardii. Powiedzmy, że jedni powiedzieliby że Twoi rodzice zrobili krzywdę tylko sobie, inni, że czyjąś władzę można zakwestionować... Jak widać, niektóre zmiany zachodzą zbyt powoli by mogły się utrzymać, jak kwiecie ściśnięte zbyt wczesnym mrozem. Jak widzisz, ja również jestem na szlaku.

\- To... przez... tamte wydarzenia?

\- No, nie roztkliwiaj się. Żyjemy. 

\- Żyjemy - cichutko wyszeptała dziewczyna.

\- No - roześmiał się Heruling - a teraz jakbyś naprawdę w to wierzyła!

\- Nie mogę, zbyt boli mnie głowa

\- Cóż, jeszcze jutro zostaję w tej mieścinie, potem czas na wyprawę

\- A dokąd? 

\- Tajemnica. Nie wszyscy dobrze widzą, że mój patron wydaje na to pieniądze. Ale wiesz, w razie czego mogę go przekonać. Mamy jakieś wolne miejsca. 

Słowom towarzyszy sugestywne spojrzenie mężczyzny, który mógłby być jej ojcem. Mężczyzny o rysach podobnych rysom samej Herefry. Którego towarzyszem był Tharantrask, z którym kiedyś Valaya dzieliła szorstkie porozumienie, które po rozstaniu się z jego bratem Tikurilem... mogło się zmienić. Krasnoludka obserwuje dwójkę Dużych Ludzi lecz nie przeszkadza im. 

Dwójka niespodziewanie spotkanych krewnych namawia się na jutro, gdy Herefra ujawnia, że jutro jest bal u króla. Zaskoczony Heruling pyta ją o jej kreację, jej kolor, a potem zaskakuje ją mówiąc, że w takim razie przygotuje coś pod kolor i odbierze ją o siódmej.



\- Valayo! Potrzebuję kreację pod kolor oczu, bo jutro idę z tym panem na bal do króla!

\- A kim jest dla Ciebie ten pan?

\- No... nie wiem?

\- Czyli zabierasz na bal do króla kogoś obcego?

\- No... ale, on znał moich rodziców? I... czy nie jesteśmy do siebie podobni? I czy mówi Ci coś imię Raekhost?



Raekhost - znany wskutek skandalu dotyczącego jego dumy. Raekhost wywalczył sobie politycznie przywilej stanowienia gwardii przybocznej króla Rohanu...  Gdy jednak doszło do święta, gdzie miał króla chronić, okazało się, że miał o jedną osobę za mało. W debacie czy dopuścić szlachcica do tego zacnego obowiązku Raekhost obruszył się na władcę i zaczął tworzyć koterię anty-królewską. Mówi się, że od tego momentu rozpoczął się schyłek silnej władzy królewskiej. Uważa się, że jednak sam Raekhost słusznie stracił królewskie względy, był jakiś haniebny skandal stojący za odejściem tego brakującego gwardzisty... bądź gwardzistki. Działo się to mniej więcej dekadę temu.



Młoda Herefra szybko łączy miecz otrzymany od swej matki z całą sprawą.

\- Valayo, to moja matka wkurzyła się na tego mężczyznę...

\- Nie mam takiej wiedzy, by odpowiedzieć na wszystkie Twoje pytania, ale możemy jej wspólnie poszukać. Np. porozmawiać z tym mężczyzną 

\- Ale, on trochę oberwał za to... o nie! 

Herefra biegnie do stajni, sprawdzić co z Arrodem!

* * *

Ernwar i Ori spotykają się w drzwiach domostwa Oinssonów. Młodzian - pomny nowych nauk - skłania się krasnoludowi na modłę jego ludu, uwzględniając brodę, wiek, i status gospodarza. Ten, mile zdziwiony, w myślach kwituje, że dwa jeszcze lata takiej nauki i młody Puszczanin będzie kimś, z kim nie wstyd będzie pokazać się choćby i na dworze. W charakterystycznym dla siebie bezpretensjonalnym stylu czcigodny Oinsson mówi Ernwarowi, mierząc wzrokiem jego wciąż nieistniejącą brodę, że co prawda brody nie masz, ale jak mi się kłaniasz, to możesz się traktować jakbyś miał - o - taką!

Choć Ernwar zaraz zaczyna opowiadać czego się dowiedział, to jednak krasnoludowi (i otoczeniu) udaje się go przekonać, by wpierw dotrzeć do gospody.

Tam młodzian mile zaskakuje Gundina, gdy ujawnia że do serca wziął sobie jego słowa i poszukał potomka nieszczęsnych krasnoludów z podziemi opodal Spacerowych Smajli.   

Beleriand ujawnia, że zapytał tutora i dzięki temu odnalazł ślad Rugnissonów. Że tydzień temu odprawiono sprzed drzwi Ereboru potomka rodu. Pyta krasnoludy o to, czy nazwisko to faktycznie tak straszliwe lub chociaż że nie lza o nim mówić i przywodzi reakcję szlachetnego tutora i jego opowieść o klątwie i płomiennym końcu tego rodu. Nie szczędzi nawet tego, że poradzono mu by nie szukał dalej a przedmioty sprzedał co rychlej by końca w płomieniach uniknąć. Opowiada legendę o Turinie Turambarze, smoku Glaurungu zgubie Nargothrondu, wywodzi o Zgubie Durina, by unaocznić jak mocne muszą być istoty, które takową klatwę na ongiś możny ród mogłyby rzucić, by go do takiego stanu sprowadzić. Reakcje jednak starszych towarzyszy zimnym kubłem są na gorącą głowę. Gundin i Ori nic nie wiedzą, jedynie jakaś stara klechda się przypomina, którą się dzieci straszyło. Valaya słucha z uwagą, swoim zwyczajem łącząc nową i starą wiedzę... lecz nawet gdy wzrok młodego gorliwie uwag jakich wypatruje widzi... pobłażliwość. Jakby matrona wiedziała, jakie błędy w jego historii się kryją lecz płazem mu je tym razem puścić miała. 

Już właściwie temat odpuszczając, młodzian jedną rzecz wynajduje: król Rugnissona odprawił, a pasterzom z własnej kiesy zapłacił. I Rugnissonowi wyprawę opłacił. Gdy Valaya - zwykłym tonem swoim, spokoju pełnym - stwierdza, że król powody miał, tedy młody potakuje, że właśnie. Czyli coś w tym jest. Gdy jednak sugerują mu, by króla spytał, płoszy się, że on prostym jest Puszczaninem i że za wysokie te progi na jego nogi. Ori prycha, zaś Valaya delikatnie uśmiecha się i mówi, że króla spytać może.



| PD 10 i PR 14

 

## Sesja 4: w dniu uczty króla Daina - przedpołudnie

1. Valaya rozmawia z Tharantraskiem. 
2. Kompania zastanawia się co jak król Dain zakaże wyprawy.
3. Ernwar w swojej komnacie spotyka pana Oina o której rozmowie opowiada potem Myrtle i Oriemu - Elendilmir 
4. Gundin i Ori kują - rękawicę
5. Valaya i Ernwar knują ;-) - sekretne akcje
6. Myrtle zwiedza Erebor i z Undurem idą na pierożki i omawiają biznes marmoladowy

### Valaya i Tharantrask 

Ujawniono: 

- oboje żyją w głównie w drodze
- Valaya woli nie mówić od kogo wie, że Kurti i Tharantrask ruszają w dal, Tharantrask woli nie mówić gdzie
- Tharantrask stwierdza, że Kurti wdał się w głupszą gałąź rodu, jego, czy swego ojca. 
- Ty i Badoc postawiliście na niezłego konia. Niegdyś nam się nieźle powodziło. Teraz, rozumiem czemu Kurti chce czegoś dokonać.
- Valaya zaskakuje Tharantraska ujawniając, że nie wyszła za mąż. Ten jest zdziwiony, gdyż "na pewno niejeden się starał".
- wybierają się odratować Kurtiego, który chce szlify zdobywać
- Valaya ujawnia, że wie dokąd, Tharantrask pochmurnieje i pyta ile osób o tym wie, ale nie zaprzecza
- Tharantrask otrzymuje bransoletkę Valayi by "po prostu mu to daj" (Thikkonlir)
- sugerowana jest wspólna przeszłość Valayi i Thikkonlira
- Hobur Bardinsson - mistrz górnictwa z Gór Żelaznych co namotał w głowie Kurtiemu, że zna sekretne wejście do Morii - Hobur to starsz brat Gundina
- Wyruszają za dzień. 



### Drużyna rozważa wyprawę

Herefra pyta Ernwara o Raekhosta i dzieli się opowieścią od Herulinga.

Raekhost Niegodny Baron - opowieść o tym jak charakter doprowadza do upadku. Samolub, chciwiec, folguje sobie i inni to widzą. Ernwar mówi, że to opowieść ciotki Matyldy.

Rodzice Herefry nie są publicznymi postaciami

Ale jest postać dzielnej damy rzucającej rękawice baronowi - była pierwszą osobą, która miała pokazać światu jego niegodziwość.



Drużyna rozmawia o tym co jeśli król zabroni wyprawy. Ernwar proponuje sekretną wyprawę przed balem albo poproszenie króla o zezwolenie na sekretną wyprawę pod pretekstem zwiadu.

Gundin w trakcie rozmowy tylko pił słaby alkohol by się otrzeźwić, starym, krasnoludzkim sposobem i okazjonalnie coś mruknął.

Valaya, gdy wróciła do stolika, sprawiła wrażenie bardzo zmartwionej i nawet nieodległej od płaczu, na co Herefra wojowniczo zaproponowała, że wyzwie tego krasnoluda na pojedynek! Usłyszawszy spokojne "nie trzeba".
Ernwar dołączył, oferując pomoc jeśliby Valaya potrzebowała. Krasnoludka uprzejmie jak zwykle podziękowała obojgu.

Odwiedziny w stajni ujawniają, że drugi koń - zniknął. Dokąd pojechał nocą Heruling? Nie wiadomo.

Wracając do domu Oina Gloinssona, Ernwar opowiada Myrtle o czym rozmawiali nim przyszła.


### Ernwar i Oin

Ujawniono:

* konflikt na linii Oin - Dain: Oin oskarża króla o poddanie Morii, zrzucenie tego na jego barki, tchórzostwo, nawet osiągnięcia króla są "ledwo" lub "konserwatywne", a debata kto ma być królem wg Oina wskazuje, że Dain to "bezpieczna opcja"
* korzenie rodu Ernwara - choć nie do końca to ujawnienie
* wizję z Elendilmira i reakcję Oina na koronę, reakcję nieświadomie ujawnioną przez zakrzywienie rąk i przechył do przodu
* jak wygląda przedmiot, który Oin chce odzyskać:  berło z półwachlarzem i obręcz z granatowym kamieniem, za kamiennymi mlecznymi drzwiami __czy to jeden czy dwa przedmioty? topór i diadem?__
* jak głęboko może sięgnąć podejrzliwość krasnoludzka: Oin szuka komu jest lojalny Ernwar poza drużyną, "kogo chce zmiażdżyć pod obcasem buta kiedy już będzie miał potężnych przyjaciół"
* jak niesprawny w słowach jest młodzian. Chłopakowi nie udaje się przekonać Oina ani do spojrzenia inaczej na króla lub zastanowienia się nad tym dlaczego król czyni to co czyni, ani do docenienia syna, ani do dostrzeżenia niebezpieczeństw wyprawy ani do zaproszenia reszty kompanii do drużyny lub objęcia ich swą ochroną. Jedyne co osiąga, to ukazanie swej wartości, co zyskuje mu ochronę Oina oraz przesyła mapę Rugnissonów potomkowi rodu wraz z listem. Ernwar traktuje spotkanie jako osobistą klęskę i przeprasza za nią Myrtle i Oriego, postanawia też zacząć się uczyć, choć jeszcze nie wie czego.



Ernwar, Ori, Myrtle:

- gdy Oin wychodzi, przytłoczony rozmową (i Elendilmirem) Ernwar idzie porozmawiać z przyjaciółmi, 

- Ori pyta, czy jego ojciec uraził chłopaka ale ten odpowiada zaskakująco dojrzale, że pan Oin jest starszą osobą i patrzy na sprawy inaczej i że urazy nie ma
- Beleriand przekazuje jak duże jest brzemię Oina i jego samotność w jego dźwiganiu, 
- jedyne miejsce w pokoju Oriego które jest Oriego: posłanie Oda
- rysunek przedmiotu - Ori - to nie topór Durina, on wygląda zupełnie inaczej
- Myrtle - celne zdanie o Oinie (kto pamięta?)
- Ernwar przeprasza za swą nieskuteczność, widać, że gnębi go fakt iż ani razu nie przebił się do starego krasnoluda, że może pogrzebał okazję by przekonać Oina do zmiany zdania
- Ernwar i Myrtle uważają, że faktycznie drużyna może zostać postawiona między młotem i kowadłem, a że może nawet i królowi i Oinowi pójdą wbrew. 
- Ori stwierdza, że nie pójdzie wbrew królowi. Nie mówi tego jednak o swoim ojcu.
- Ernwar mówi, że spojrzenie Oina potrafi zgiąć człowieka w pół i żartuje, że Ori pod tym spojrzeniem urósł wysoki. Oinsson kwituje z uśmiechem, że to siła nawyku
- Ernwar zostawia Oriemu rysunek przedmiotu z wizji z dopiskiem "przechowaj go na noc"... oraz Elendilmir. Krasnolud chowa go w bezpiecznym miejscu!
- Odo - powiernik Elendilmira


**Wizja z Elendilmira**:

brama z księżycowego światła - tam w mroku jest mnóstwo cieni

łopoczacy sztandar za mną

albo odległe, skórzaste skrzydła

wiatr wzbija się, potężny, ale nie jest świeży

jakbym był w tunelu

stęchłe, porywiste powietrze wzbudzone przez coś, co ściska żołądek natłokiem emocji

od niewiedzy, która może być zgubna

w mroku widać też refleks - bardzo słabo, bardziej można domyślić się co mam przed sobą - złote berło, nie, berło byłoby kuliste u zwieńczenia, to berło posiada półwachlarz na sztorc

nie wiesz jakiej jest wielkości, perspektywa okłamuje,

bardzo słabo widać ceremonialny przedmiot, ale wydaje się, że jego część, coś, co go wieńczy, jest wyraźniejsze i posyła mi pojedynczy, szafirowy refleks, niczym tajemnicze mrugnięcie

jakby coś będące może najbardziej materialną częścią wizji

jakbym patrzył w lustro i za drugą taflą jego dostrzegł również podłużną postać

a na jej zwieńczeniu obręcz, posiadająca pojedynczy, przejrzysty, granatowy kamień, posyłający refleks, jak mój diadem posyła mój w ciemności

wiem, że to, co widzę, znajduje się na końcu ścieżki Oina

tam wybiega jego umysł, cały czas, nawet podczas tej rozmowy

jak latarnia, nawigująca statek podczas sztormu



### Pierożki

Myrtle pod wpływem szeregu niemiłych zdarzeń wybiera się zwiedzać Erebor by zobaczyć krasnoludy od innej strony. Pech sprawia, że Undur Bonsson odnajduje Myrtle i wprasza się na pierożki. Undur niezbyt zręcznie wciela się w rolę przewodnika po lokalnych delikatesach, cokolwiek nachalnie proponuje biznes pod nazwą Marmolady Bonssona, kiedy nawet nie umie gotować ani robić marmolady i gdy rozmowa schodzi na krasnoludy podsyca - a nieledwie utwierdza! - w Myrtle wątpliwości na temat tego jak do niziołków podchodzą krasnoludy. Mówi nawet wprost, że jest ona wyzyskiwana do cudzych celów. Próbuje wynaleźć kto zakontraktował Myrtle i Gudina, na jakich warunkach i bardzo zaskakuje go braki istnienia takiego kontraktu.



### Po przymiarkach strojów

Panie przed balem wybierają się dobrać stroje, w tym taki pod kolor oczu Herefry! Po powrocie do gospody sługa uprzejmie informuje, że ten jegomość pod ścianą dopytywał o waszmości! 

Opis Arciriasa (patrz DP), czytającego księgę. Bardzo skupiony, księga nowa, "Kroniki Gondoru tom 72". Valaya uświadamia sobie połączenie jakie ten człowiek ma do Sarumana i przypomina sobie, że o ile Radagast mówił, że Saruman mógłby ponownie rozniecić Latarnię Baltiego o tyle nie zasugerował by tam się udać.

Myrtle dostrzega intensywność (choć nie jak u Oina), ułożenie ale też sztywność w ruchach, charakterystyczną dla Dużych Ludzi piastujących Stanowiska.


| PD i PR? - następna sesja

## Sesja 5: Klątwa i bankiet

Kiedy Myrtle zwiedza Erebor a Valaya asystuje Herefrze w przemyślanych zakupach, Ernwar robi zakupy i pisze oraz wysyła listy i odbywa lekcję etykiety. Ta ujawnia, ku zgrozie Puszczanina, że poprzedni tutor zginął  - w pożarze! Ori zaprasza Gundina do swojego królestwa.


### W kuźni Oriego

Ori zaprasza Gundina do kuźni i ujawnia, że chce z nim wspólnie rękawicę wykuć. 
Pełno tam narzędzi i materiałów i Gundin mimowolnie okazuje uznanie.

Obaj kompani okazują się rzemieślnikami na schwał i praca wre. Szybko powstaje opancerzona rękawica, która nie zatraca ruchomości i w której palcami prosto poruszać idzie. Runy Gundin wytrawił kwasem i poprawił złotem. Do tego nałożył srebro - zwłaszcza przy i dokoła mankietu, gdzie dodatkowo osadził kamienie. 
Sama rękawica - dana pod światło - ukazuje warstwy błyszczące, jakby ze skruszonych kamieni szlachetnych.
Zwraca uwagę zwłaszcza piękny szlif w największym klejnocie, rodoid, który czasem wygląda jak zielony agat a czasem jak fioletowy ametyst. Umieszczony na ochronie śródręcza, w samym centrum, ma niecodzienny kształt. Wprawne oczy rozpoznają, że to kunsztowny szlif gruszkowy.

| Gundin może rozwinąć specjalne możliwości rękawicy wydając punkty Odwagi (nagrody).

Gundin pyta Oriego jak ten chce ją nazwać, lecz Ori uprzejmie mówi mu, że to jego robota. 

\- Ale... co to ma znaczyć? - Gundin wybałusza oczy
\- Nic tak nie spaja przyjaźni jak wspólna praca. Zasłużyłeś na to.

Gundin szuka czegoś po kieszeniach, ale nie znajduje nic czym mógłby się odwdzięczyć. Ori zauważa, że długo już razem podróżują. Zarumieniony Bardinsson przyjmuje prezent z wdzięcznością. Rękawica przyjemnie rozgrzewa stare blizny na dłoni, a ściskając słychać przyjemny dla ucha chrzęst skóry i pancerza.

Gundin natychmiast próbuje władać swym młotem. Kamień obrany na cel pęka pod ciosem, a zadowolony Bardinsson - słysząc jej muzykę - rzecze:

\- Nazwę ją Pieśnią Przyjaźni.

### W międzyczasie w gospodzie

Arcirias przedstawia się jako Kronikarz, Pierwszej Klasy, ujawnia też, że już odwiedził Vigola, wyraża żywe zainteresowanie opowieściami samych bohaterów. Valaya uprzejmie zgadza się na późniejszą rozmowę, Arcirias wtedy dopiero zamyka księgę, kłania się swoim zwykłym płytkim ukłonem i opuszcza gospodę. Valaya zostaje z wrażeniem, że kiedy rozmowa zeszła na temat Vigola, Arcirias... minął się z prawdą. 

Ernwar - blady i spłoszony - przychodzi do gospody i wita dziewczyny radosnym uściskiem, jakby po wielomiesięcznej rozłące. Poprawia brwa w palenisku, wpychając je głębiej, i pyta je o plany. Myrtle zdradza, że nie ma już planów i opowiada o dziwnym krasnoludzie, który chciał by mu załatwiła pracę.

Herefra uświadamia sobie, że brzęk podków jaki słyszy zdradza duże zwierzę, większe niż Herulinga... ale jakby obce. Zaciekawionam, wychodzi by zerknąć i dostrzega, że Arcirias rozmawia z elfem o złotych włosach. Ten uśmiecha się do niej mile, zatem Herefra odwzajemnia uśmiech.

Arcirias zwraca się do elfa per książę i przypomina mu o zobowiązaniu, książę odpowiada, że jeśli Arcirias tego wymaga, to on dopełni zobowiązania.
Herefra myśli, że w twarzy elfa jest coś nostalgicznego, ale może wszystkie elfy tak mają. Rumak elfa jest piękny, biały, bez jednej nawet skazy. Po oczach widać, że to mądre zwierzę. Uzda, siodło i osprzęg są elfickiej roboty, wielokroć splecione, złotem szamerowane, faktycznie rumak godny księcia.

Herefra podekscytowana dzieli się nowiną z kompanami, wywołując u nich pewne zaskoczenie. Książę? Elfów? W Ereborze? Myrtle wspomina Eola i Idril, pada imię Thranduil, ale to król... Wypytywanie Herefry nie idzie szybko, pierwsze odpowiedzi to "był piękny", ale Valaya nie zraża się i spokojnie pyta dalej. Z opisu na myśl przychodzą Galadriela i Thranduil. Pierwsza raczej niekoniecznie ma dzieci, lecz Thranduil ma syna, Legolasa.

Rohirrimka zastanawia się skąd tu elf, czy on będzie na balu i podpytuje o to Valayę. Przyszła rajczyni spokojnie odpowiada, choć młodej dziewczynie udaje się ją rozbawić kiedy pyta o zrywanie jabłek z drzewa...

Szybko się okazuje, że to rumak najbardziej się Rohance podoba! Herefra nawet odgraża się, że jeśli elfy mają tylko lasy, to ona tego konia uratuje, zabierze stamtąd i puści na równinę, by się wybiegał... choć mówiąc to - rozgląda się uważnie i nieco ścisza głos.

### Na dziedzińcu Oina

Valaya wysyła liścik do krasnoludów, by porozmawiać przed bankietem. Gundin i Ori wychodzą razem, ramię w ramię, co nie umyka uwagi niezadowolonego Oina.

\- I jak, synu. Podjąłeś już decyzję? Czyja będzie ta wyprawa?
\- Zrobię wszystko, by do wyprawy doszło - unika młodzian
\- Nie odpowiedziałeś na moje pytanie - natychmiast ostro ucina sprawę Oin
\- Sprawa jest oczywista - nieco znużonym głosem tonem odpowiada Ori - Od początku wymyślałeś, prowadziłeś i planowałeś.

Młodzian próbuje bezskutecznie przekonać ojca by poszedł z nimi, ale ojciec lekceważąco wyraża się o gościnie Żelaznej Stopy, przestrzega go nawet, że 'wkraczasz w kłębowisko żmij'. Ori wzrusza ramionami mówiąc, że będzie w towarzystwie przyjaciół, co przyciąga wzrok jego ojca do Bardinssona.

Gundin rozzłaszcza Oina, tak ten burknąwszy pod nosem - odchodzi.

### Rozmowa w pokoju Valayi

Valaya chce porozmawiać z Órim i Gundinem przed balem u króla by ich uprzedzić, że zostanie mianowana na królewską rajczynię. 

Gundin szczerze jej gratuluje. Ori jest nie lada zaskoczony. Myrtle patrzy na Valayę i dostrzega, że ta ma mieszane uczucia.

Krasnoludka przestrzega Óriego, że wspomniała Dáinowi o jego planach dotyczących wyprawy do Morii i żeby Óri nie był zaskoczony, że król o nich wie. Zapewnia Óriego o tym, że niezależnie od nadanego tytułu nadal będzie go wspierać w jego dążeniach.

W zamieszaniu pojawia się Undur, który forsuje drogę do pokoju Valayi, przyodziany w zasłonę jakową, na togę udrapowaną, wylewnie wita się z Gundinem i daje wyraz oburzeniu na wyzysk swego kompana! 

\- Przyznaj się przyjacielu, kto Cię chce oszukać!
\- Mam wrażenie, że wszyscy, ale na pewno nikt w tym pomieszczeniu!
\- Aha! Wszyscy! ... zaraz, nie w tym pomieszczeniu?

Zamieszanie potęguje się, aż Undur oskarża Oriego, Gundin rzuca, porozumiewając się wzrokiem z Orim:
\- Faktycznie, Ori, powinniśmy mieć kontrakt.
\- Zwłaszcza, jak masz takiego obrońcę.
Gundin targuje się z Undurem, że ten weźmie 17% jego działki, ale Gundin "zapewni mu transport". Undur zaczyna się targować z Orim o udział "jego klienta".

Herefra wzdycha, wychodzi i sprawdza w głównej sali czy pojawił się Rohirrima. Znajduje go od razu, w szykownym dublecie zdobionym w końskie podkowy. Valaya zostawia Oriemu klucze i wychodzi za Herefrą. Ori wysyła Undura po coś by umowę spisać i naradza się z Gundinem co począc z "nowym nabytkiem". Zaczyna się żonglowanie terminami kontraktu, udziałami w skarbie, 'w formie zlota i szlachetnych kamieni', 'z wyłączeniem broni'... 

| Niezrealizowane: wypytanie Gundina o Hobura Bardinssona, o którym w naszej rozmowie wspomniał Tharatrask. Czy jego krewny rzeczywiście zna sposób na otwarcie wrót Morii? Wg Gundina Hobur tych informacji nie ma.


### W głównej sali

Herefra i Heruling wymieniają uprzejmości i plotki. Rozmowa schodzi na tańce i napitki, wspomniane jest upodobanie Daina do miodu, Runiczny Destylat... Heruling zdradza niepewność imienia władcy, dostrzega też, że dziewczyna pochmurnieje w odpowiedzi na komplement "sama mogłabyś się obronić". Mężczyzna jednak nie daje za wygraną i drąży temat, aż rozchmurza dziewczynę. 

Kiedy wychodzą, wpada na nich krasnolud w opalonym a miejscami zwęglonym stroju, szukający... Ernwara i Oriego. Herefra podtrzymuje go i doprowadza do tej dwójki, Heruling podąża za nią.

- przepraszam, zawiodłem - wykrztusza posłaniec padając na kolana, wciąż głęboko wstrząśnięty. Gdy Ernwar słyszy o ogniu co buchnął aż z pieca, podskakuje i wyrywa się z osłupienia. Ori trzyma wtedy już glinianą tabliczkę z krótkim napisem w khazadzie. Wśród administracji Puszczanina, który ogląda posłańca, uspokaja go i wypytuje, daje się wywiedzieć pewnych rzeczy. Rzecz wydarzyła się pod Wieżą Kruków, płomień - a raczej płomienie - z góry buchnęły, fioletowe i niebieskie. 

Sama wieża, jak uświadamia sobie Ori, nijak żadnych ogni mieć nie powinna, ale kruki posłańcze. Wejść można z tunelu albo z ziemi. Z pytań Ernwara krasnoludy szybko orientują się, że żaden wypadek kuźniczy czy zwykły płomień tak wyglądać nie powinien, pada podejrzenie o alchemiczne materiały jakowe, ale co takowe miałyby robić w Wieży Kruków?
Ernwar pyta posłańca o imię: Sumri. Okazuje się, że Sumri *nie zdążył listu przekazać*, bo ledwo się pojawił Rugnisson wręczył mu tabliczkę i kazał uciekać. Jakby się całej sprawy spodziewał. Dwu z nim było, ale uciec nie zdążyli: jeden nie zdołał wstać, drugi ranny, nie zostało mu wiele życia.

Beleriand każe Sumriemu zapamiętać swe imię i znaleźć się jakby rzecz na tym się miała nie skończyć. Zdejmuje zeń - i przejmuje - obowiązek meldunku do pana Oina i każe mu o sprawie więcej nie wspominać. Jednocześnie mówi mu, że nie posyłał go tam wiedząc o sprawie i że Sumri dzielnie się sprawił i dobrą rzecz uczynił. Krasnolud ujęty traktowaniem mówi, że łgali ci, co mówili że ludzie honoru nie znają.

Na tabliczce napisane jest w khazadzie: w niebie cień, co Valaya rozpoznaje jako pieśń z Ered Mitrhin, Szarych Gór. 

[W niebie cień](pieśni/Gwathran/W_niebie_cień.md)


Heruling i Myrtle chcą się dowiedzieć co dalej. Wpierw pyta Myrtle, na co Ernwar rzecze, że może uzupełnić część opowieści, reszta jest w gestii Valayi. Starsza krasnoludka wprost mówi, że poemat może wyrecytować, ale lepiej by to uczynić po bankiecie. Heruling pół-żartobliwym tonem, charakterystycznym dla siebie, mówi, że on akurat chciałby się dowiedzieć co się tu wyprawia, lecz Ernwar poważnie przeprasza go, mówiąc, że w niewiedzy zaryzykował już czyimś życiem wczoraj i że póki co wystarczy i opowiada się za Valayą, co zyskuje mu pełne uznania spojrzenie tej ostatniej.

Kompania wyrusza na bal a Beleriand zahacza o domostwa pana Oina by przekazać wieści i zabrać Smyka, decydując, że pies będzie go teraz nieodstępował. BARDZO cieszy się z towarzystwa psa i czerpie zeń komfort, nawet z niepokoju Smyka. Ori poucza Oda, by ten drugiego ogara na balu dopilnował.

### Bankiet!!

Wielka Komnata Throra, w oddali tron, krasnoludy bogato i szykownie odziane. Komnata nieprzybrana, stołów bankietowych brak, króla nie widać.

Jeden z lokajów wskazuje windę. Z plecionki stalowej kosz z symbolem Samotnej Góry, w środku kolejny pałacowy sługa. Kosz nie trzyma się podłogi!

- Zapraszamy do Groty Durina, szlachetni!

Podłoga i świat ucieka spod nóg i jedynie skomplikowany krasnoludzki mechanizm "wie", co się dzieje... oraz, oczywiście, krasnoludy.

Ernwar zajmuje się Smykiem, Myrtle łapie Oriego za rękę, a Ori mamrocze pod nosem, nie ufaj lękom, nie ufaj lękom... 
Można dostrzec górników w ich szybach, minerały. Pęd jest duży, porównywalny do biegnącego konia. W pewnym momencie zrobiło się ciemno i słychać dziwne puffanie, aż ażurowa plecionka kosza dotarł do oświetlonego obszaru. 

Gondola - już bez pasażerów - rozpoczyna wędrówkę w górę. Pasażerowie zaś mogą spojrzeć na Grotę Durina. Jasnym jest, dlaczego zaszczytem jest znaleźć się tutaj.

To tutaj celebrowano odzyskanie Samotnej Góry. Teraz jednak komnata jest odbudowana i zdaje się być żywcem wyjęta z dawnych pieśni. Gigantyczna. Lewa ściana zdaje się otwierać na słońce, pełna gorących refleksów. Prawa strona traci się w księżycowych korytarzach, którymi do groty trafiają srebrne rozbłyski. Srebrne łańcuchy świecą jak mitrhil, co wszem wiadomo absurdem jest. Żyrandole i świece oświetlają komnatę.

Za podium dla orkiestry są dwa królewskie krzesła a za nimi Valaya domyśla się istnienia rajcowskich krzeseł.

Kominki są po lewej i prawej stronie, po prawej ze trzy nim zaczynają się schody. 

Co najbardziej zadziwia i zachwyca to dwa diabelskie młyny kręcące się nawet teraz, z gondolami. W sufit i podłogę wpuszczona jest na drobnych, srebrnych łańcuszkach istna kolejka linowa pater pełnych dań. W istnej rewii zapachów przewijają się ięsiwa jak i tort wyglądający na gigantyczne schody, nieskazitelnie białe, niczym z marmuru. Na samym szczycie widać drobne podobizny bohaterów spod Dol Guldur, co pokonali Malaryka.

Jest rozwiany włos Idril. Jest topór Brunhildy. Jest jaśniejąca laska, którą władał Eol... Jest tam również Myrtle co u hobbitki wywołuje radość. Jest tam również Ori, który poważnieje widząc to wszystko. 

Krasnoludy rozpoznają w diabelskich młynach drakomachinę. Tam rzeźby ukazują krasnoludzkich bohaterów walczących ze smokami. Gondole mają w sobie jakąś płynną substancję, która w ogniu... zmienia się, by niczym brokat łysnąć.

Ten ognisty spektakl wzmacniany jest przez kawałek ściany, lewej. Połowę jej szerokości zajmują szklane tafle, sięgające od parkietu do samego stropu. Tafle na 10 lub więcej metrów odbijają drachomachię i feerię złotych, ciepłych barw.

Kryształ czysto zadźwięczał, podrywając biesiadników spod stołów i zatrzymując taneczników w pół kroku. Oklaski rozbrzmiały jak rzęsisty deszcz. Kompania dzieli się. W centrum zostają Ori i Myrtle.  Wśród tłumu krasnoludów widać też jednego elfa? Życzenia, gromkie brawa wciąż trwają jeszcze długo nim goście wrócą do poprzednich zajęć. Ori patrzy na Valayę prosząco, na co ta podchodzi bliżej, służąc wsparciem. Ernwar krzyczy: Brawo Ori! Brawo Myrtle! Brawo Brunhilda, Odo, Idril i Eol!

Majordomus w zacnym berecie wita szanownych gości i prowadzi ich na ich honorowe miejsca, opodal wysłanników Gondoru i elfów. Miejsca po lewej mają drachomachię. 

Podkowa stołów ma charakterystyczną wyrwę zaraz obok podium orkiestry, tak, że można właściwie mówić o dwu literach L, stykających się koło podestu królewskiego.

Valaya, Ori i Myrtle
Heruling i Herefra
Ernwar
Gundin na rogu, na krześle, które wydaje się nie być od kompletu... co Gundin kwituje myśląc "jak i ja". Miłe to nieco, bo mniej się wyróżniać można.

Po drugiej stronie jest Arcirias, pięknie i uczuciowo kłania się elf o długich i gęstych blond włosach i czystej cerze. Czyżby Legolas, książe z krwi Thranduila? Czoło księcia 

Arcirias przedstawia się jako poddany króla Gondoru i dobry znajomy Przewodniczącego Rady Czarodziejów i pełniący funkcję Kronikarza Pierwszej Klasy, przedstawia też Legolasa i potwierdza jego tożsamość.

Legolas ciepło wita kompanię, Myrtle od razu, wprost pyta o Idril. Okazuje się, że elfka jest doradczynią króla Thranduila, co zatrzymuje ją na dworze. Elfka przekazuje pozdrowienia. Rozmowa schodzi na trud żywota radców, zwłaszcza wielkich królów a elf dziękuje Oriemu za to, że Idril wróciła do domu. Ori jednak nie przyjmuje podziękowań, składając zasługę u stóp Idril właśnie. Legolas i Ori dzielą się uwagami o nieobecnej, zgadzając się, że zaiste, jest ona upartą osobą.

Ori dokonuje prezentacji kompanii. Arcirias zagaja:

\- Wygląda na to, drodzy moi, że ciekawe rzeczy Wam się przytrafiły po drodze. Muszę rzec, że niesamowity smutek odczuwam, kiedy słyszę, że Wasza wyprawa nie doczekała się takiej sławy jak wyprawa Thorina Dębowej Tarczy.
\- Nie nam to oceniać - chłodno kwituje Valaya
\- Nikt nie zamieszkał w Dol Guldur panie. Pozwól, że to będzie moją odpowiedzią.

Myrtle proponuje swoją opowieść, Arcirias daje jej jednak poznać lekceważenie, co uraża hobbitkę i chłodzi atmosferę. Gdy w końcu Myrtle opowiada, opowieść celowo pozbawiona jest niektórych szczegółów. Nienawykła do tego hobbitka, znajduje pomoc w Puszczaninie, który pomaga z akompaniamentem... zagłuszając kluczowe fragmenty. Arcirias kończy niezadowolony, ale Myrtle nie przepada za nim. 

Arcirias próbuje wciąż poznać sekret, który pozwolił bohaterom dostać się do Dol Guldur mimo otaczającej go magii. Kronikarz przywołuje swego pryncypała, mówiąc, że ten musiał użyć potężnej magii. Ori odpowiada pochlebstwem, że Saruman wypędzał wielkie zło, a to z czym oni walczyli nie dorastało do pięt takiej sile.

Ernwar znienacka pyta kronikarza o los mędrców z Fornostu. Zaskoczony pytaniem Arcirias wywodzi, że nie można stwierdzić go z pewnością, ale że faktycznie Cień mógł ich ścigać i tezy takiej odrzucić nie należy, ale że pewności nie ma, bo magia to nieprosty temat. Co Beleriand podchwytuje mówiąc, że zaiste temat niełatwy, ponury, nie na teraz i może zatem należałoby go zmienić, co pozostali skwapliwie podchwytują. Arcirias jednak to ignoruje i wykorzystuje imię Sarumana Białego, podkreślając, nie pierwszy raz tego wieczoru, iż to jego pryncypał, a w dodatku czarodziej. Myrtle skwapliwie i z szczerą ciekawością zapytowuje, czy "jak Gandalf", co przytyka Arciriasa na moment. Beleriand dodaje, czy jak Radagast? Co wywołuje aż zatchnięcie się, a potem wywód o starszeństwie pośród czarodziejów, kolorach i zwierzchnictwie.

Utarczka z Arciriasem przybiera na sile, ujawnione jest, że uzyskał on słowo Legolasa i że dość śmiele sobie poczyna wobec księcia, co oburza Puszczanina. Elf jednak dziękuje chłopakowi i mówi, że jedynie do końca wieczora jego słowo go obowiązuje.

Gundin nieco zbyt głośno prosi Ernwara by ten podał dalej do Oriego, czy można rozmawiać o Khazad Dum.

Pojawia się Dain w odświętnym szarym wamsie. Majordomus daje sygnał i krasnoludy kłaniają się.

Król zagaja:
\- Szanowni, moi drodzy. Zebraliśy się tutaj wszyscy wiecie po co. Ale dobrze, że jesteście tutaj. Mamy komu dziękować i część z tych osób znajduje się z nami tutaj. Część z nich znamy dobrze, choć pokazali się w nowym blasku, a część tylko z opowieści. Ori, synu Oina, pokaż swą twarz.

Ori, z pewnym wahaniem wynikły z niepewności jak najlepiej spełnić życzenie władcy, podchodzi bliżej. Na twardej twarzy Daina widać poruszenie. Król kontunuuje:

\- Myrtle, niziołcza kobieto z Tuków. Okaż swą twarz. 

Myrtle kłania się królowi i obecnym. 

\- Idril z elfickiego królestwa, Eolu z elfickiego królestwa, Brunhildo z leśnych ostępów, z Puszczańskiego Grodu. \- Ernwar prostuje się, Myrtle dumnie unosi głowę, Odo siada i wypuszcza cichy skowyt. Ori przyklęka, głaszcze psa po głowie i wstaje. Król kontynuuje dziękując, że taka drużyna zewsząd zebrana powstrzymała wojnę. Król podchodzi do bohaterów, zaskakując ich nielicho! Ori chce klękać, Myrtle chce iść w jego ślady, lecz król powstrzymuje ich chwytając ich za przedramiona. \- Przyjmijcie nasze dzięki. 

Ori i Myrtle wzruszyli się.

\- Dobrze, że mogę podziękować Ci osobiście, Ori, synu Oina. Dobrze, że nie muszę mówić o Tobie jako o bohaterskim przykładzie. Erebor jest Ci wdzięczny! I Tobie, Myrtle! Klęknijcie!

Ori chce klękać, ale słyszy wtedy zniecierpliwione "nie Ty!". Posłuszna rozkazowi króla... klęka cała sala, także Ernwar, czy - jako jedna z pierwszych - Valaya.

Dekretem królewskim zabawa.

\- Valayo, córko Badoca, pokaż swą twarz! - król nie skończył jeszcze! \- Valayo, królewskim przywilejem i przyjemnością jest uznawać zasługi tych, którzy przyczynili się do budowania Ereboru. Krasnolud może budować toporem, kilofem, młotem, ale szczególnie trudno budować jest... cóż, gdybym wiedział czym, nie musiałbym Cię prosić o pomoc! Valayo, córko Badoca, czy przyjmiesz ode mnie tytuł rajczyni i przyjmiesz ciężki obowiązek dźwigania na sobie interesów Samotnej Góry! Valaya jest weteranką na dzielnicy Kruszonego Kamienia. To ona przyprowadziła do nas bohaterów Ereboru. Kiedy będziecie mieli do czynienia z nią, to tak, jakbyście mieli do czynienia ze mną! A tego przecież - zaśmiał się władca - byście nie chcieli, he, he, he. 
Valaya uśmiecha się, a na koniec otwarcie śmieje.

Zaprosiwszy Valayę na jej nowe miejsce obok siebie, król życzył gościom smacznego i wzniósł toast za Valayę, jak wcześniej za Oriego i Myrtle. Valaya wypija pierwszy kielich z kompanami, przeprasza ich i rusza do stołu radców. Wchodząc po kolejnych stopniach zaczyna widzieć coraz więcej i więcej, dostrzega innych rajców, którzy w końcu przestają nad nią górować - jedno z tych krzeseł należy do niej.

Bombur zagaja do Herefry: 

\- wierzchowce, ja?! patataj, ja?!  
\- Ty! odpowiada zwięźle Herefra, a Ernwar się krztusi słysząc to.  

Po kolejnych wymianach okazuje się, że Bombur pragnie Herefrze coś pokazać. Złotowłosa pełna jest złych przeczuć, w końcu jakież wierzchowce można tu zobaczyć? Gruby krasnolud prowadzi ją ze sobą, aż dostrzec można włochaty zad, rogi... i mordę żującą obrus. Bombur łamanym Westronem próbuje powiedzieć, że chce, by Rohirrimka ujeżdżała jego kozice!

Sytuację próbuje rozbroić Ori, robiący za tłumacza.

\- Ori - perswaduje krasnoludowi oburzona dziewczyna \- przekaż swojemu koledze, że nie będę na tym jeździć! 
\- on kocha swoje...  
\- kozice! a ja kocham swoją godność!  
\- wiesz, one są bojowe...  
\- bojowe? ona je stół!  


Heruling kieruje się ku stołowi królewskiemu mimo gromiących spojrzeń, u góry schodów krzyżują się przed nim topory strażników. Akustyka pomieszczenia przemyślnie skrywa dźwięki z góry schodów. Ernwar rusza powiadomić przyjaciółkę.

Heruling prosi Daina o wstawiennictwo za sprawą swego krasnoludzkiego przyjaciela, przerywając rozmowę króla z Balinem. Mistrz Koni zręcznie podkreśla, że już ma topór na szyi będąc bezbronnym, lecz Dain nie wypadł sroce spod ogona i obnaża fortel, każąc intruzowi się streszczać. Ten też tak czyni, przedstawiając sprawę Kurtiego z Kornissonów i z pewną nerwowością prosi o pozwolenie wkroczenia na teren starożytnego królestwa krasnoludów.

\- Chcesz wkroczyć do Morii?  
\- Nie, nie chcę, ale jeśli będę zmuszony, wolałbym to uczynić za Twoim pozwoleniem  
\- A jeżeli go nie dostaniesz?  
\- Wolałbym go dostać panie. Proszę bardzo.  
\- Herulingu z Dunharrow, zastanowię się. Ale nie wchodź pod topory moich gwardzistów, drugi raz mogę ich nie powstrzymać.  

Widząc powracającego Herulinga, Herefra wymawia się Bomburowi i rusza ku niemu, Ernwar wraca z nią. Tymczasem Balin odsyła służbę i prosi Valayę o zaproszenie kompanii, skoro temat i tak, niczym diabeł, wyskoczył z pudełka.

Sprzeczka u dołu. Zraniona Herefra. Heruling; Nie tobie się będę tłumaczył pani (do Valayi). Król czeka.

Król odzywa się natychmiast, jak tylko Drużyna pojawia się u szczytu schodów:  
- ktoś Was ubiegł z prośbą  
- jak to ubiegł  
- Heruling poprosił o pozwolenie na wstęp do Morii  
Herefra zawstydzona spuszcza głowę  
Ernwar ją niemo pociesza  
Ori mówi, że nic o tym nie wiedzieli  
Król - mamy ważniejsze rzeczy do omówienia  
Myrtle: Ori, no mów.  
Ori: 
Panie, całe moje życie zawsze marzyłem o tym, aby... Jak byłem młodym krasnoludem to były marzenia ściętej głowy. Ale zdarzył się cud: mała drużyna najsilniejszych krasnoludów w historii odzyskała dom. Ale jest jeszcze jeden dom do odzyskania. Pragnę, aby kiedyś mogliśmy się cieszyć z tego, że Mrok już tam nie zalega. Kiedyś chciałbym udać się... spróbować. Wiem, że to fantazja, ale uwierz mi panie, to z początku mej wędrówki tak było. Cała podróż i to, co stanęło na mej drodze, pozwoliło mi dorosnąć, ale pragnienie nie odeszło a jedynie się wzmogło.  
\- Rozumiem Twą motywację. Masz serce na właściwym miejscu Ori, tak jak mówiła Valaya. Co sprawia, że uważasz, że powiedzie się tam, gdzie mnie i innym się nie powiodło. Mogliśmy dokonać szturmu, mieliśmy przy boku wielu krasnoludów. Ale nie zrobiliśmy tego. Co sprawia, że sądzisz, że Tobie się uda?  
\- Nie uważam panie, że sami odbijemy Morię.   
\- Co chcecie zatem uczynić?  
\- Uważam, że kluczem do odzyskania Morii jest zaginiony artefakt, topór Durina.  
\- Topór Durina. Chcecie go odzyskać, z trzewii Morii. I przekazać go mnie?!  
\- Oczywiście panie. Tylko król powinien go nosić.  
\- Nie wyglądasz na pochlebcę Ori. Wyglądasz na wojownika. Powiedz mi - jak jeden woj do drugiego - swój taktyczny plan.  
\- Odnaleźć tajemne wejście do Morii... Za pomocą artefaktu. I... postarać się... postarać się, wytropić miejsce w którym zalega ów topór. Wędrować jak najszybciej w czeluściach. Jeżeli... coś się będzie działo. W razie czego. Sam stanę i zabezpieczę odwrót.  
\- Nikt nie wątpi w Twoje męstwo. Jaką drużynę przyjmiesz?  
\- Panie, mój ojciec od lat chciał tego dokonać.  
\- Wiem - król ciężko oznaczył tę kropkę nim kontynuował - Czy to Twój ojciec dobiera drużynę z którą wyruszysz?  
\- Nie. Moich kompanów widzisz przy mnie panie. Powierzyłbym im całe swoje życie. Nie udałbym się tam bez nich.  
  
Rozmowa toczy się dalej i król bez problemu obnaża wątpliwości Oriego dotyczące przywództwa wyprawy i wyjścia z cienia ojca. Pyta też wprost Oriego.  
  
\- Waham się. Nie z racji na umiejętności mego ojca. Nie chcę zabrać moich towarzyszy.  
\- Ori, czasami musisz zawieść swoich bliskich. Odpowiedzialność to ciężkie brzemię. Cenię Twoją odpowiedź ale jeszcze nie zdecydowałem. Jak liczna będzie Twoja wyprawa?  
\- Tylko nieliczne osoby. Ci tu obecni. Im mniej, tym lepiej.  
  
\- Gundin, czyż nie?  
\- Tak panie.  
\- Moi doradcy mówili mi o Twoim rodzie.  
\- A co takiego mówili, panie? niechętnie  
\- Opowieści o tym, jak opuszczano Khazad Dum. Choć wiele jest wersji tej opowieści, żadna nie jest pochlebna dla Twojego rodu. Jestem ciekaw, chcesz stanowić część istotnej historii, nie ma to z tym związku? Mów.  
\-   
\- Mógłbym ci powiedzieć pewnie tylko to, co już słyszałeś, a to czego się domyślam, musiałbym potwierdzić.   
\- O ile żaden z moich doradców nie zdołał dowieść ponad wszelką wątpliwość co dokładnie się stało w Khazad Dum, ale powinieneś móc coś powiedzieć o swoim bracie, z tej _drugiej_ wyprawy, czyż nie?  
\- Wybacz mi panie, usłyszałem o tym niedawno, nie mam pojęcia co Hobur miał na myśli mówiąc, że wie. Nie wiem, czy to nie szalej przezeń przemawiał.  
\- Jak mam Ci zaufać Bardinsson? Daj mi jeden dobry powód.  
\- Panie, chcę się dowiedzieć ile prawdy jest w sprawach mego rodu. Czy zawsze będę musiał chodzic ze spuszczoną głową jako Bardinsson?  
\- Moi doradcy wskazują na zalety wychowania i jego wady. Mam uwierzyć, że jesteś jedynym nieskazitelnym klejnotem całej swojej rodziny?!  
\- Gloin mógłby opowiedzieć o tym, jak się spotkaliśmy. Tak znamienity krasnolud jak Ori mi zaufał. To powinno coś znaczyć panie.  
\- Gundin jest mym przyjacielem panie - wtrącił Ori.  
\- Przyjaciele często potrafą widzieć to czego nie są w stanie widzieć osoby obce, nawet dobrze poinformowane. Czasem natomiast widzą sprawy w krzywym zwierciadle. Ale i to wykorzystam! Pytam Was, o jedną rzecz, która powinna przemawiać za uczestnictwem tego tu krasnoluda w wyprawie.  
  
Ernwar jakby tylko na to czekał, słowa spłynęły natychmiast, jakby już wcześniej chciał je rzec, tylko się wstrzymywał:  
\- Gundina nie skarby motywują panie. Gdyśmy stali na skarbie cenniejszym od niejednego królestwa, chciał go zakopać i zwrócić właścicielom, uczynił to tak honorowo, że dobrowolnie dałem mu słowo.  
\- Valayo?  
\- Myślę, że jego spokój, opanowanie, przydadzą się. A potrzeba serca będzie wskazana w tak trudnej podróży.  
\- Myrtle z Tuków?  
\- Jest... dobrym wojownikiem.  
\- A kogo pokonał?  
\- Trolle  
\- Tak? Gundin, ile pokonałeś trolli?  
\- A ostatnio, czy w ogóle?  
\- Ostatnio.   
\- Ostatnio to trzy  
\- No nieźle. Herefro?  
\- Misja, której pragnie podjąć się Ori definiuje każdego krasnoluda. To, że ta tajemnicza persona - Rohanka wskazuje Gundina - pragnie towarzyszyć Oriemu wydaje mi się najlepszą odpowiedzią.  
\- Ori, masz jeszcze coś do powiedzenia o swoim kompanie?  
\- Rzemieślnictwo panie: kiedy troll głazem nam wyjście przywalił, Gundin młotem ją skruszył.  
\- Hmmm, władasz młotem?  
\- Tak panie. Młotem, z kopalnii tak jakoś wyszło.  
\- Niewielu jest młotodzierżców. A jak czemuś porządnie przypieprzyć... to nie wstanie.  
  
Król zamyślił się przez moment, ważąc słowa wszystkich, te padłe wcześniej i teraz. W końcu zdecydowanie spojrzał na Bardinssona mówiąc:  
  
\- Dobra Gundin. Nie mam z Tobą swady. Nie jestem stara krasnoludzka matrona i nie będe zajmować się plotkami. Wykuwaj swoje przeznaczenie jak tam chcesz, mnie nic do tego.  
  
\- Valayo, czy jakbym na Twoje miejsce znalazłbym kogoś innego, przyjęłabyś taka ofertę?  
\- Nie wiem panie, na czy ona polega?  
\- Zważ na drugą wyprawę. Czy mam mówić wprost?  
\- Nie panie. Pójdę z mymi towarzyszami.  
\- A gdybym Wam rzekł, że nawet jakbym ja tam poszedł, w doborowej kompanii, to nie uważałbym, że wrócę?  
\- Z Tobą w ogień panie (Ori)  
\- Nawet gdy mowię, że niepewnym powrotu, nadal chcecie iść?  
\- Panie, nie uważam, że jesteśmy potężniejsi niż armia, która wydrze hale orkom. Ale możemy tego dokonać.  
  
Pytanie co z Herulingiem: Valaya zbada temat  
  
nie wyślę Was nie sprawdziwszy tego i owego  
sąd Durina!  
  
sąd Durina to mityczna stara, nieużywana dziś praktyka. Prosty test jaki Durin dał pierwszym synom. Przynieście mi tego, co żądał najbardziej. Ten, który przyniósł mu to jako pierwszy, miał uzyskać jego łaskę. Wyprawa zaprowadziła ich na krańce świata, dopiero po niej zrozumieli swą słabość i dostrzegli, że w jedności siła.  
  
topór Durina, znaczy Daina, poleciał w gondolę a Arcirias spuścił ze smyczy Legolasa  
  
  
| PD 18 i PR 25 (za obie sesje, tę i poprzednią) 


### Sesja 6: Sąd Durina

Zdjęcia

Pierwsza do akcji rzuca się Herefra - biegnie nim król skończy jeszcze mówić, kierując się ku najbliższej kolumnie wspierającej całą grotę Durina! Dziewczyna tnie w drodze suknię, by móc się lepiej poruszać i od razu poczyna wspinać się po misternej plecionce wokół kolumny wykutej. Wkrótce dociera do miejsca, gdzie kolumna łukiem przebiega pod sklepieniem groty!

Ori wydaje rozkazy, przekrzykując nawet króla w pierwszych chwilach. Zachęcony przezeń Bombur spuszcza kozice, ale gdy młody krasnolud próbuje jednej pobiec naprzeciw niczym taran wpada nań królewski przyboczny. Wskutek zderzenia Ori ląduje na ziemi, ale gdy gwardzista chce się doń dostać Odo staje mu naprzeciw, a potem potężnym atakiem zastrasza Kamiennego Gwardzistę! Paszcza ponad tarczą i przyboczny nagle musi całkowicie skoncentrować się na obronie!

Ernwar rzuca się ku drachomachii, Smyk podrywa się i biegnie za nim. Beleriand zwalnia nieco, by królewski gwardzista opodal drachomachii skoncentrował się na powstającym i dołączającym krewnym Herefry, widzi jednak, że Heruling z Dunharrow również planuje swój ruch mając jego na uwadze! Gdy przyboczny rusza w ich stronę młodzian wskazuje na Rohirrima i rzuca: "on wzbudził gniew króla!" po czym nadkłada drogi wskakując na stół obok księcia elfów. 

- Panie - uprzejmie skłania się Legolasowi, a gdy ten równie uprzejmie odkłania się, dobywa Elendilmira i zakłada go na głowę. Niedowierzanie koloruje twarz księcia, a Beleriand skacze ku drachomachii!

Heruling gwiżdże donośnie, co rani czułe uszy krasnoluda i chwilowo obniża jego gardę. Doświadczony Rohirrim wykorzystuje to by wyrwać mu topór i go obalić, po czym rusza pędem ku ścianie!

Na drodze staje mu jednak Myrtle z Tuków... a raczej z orkiestry, wśród której się schowała wyskakuje nań z ukrycia, wgryzając się w jego ramię do krwi i chwytając go za oczy! Heruling chwieje się na nogach.

> Myrtle PC

W starciu oboje obrywają tarczą od gwardzisty, Heruling rzuca się plecami na niego, przez co Myrtle obrywa od tarczy i spada na ziemię. Podnosi się chwiejnie, ale szybszy od niej Rohirrim dobiega do ściany i łutem szczęścia rozcina toporem właściwe ogniwo! Żyrandol spada na jeden ze stołów podczas gdy Heruling zostaje wywindowany dwa poziomy w górę! Ciśnięty weń mściwie przez Myrtle kawałek tortu - chybia!

Legolas niczym wiatr wyprzedza Puszczanina! Próba wprawienia go w osłupienia spełza niestety na niczym: 

- Chylę czoła przed dziedzictwem Numenoru, jednakże złożyłem przysięgę. 
- Zapraszam więc - Enrwar nie traci kontenansu
- Skorzystam - uśmiecha się elf 

We wspinaczce elf zręcznie wskakuje na drachomachię, Ernwara natomiast zaskakuje karkołomny manewr gwardzisty z piętra wyżej - ciśnięty przezeń młot powala młodziana na kolana! Elf więcej ma szczęścia i unika większości ciosu, wprawnie lawirując po wagoniku.

Z dwu przybocznych przy królu stojących jeden zajął się Orim, jeden zaś do Gundina rzucił "Stój!" i ruszył szybko w jego stronę. Gundin ni drgnął, dając okazję Valayi do zejścia na dół i ruszenia w stronę królewskiej windy, ukrytej za kotarą. Winda owa potrzebowała jednak klucza! 

- Spokojnie! - uniósł w górę dłonie Bardinsson - Nieuzbrojony jestem!

Gwardzista począł zbliżać się nieco spokojniej

- Porozmawiajmy o tym z królem - zupełnie swobodnym tonem i wciąż z rękoma w górze ciągnął Gundin, co wywołało pewien popłoch u strażnika 
- Nie mogę Ci na to pozwolić!
- Ach. No cóż - rzekł Gundin, pięścią obutą w wykutą z Orim rękawicę spuszczając niczym głaz na hełm niespodziewającego się ataku krasnoluda. Hełm rozbrzmiał niczym dzwon, rozpękł się na pół i spadł, razem z zamroczonym ciosem gwardzistą, na ziemię.
- Panie - z szacunkiem zagaił Bardinsson czyniąc krok w stronę króla i ponownie wznosząc ręce ku górze, w tym ową w straszliwej rękawicy
- Gundinie - rzekł z pewną rezerwą Dain - sięgnąłbym po topór, ale... - wzruszył ramionami
- Nie musisz się niczego obawiać z mojej strony panie - rzucił - wedle wielu, zgoła fałszywie - Gundin

I obaj zwarli się w zapasach!

- Widzę, że to dla Ciebie nie pierwszyzna Panie - schlebił Gundin
- Nie czaruj tylko walcz - rzucił monarcha, reagując na zwód Gundina i broniąc swej nogi, podczas gdy chytry Bardinsson zerwał klucz i rzucił krzycząc - Ori! Łap! 
- Odo - zagrzmiał Ori podnosząc się i biegnąc ku coraz bliżej będącej kozicy Bombura! 

Odo odbił się od tarczy na której się opierał i wspaniale zaaportował klucz, niosąc go Valayi. Krasnoludzka matrona niewzruszenie otworzyła windę, weszła do niej z psem, zamknęła się i ruszyła w górę, zapoznając się z pulpitem sterującym wszelkimi mechanizmami. Dość szybko odkryła jak wyjechać na dość wysoki - bo już czwarty! - poziom. Ostatni poziom pod kuchnią i najwyższy w grocie, jeden poziom poniżej topora! Jadącej windzie towarzyszyły srebrne dzwoneczki, zwykle obwieszczające nadciągającego króla. 

Herefra wspaniałym sprintem rozpędza się i z okrzykiem Rohan wskakuje na drachomachię... 

CDN

![](../grafiki/notki1.jpg)

![](../grafiki/notki2_1.jpg)

![](../grafiki/notki2_2.jpg)

![](../grafiki/GrotaDurina.jpg)

![](../grafiki/drachomachia_i_patery.jpg)


## Faza Drużyny w Ereborze

Kompania spędza dzień na odpoczynku i przygotowaniach do zasadzki na Cienistego Wędrowca.

**Wszyscy do wydania: 95 PD oraz 103 PR**

Przedsięwzięcia:

. Gundin i Herefra przeszukują Głębokie Sztolnie pod kątem miejsce do bitwy.
. Ernwar i Valaya w Komnacie Mazarbul szukają informacji o Cienistym Wędrowcze.
. Myrtle i Ori wyzbywają się złych myśli (punktów Cienia).

### Gundin


### Valaya


### Herefra

### Óri


### Myrtle


### Ernwar

## Sesja 7: Przygotowanie zasadzki, pierwsza noc, wieści dla Herefry i Oriego

## Sesja 8: Zasadzka, druga noc, wieści dla Myrtle, trzecia noc, Valaya kontra smok, Ori i Gundin

| PD 10 a PR 12 (albo na odwrót?)

## Sesja 9: Zasadzka, czwarta noc, wieści dla Ernwara i Valayi, orcza horda

| PD i PR za tę sesję będzie dane potem.


## Sesja 10: walka!


