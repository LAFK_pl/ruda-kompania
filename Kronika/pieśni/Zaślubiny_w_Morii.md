# Zaślubiny w Khazad-Dum kiedy go Morią jeszcze nazywano

Moria - straszliwe to miano,  
Zguba Durina, szeptano,  
Nieprzeliczone istnienia,  
Żywcem tam pogrzebano.  

Tragedia to długobrodych,  
Twardych dzieci Aulego.  
Khazad-Dum odzyskanie,  
Marzeniem jest niejednego.  

Rzecz ta dzieje się w czasach,  
Gdy Morią wciąż się nazywa  
Stolicę krasnoludów,  
Siedzibę samego Durina.  

Balrog tam wciąż przebywa,  
I orków czarnych plaga,  
Komnaty cudne bezcześci,  
Tam właśnie trafia Wyprawa.  

  ---  ---  --- 

W komnacie gdzie niebo ma się pod stopami,  
Gdzie przemyślny ku niebu porwie Cię mechanizm,  
W dziele majstrów Beleghostu i Nogrodu też,  
W Lazurowej wspaniałości co zapiera dech  

Tam w dorosłość wkroczył Ori, syn Oina, gdy  
Na wniosek Valayi Mądrej łańcuch hierarchy  
Od Thikonrila przyjął, ciężki, szczerozłoty  
I uroczyście ceremonię odprawił zaślubin.   

Kurti, czy zechciałbyś, rzecze Thikonril,  
Ze spokojem w głosie jakby nigdy nic,   
do krewniaka, co do Morii poszedł sam.  
Ten zaskoczon co niemiara,    
a zaszczycon dwakroć bardziej,   
że jak obecnością świadczył godność ceremonii,  
to mu głos aż z dumy i radości drżał.  

Sygnet mego rodu na swym palcu masz  
Bądźże świadkiem mym Gundinie Bardinsson  
Valayi słowom odmówić nie umiał  
Skrępowanie język w kołek obróciło mu  
"Wszystko cacy" rzekł a uśmiechy obecnych  
same na to poszerzyły się   
"i niech się szczęści parze młodej" /x2  

W komnacie gdzie niebo ma się pod stopami,  
Gdzie przemyślny ku niebu porwie Cię mechanizm,  
W dziele majstrów Beleghostu i Nogrodu też,  
W Lazurowej wspaniałości co zapiera dech  

Los kazał nam długo na się czekać  
 -- tu drugie zdanie Valayi --  
Tak prostymi słowy zaczęła Valaya  
Thikonril to jednak wojownik-poeta!  

Valayo mądrość Twa w dwójnasób jak moja odwaga  
Niech wszyscy słyszą że ślubuję być z Tobą  
po kres mych dni - tarczą, podporą, ostoją  
Głębiej niż korzenie ziemi niech pamięć o nas sięga.  

W komnacie gdzie niebo ma się pod stopami,  
Gdzie przemyślny ku niebu porwie Cię mechanizm,   
W dziele majstrów Beleghostu i Nogrodu też,  
W Lazurowej wspaniałości co zapiera dech  

Tam hierarcha Ori w obecności świadków  
słyszał Aulego uszami, ustami błogosławił  
Tam Valaya wybrankiem swoim uczyniła  
Narzeczonego swego - Thikonrila  

A była to uroczystość jak dotąd żadna inna!  
W Khazad-Dum, gdy tam była sama Zguba Durina!  
Księżycogryz! I orków bandy nieprzeliczone!  
Podczas wyprawy przez wielu nazwanej szaloną!  

Tam w pięknej tajemnicy Róży Wiatrów,  
Z płytkami z nieba zrobionymi u stóp,  
Wśród dziedzictwa Nogrodu i Beleghostu,  
Związano więzi szczepów z Błękitnych Gór,  

Do wtóru skrzypce grały Myrtle Tuk,  
Słychać było Szmer Cisu i głosów chór,  
Bo wśród drużyny i rodziny swojej stali  
Thikonril i Valaya.  

W komnacie gdzie niebo ma się pod stopami,  
Gdzie przemyślny ku niebu porwie Cię mechanizm,  
W dziele majstrów Beleghostu i Nogrodu też,  
W Lazurowej wspaniałości co zapiera dech  

Tam związała się Valaya z Thikonrilem  
I poświadczono to Elendilmirem  
Wrogom tej dwójki zgryzota i biada  
Bo stoi za nimi niemała gromada  
Przyjaciół, krewniaków,   
ze wszystkich stron świata  
Tego ślubu nawet Moria   
powstrzymać nie mogła!  
