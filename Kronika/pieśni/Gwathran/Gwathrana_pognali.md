# Gwathrana pognali

Opowieść o tym, jak Ruda Kompania Gwathrana z Ereboru pognała, tak, że gad sromotnie pierzchać musiał i sprowadzone przez siebie orki na pastwę Dainowych hufców porzucił.

Nuta: https://www.youtube.com/watch?v=CJ2k4JWFMV0

Gundin Bardinsson jest honorowy.  
"Więcej to warte niż wiele królestw  
Brosza ta rzeczą jest Rugnissonów,  
Potomków spotkasz to im proszę zwróć."  

Natychmiast mu to przyobiecałem,  
Ujął mnie tymi prostymi słowy,  
O Rugnissona więc zapytałem,  
A w nocy tutor w ogniu poniósł śmierć.  

## Valaya

Dain radczynią wczoraj ją mianował,  
Dziś ze smokiem magią się już mierzy,  
Zważaj na nią, ważkie są jej słowa,  
Co zaśpiewa to hufiec powtórzy  

Ją na pierwszy cel Gwathran obiera,  
Runy jej wszak przed nim skarb ukryły,  
Valayi monstrum nie doceniło,  
Słaby punkt smoka ona odkryła - 0:47  

_ wstawka do 1:00  _  
Gwathrana pognali x2  
Gwathrana - pogna-li z E-reboru tak  
Gwathrana pognali x2  
Gwathrana - pogna-li z E-reboru tak  

## Ori

Ereboru Strażnik, pierworodny  
Oina Gloinssona tego z Trzynastu,  
Kompan z niego zawsze niezawodny,  
W bitwie, w karczmie czy na jakim szlaku  

Myrtle złapał kiedy smok nią cisnął,  
Valayę sprzed smoczej paszczy wypchnął,  
Smoka prawie miał - ten musiał pierzchnąć  
Rudą Kompanią Ori dowodził  


## Herefra!

Piętnaście wiosen i złote włosy,  
Uwielbia konie i Badocsdottir,  
Miecz i tarcza wciąż dla niej za duże  
Pozna obie gad na własnej skórze  

Mieczem jej wuja, przebija łuski,  
Jucha jadem już stal ostrza trawi,  
Herefra ostrze w kałużę rzuca,  
By matki mieczem smokowi poprawić - 1:51  

_ wstawka do 2:15 _  
Gwathrana pognali x2  
Gwathrana - pogna-li z E-reboru tak  
Gwathrana pognali x2  
Gwathrana - pogna-li wi-działem to ja  
Gwathrana pognali x2  
z Ereboru gad nocą pierzchać musiał  

## Myrtle

Dżem czy marmoladę przyrządzi,  
Gra, śpiewa, nie straszny jej upiór  
Kompania z niej jest pierwszorzędna  
Myrtle Tuk przeżyła Dol Guldur,  

Herefrę swym ciałem zasłoni,  
Łuk Uszatek strzały ma celne!  
A jeśli nawet coś nabroi,  
To na pewno zrobi to z serca! - 2:34  

_ wstawka do 2:47 _  
Gwathrana pognali x2 Gwathrana - pogna-li z E-reboru tak  
Gwathrana pognali x2 Gwathrana - pogna-li wi-działem to ja

## Gundin!

Cios jego młota smoka powstrzyma,  
Prastara to broń, kunsztowna, cudna  
Gundin obraca nią niezawodnie,  
Bronią dlań nawet są żyły wodne.  

Gwathrana ryk bólu uszy przeszył,  
Gundina cios go nieźle przestraszył,  
Bardinssonów dni chwały - nie przeszły,  
Gundin młody jest i wiele przed nim - 3:17  

_ wstawka do 3:38 _  
Gwathrana pognali x2  
Gwathrana - pogna-li z E-reboru tak  
Gwathrana pognali x2  
Gwathrana - pogna-li wi-działem to ja  
Gwathrana pognali x2  
Gwathrana - pogna-li z E-reboru tak  

## Adelina i finał

Gondoru księżniczka jedni rzekną,  
Ohtarina córa inni szepcą,  
Adelina zbawcze wieści wiezie,  
Smok? Czemu nie? z uroczym uśmiechem  

Taką właśnie Rudą Kompaniję  
Ereborską nocą ja ujrzałem,  
Sromotnie Gwathrana pogonili  
By ich sławić to Wam zaśpiewałem!  
