Zmrok wkrada się do Ereboru. Płomień śmierć niesie. Wg kaprysu. Wg przeznaczenia.
Opowiem Wam historię o smoku innym niż Smaug, który pod Erebor przywędrował. Smoku, co ród Rugnisson klątwą swą niemal unicestwił i podczas Dainowego Sąd Durina zwycięską Herefrę napastował i Oina domostwo ogniem trawił. I o jego klątwie.

## Zmrok wkrada się do Ereboru

Pożar dziwaczny na zboczu góry,  
pasterskie stada kompletnie trawi,  
Rugnissonowi wstępu odmawia,  
Choć król z swej kiesy ekwipuje go.  

Niechętnie mówi mi o tym tutor,  
Klątwa podobno na rodzie spoczywa,  
Czy tak być może? Jutrom usłyszal  
"Zginął w pożarze nocą w domu swym." 0:28  


## Turin Turambar 

    e a .   / e h .     / e c .     / e gis d a . / e gis-a-gis-a-gis a /x2

    Przeciw  smoczej potędze bezsilny jestem ja czy ty  - jeśliś sam. 
    Straszliwy los Turina zdławił go gdy mierzył się z nim sam.


Turin Turambar to wielki heros,  
stworzył Dor Cuarthol, Glaurunga zgładził  
Wrogiem prawdziwym widział go Morgoth  
Miłość Finduilas ocalić może go  

Glaurung Turina tak zaczarował,  
By Finduilas krzyków nie słyszał,  
Matki i siostry szukać wyruszył,  
I tak utracił najbliższe wszystkie trzy  

                            Ufanie słowom smoka parszywy jakiś żart.  
                            Jad, hańba i tortury, wie Arcirias   
                            Rodzinę do Rhosgobel śle Elendilmira blask,  
                            Ze snu mnie łopot skrzydeł zbudził dziś,  
                            Jedynie jedność uratuje nas.   
                            Ratujcie jedni drugich raz po raz.   
                            Ori Valayę, Myrtle Herefrę zaś.   
                             Tylko w jedności jest nadzieja. 1:27  

## Jedność

   Wszystko czego poszukujecie spotka zguba a to co kochacie spadnie w Cień  
   Klątwą nas chce usidlić, trza go pokonać to jasne jest jak dzień.  
   
   Valayi runy, wiedza, Gundina młot prastary, ten klejnot, Adeliny łuk  
   Herefry kunszt wojenny, Oriego serce i cała Myrtle Tuk  
   

   Nim szukać rozpoczniemy zakończmy łowy zaczęte.  
   Wciąż skarbu szuka pożoga i wściekłością wre.   

Mściwy, rozwścieczon, wszak ma nas za nic,  
Pogonić Gwathran musi za nami  
Klęska z rąk naszych, straszna sromota  
Zapłacić muszą mi robaki te!  

W dni kilka orczą hordę sprowadził,  
Tam gdzie zmierzamy, będzie mu łatwiej  
Proszę, druhowie, stańmy do starcia  
Rodziny nasze uratować chcę.  

                            Jedynie jedność uratuje nas.   
                            Ratujcie jedni drugich raz po raz.   
                            Herefra mnie osłania bym ja uleczył Was  
                             Tylko w jedności jest nadzieja.  

                            Jedynie jedność uratuje nas.   
                            Ratujcie jedni drugich raz po raz.   
                            Valaya Oriego, Ori Myrtle łap  
                             Tylko w jedności jest nadzieja. 2:40  

## Pamięć ofiar

    Rzucił klątwę na nas i mściwy za nami gna  
    Spotkajmy się z nim raz jeszcze dokończyć sprawy trza  
    
    Jeśli ktoś lepszy plan zna niechaj mówi wprost  
    Jak mnie dopadnie Gundinie Herefrę proszę broń  

    Oin gospodarzem i bohaterem jest,  
    W trzewiach smoka zostawić go nie godzi się  

    Wyzwijmy go raz jeszcze, musi zgodzić się  
    Dobijmy go już wreszcie, zakończmy sprawę tą.  

Wykłute oko, ucięty ogon,  
Strzaskane łuski, gad zranion srogo,  
Skrzydła podkulił, orki porzucił  
Bitwa zwycięska ale żywym pierzchł.  

O pamięć proszę dla ofiar jego,  
Sług z domu Oina, tutora mego,  
Dzielnych krasnali z krwi Rugnissonów,  
Fundina i Kamiennej Gwardii też.  


Czy jestem nieopierzonym młokosem? Na pewno.  
Czy brak mi doświadczenia? Niewątpliwie.  
Ale gdy mówiłem o klątwie Turina po raz pierwszy - nikt nie wziął tego poważnie.  
Proszę, druhowie. O radę. O mądrość waszą. Smoka na karku, co dybie na życia nasze i bliskich naszych mamy. 
Gundinie, Valayo, Ori, Myrtle. Nie chcę Cienistego Wędrowca palącego Eryn Galen, czy nawet fragment jej. 
