## Rugnissonowie

Pieśń Ernwar ułożył przed bitwą by jasnym było z czym przyszło się Rudej Kompanii mierzyć, a także dlatego, że los Rugnissonów straszliwym był i młodzian chciał im w jakichś sposób hołd oddać.
Po bitwie pieśń wzbogacił, zatem jest więcej niż jedna wersja znana.

Melodia będzie podobna do (zwrotki można śpiewać w harmonii): 
e e e d e fis fis   
fis fis fis e fis g g  
g g g a g fis fis  
fis fis fis g fis e e  

Wśród setek krasnoludzkich rodów,  
Od Siedmiu Ojców powstałych,  
Ród Rugnissonów od wieków,   
polował na maszkary.  

Stwory przerozmaite,   
często rodem z koszmaru,  
Rugnissonowie łowić,  
sobie upodobali.  

W początkach Trzeciej Ery,  
Dunadanów poznali,  
I wtedy z Cienistym Wędrowcem,  
ścieżki swe skrzyżowali.  

Gwathran Elendilmira,  
posiąść pragnął od dawna,  
już przecie Silmarilien,  
Skarbu strzec przed nim kazała.  

Pieśń o Cienistym Wędrowcze,  
Rugnisson mi przekazał.  
Życiem za to zapłacił!  
Lecz drogę mi tym wskazał.  

Gwathran, Cienisty Wędrowiec,  
Ohydne plemię Glaurunga,  
Pod ziemią skryć się lubi,  
Earendila unika.  

Jeżeli atak to w nocy,  
Najlepiej też pod ziemią.  
Wtedy moc pomnóż dwa razy,  
(Słońca promienie nadzieją).  

Ziemię on topić potrafi,  
Biczem ognistym uderzy,  
Do Ereboru się dostał,  
Oina w jego progach uśmiercił.  

Rozmiary zmienić swe umie,  
W cieniu potrafi się ukryć,  
Cienia zranić nie zdołasz,  
Cień także zranić nie umie.  

Gdy broń chcesz wziąć na Gwathrana,  
Pamiętaj by nie brać jednej,  
Po jednym ciosie chwyć drugą,  
Ta pierwsza w ruinie już będzie.  

Skrzydła strach sieją w sercach,  
Je ciachaj jeśli ma nie mknąć,  
Pysk zionie strumieniem ognia,   
Lub słowami Cię przeklnie.  

Przeklinać Gwathran potrafi,  
Nawet posłańcy Manwego,  
Obronić się nie zdołali,  
Od losu okrutnego.  

Cienistego Wędrowca,  
Poznały orły wielkie,  
Numenor, Rugnissoni,  
Erebor i Ruda Kompania.  

Ich bojów poznaj dzieje,  
Bij: tułów, ogon i skrzydła,  
paszczę oraz pazury,  
Oraz uważaj na cienie!  

Smok zionąc pali okrutnie,  
Lecz wolisz ścierpieć ten płomień,  
Lub dalekosiężny ogon,  
Wszystko! byle nie słowa.  

Strachem tchną skrzydła cieniste,  
Pazury ma niczym kindżaly,  
Gwatran rozmiary swe zmienia,  
Olbrzymi! Nie, całkiem mały.  

Poznaj historię Turina  
Turambara herosa,  
Co Glaurunga pokonał,  
(przy czym dokonał żywota)  

Strzeż się druhu mój ognia!  
Uważaj zwłaszcza w nocy,  
Walczysz z duchem ognistym,  
U mędrców szukaj pomocy.  

Wspomnij też Rugnissonów,  
Odważne krasnoludzkie plemię,  
Co od zarania dziejów,  
Z maszkar oczyszcza Środziemie.  

Zatem dybiesz na smoka,  
Wyraźnieś nierozważny,  
Dobierz kompanów zaraz,  
Inaczej śmierć Cię znajdzie.

Samemu nie podołasz,  
Poniechaj, śmierci nie szukaj,  
Śmierć już znalazła wielu,  
Niechaj i Ciebie szuka.

Lecz jeśli smok Cię znajdzie,  
Tedy pamiętaj o tem,  
Można gada uśmiercić,  
Lecz padać będziecie pokotem.  

Jedynie w jedności siła,   
Ratujcie jedni drugich,  
Słońce jeśli wam miłe,  
Da się go ukatrupić.  

Pamiętaj o Rugnissonach,  
Ściga ich los okrutny,  
Jak napotkasz którego,  
Wspomóż go w jego trudach.
