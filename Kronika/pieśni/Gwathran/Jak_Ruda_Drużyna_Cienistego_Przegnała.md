# O tym jak Ruda Drużyna Cienistego przegnała


Gdy Ruda Drużyna przeciw Cienistemu stawała,  
Wiele dni i nocy w Odległych Sztolniach się zasadzała.  
Dzielny Óri syn Óina, wziął bitnych krasnoludów pod komendę  
i nikt z nich nie wątpił, że potwierdzi on swoją legendę.  

Towarzyszyła mu wiernie zasłużona Mertle Tuk,  
hobittka o walecznym sercu, której nie straszny żaden wróg.  
Wraz z nimi Valaya, mędrczyni rodem z Gór Błękitnych,  
nie brakło Drużynie jej rad, pieśni i pomysłów wybitnych.  

Ludzka młodzież także u ich boku stanęła do boju:  
Ernwar z Baleriandów niósł Światło pełen spokoju,  
a waleczna Herefra Steelsheen, rohirmirrska jeźdźczyni,  
wykazała się odwagą jako wojowniczka i dowódczyni.  

Drużyna niepomna na przesądy przyjęła także Bardinssona,  
dobrze się stało – jego postać będzie słowem ozłocona.  
To właśnie Gundin z Gór Szarych zadał ważny cios poczwarze.  
Niechże teraz kto powie, że Bardissonowie to wszarze!  

Bohaterów wsparła także Adelina Otarin, córa Eradura  
– oto gondorska szlachecka krew co nadaje orężu pazura!  
Niech te słowa ku chwale bohaterów rozbrzmiewają  
i pamięć tych, co polegli z honorem i szacunkiem opiewają!  

Óri jako pierwszy przyjął na siebie ciosy pomiotu Cienia,  
nie zawahał się ani chwili, by nadstawić swego ramienia  
za kompanów co wraz z nim stanęli do bitwy  
z toporami, mieczami i grotami ostrymi niczym brzytwy.  

Valaya córka Baddoka swą pieśnią serca witeziów pokrzepiła  
i nie uląkł się nikt z wojów, gdy jaskinię mroku mgła spowiła.  
Gundin Bardinsson zmysłem górnika monstrum wytropił,  
młotem skałę rozłupał, a plugawy opar jego brodę skropił.  

Śmiałkowie musieli paskudztwo zaczepnymi wyzwać słowy,  
by w końcu wychynęło z jamy, jednak nie od głowy,  
lecz od ogona swe ohydne jestestwo ukazywać poczęło.  
Zamierzyło się na harfiarkę lecz dzięki Óriemu wcale jej nie tknęło.  

Stwór parszywy chyżym ogonem śmiałego Óriego zniewolił  
lecz nieulękły Ernwar, godzien Elendilmira, nie pozwolił,  
by jego towarzysza więziła nazbyt długo szkaradna bestyja.  
Wypuścił celną strzałę – niech pokraka wie, że los jej nie sprzyja!  

Bohaterski czyn Puszczanina zachęcił walecznych towarzyszy broni.  
Oto złotowłosa Herefra rohirrimskim mieczem krew smoka roni!  
Nieustraszona rycerka ugodziła potwora w czułą słabiznę,  
Aż ten wydał ryk i począł sączyć gniewne słowa jak truciznę.  

Naraz zwinna Mertle sztyletem w smoczy ogon godzi,  
Choć już po kostki z rwącej i zimnej wodzie brodzi,  
gdyż Gundin Bardinsson z iście górniczym zacięciem  
zalewa bestię strumieniem wody, jakby ta była kocięciem!  

Oto złorzecząc bohaterom, potwór rozpostarł upiorne skrzydła  
lecz nie straszna odważnym wojownikom poczwara przebrzydła!  
Przejrzawszy jarmarczne sztuczki smoka, światła mędrczyni Valaya  
godzi w bestię ostrzem celnych słów i kompanów swych uspokaja.  

Wtem zdradliwy pomiot Cienia ostrymi szponami atakuje,  
Ernwar Dłużnik byłby trafion, jednak Herefra doskakuje  
i przyjmuje na siebie cios dla niego przeznaczony,  
a potem powtórny, mocarny i gniewem pokrzepiony.  

I choć dzielna rohirrimska wojowniczka na chwilę w walce ustaje,  
ponownie nieulękła Mertle sztyletem ranę w smoczy ogon zadaje!  
Zaraz za nią Óri razi smocze cielsko swym potężnym toporem.  
Każde ze śmiałków walczy z iście godnym podziwu uporem!  

Nagle Gundin Bardinsson młotem wielki zamach bierze  
bestia przeraźliwie ryczy, runy goreją, naprzód bohaterze!  
Oto oręż pradziada wśród smoczej pożogi się przeistacza:  
Mocarny topór to, a nie młot! A potwór się zatacza…  

Korpus bestii traci materialne kształty i staje się oparem,  
ale jej pysk, zwraca się do śmiałków i zaczyna plunąć żarem!  
Drużyna zręcznie umyka przed oddechem płomienistym,  
lecz Gundin i Valaya zostają naznaczeni ogniem cienistym.  

Znów ogon nikczemnej poczwary w Rohirrimkę celuje,  
ale nieustraszona hobittka sprytnie ów atak niweluje,  
co przepłaca schwytaniem swej niewielkiej figury  
i rozpoczyna się rozmowa z bestią, wielce trudnej natury.  

Ernwar pod ciężarem korony i odpowiedzialności  
walczy ze smokiem na słowa, ale ma się na baczności.  
Gdy tylko moment dogodny w dyskusji wyczuwa,  
strzałę ostatniej nadziei na cięciwę nasuwa.  

Gdy trafia ogon poczwary, ten staje się cieniem,  
a Mertle zostaje pochwycona przed uderzeniem  
w twardą skałę podłoża w krasnoludzkie ramiona.  
Óri zadbał o to, by jego towarzyszka została ocalona!  

Bestia wiedząc, że przegrywa, pierzcha z pola walki  
Dziś nie na jej korzyść przechyliły się szalki  
wagi przeznaczenia, którym chciałaby władać,  
ale nim ucieknie próbuje jeszcze ujadać.  

Rzuca słów kilka groźnych, ale czy prawdziwych?  
Z pewnością należą one do tych niegodziwych.  
Nim Ruda Drużyna Cienistego przegnała,  
strzała Ernwara jej ślepie pożegnała.  

Tak oto bohaterowie przepędzili smoka  
A wy strzeżcie się ognistego potwora bez oka!      
