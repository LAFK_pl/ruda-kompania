Pieśń, której tytuł przekazał Smauriemu potomek Rugnissonów.  
Wg Valayi pochodzi z Ered Mitrhin, Szarych Gór.  
[Muzyka](https://www.youtube.com/watch?v=eXdMjTOPP3M)

## W niebie cień

-1-  
Śnię o wyspy brzegu tam  
Meneltarmy wystrzelała grań  
Na jej ciszę, czystą tak  
Szum strasznych skrzydeł padł  
  
-Ref-  
W śnie widziałem w niebie cień  
Wędrujący pośród fal  
Zachodu  
  
-2-  
Czujne wieki strawił on  
By straż orłów strącić w zimną toń  
Żar  jego, chciwy tak  
Goryczy nabrał smak  
  
-Ref-  
W śnie widziałem w niebie cień  
Wędrujący pośród fal  
Zachodu  
  
-3-  
Śnię o chwili, którą skradł  
Miesiąc z gwiazdą jasnym blaskiem grał  
Silmarien, piękna tak  
Trwoż się o rodu skarb  
  
  
  
-Ref-  
W śnie widziałem w niebie cień  
Wędrujący pośród fal  
Zachodu  
  
-4-  
Gór i brzegów słychać tren  
Numenore w otchłań morskich den  
Gwiazdo, zwiewna tak  
Gwathran podąża w ślad  
  
-Ref-  
W śnie widziałem w niebie cień  
Wędrujący pośród fal  
Zachodu  
-Ref 2-  
W śnie widziałem w czarny cień  
Wędrujący pośród hal  
Śródziemia  
  
## Wyjaśnienia

. Gwathran - od sindarinu - cienisty , gwath to cień - Wędrujący Cień  
. Silmiarien, pierwsza królowa Numenoru, wyspy co zatonęła i królestwa matki dla Dunadanów, Gondoru i Arnoru.  
. Meneltarma - najwyższa góra Numenoru?
