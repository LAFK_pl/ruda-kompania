# Kamienne trolle w Spacerowych Smajlach

Smydl, Hydl i Fydl leże mieli sobie								// d fis a - g e cis  
Hobbita z norki wygnali, nad Anduiną przy brodzie // d fis a - g a d  

Dziadunio myślał "Borsuki! Mi pod podłogą harcują!"  
Dobytku wóz pełen spakował, i już go w Smajlach nie było.  

Rhadagast Öriego wysłał, do Spacerowych Smajli  
By Elendilmira szukać, Vigola zniknięcie wyjaśnić.  

### Refren:

Trolle wysokie są na dwu chłopa	// d e fis fis fis fis e fis g g  
Nogi ich grube są jak u słoni			// g fis e e e e d e fis fis  
Miast oblicza kamienny kartofel			// fis g a fis g a fis g a fis  
Lecz troll nie wyglądem się broni! // g fis e e e e d e d d  

Anduinę przeskoczy w dwu skokach,   
Nurt jej nawet nim nie zachwieje  
Głazy miota niczym katapulta  
Ty uważaj bo głazem zabije!  

Jak trolla spotkasz to w nocy  
(słońce przecież go w kamień obraca).  
Lecz gdy do wschodu słońca daleko,  
Duży ogień to dobry jest straszak!   

Pamiętaj, troll mowę rozumie,  
Choć głupi jest jak but!  
Obrażony za tobą pogoni,  
Dopóki nie padniesz bez tchu!  


## Öri, Myrtle, Gundin i trolle

Öriego troll za stopę  
Spod podłogi uchwycił  
I nim o sufit walnął  
Maczugę z niego czyniąc!  

Nasz krasnolud wciąż tęga mina,  
I woła, z uśmiechem na twarzy,  
"On jest mój" by kompanów powstrzymać  
Nawet kiedy nim Fydl wywija  

Gundina trzeba było  
By Öri-maczugę zatrzymać  
Gundin cios najmocniejszy,   
Kamratem swym otrzymał  

Gundin ze swoim młotem  
Öri ze swoim toporem  
Myrtle dźga swoim mieczem  
Biedne kamienne trolle!  

> refren

**ZMIANA MELODII tutaj** - a la Zbroja Kaczmarskiego

"Te głupkowate trolle, nie mają z nami szans.  
Nie takie się rzeczy robiło! Dol Guldur pamiętacie?"  

Tak Gundin do walki zagrzał, w dłoniach już dzierząc młot  
Strachowi w twarz on śmiał się, żaden troll to nie kłopot.  

**Jeszcze inna melodia: spokojna, wesoła, marszowa**

## Herefra i Smydl

Dziewczyna złotowłosa  
Trolla pierwsza dostrzegła  
Nie bacząc na jego ogrom  
Ostrzec kompanów pobiegła  
  
Anduina szeroka,   
na pięćdziesiąt metrów,  
Nad brodem jedna lina,   
jej trzyma się Herefra  
  
Smydl rzucił w nią kamieniem,   
Rohańska krew w nurt już spływa  
Lecz dziewczyna chwyta część liny  
Na brzeg ciągnie ją Valaya  
  
Valaya córka Drumby  
Ogień od razu rozpala  
I kuca uspokaja,   
Herefrze czas tak dając  
  
Troll miotał kamieniami  
Norkę hobbicką zniszczył  
Lecz wobec ostrza Herefry  
Właściwie tylko pyszczył  
  
Strzałą go zraniła  
Słowami rozwścieczyła  
I spod niego uciekłszy  
W trzech ciosach powaliła.  
  
Trzynastolenia drobna,  
Bynajmniej nie na koniu  
Jak ujrzysz złotowłosą  
Miej słowa na baczeniu!  
  
### Epilog  

W dłoniach hobbitki z Tuków  
Nawet dywanik jest groźny  
Skoczyła z nim na łeb trolla  
I zawiązała mu oczy!  
  
Öri wziął szeroki zamach,  
Fydlowi rozpruł kolano  
Troll oślepiony przez Myrtle,  
Został mieczykiem zadźgany!  
  
Gundin łeb Hydla rozwalił,  
Kilkoma młota ciosami,  
I tak ostatni z braci,  
Padł pod mocą kompanii.  
  
Kompania trolle sprawiła  
Bez większego problemu  
Lecz Vigola do powrotu namówić  
TO dopiero trudne było!  
