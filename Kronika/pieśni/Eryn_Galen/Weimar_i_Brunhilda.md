# Przestroga przed zaczarowanym strumieniem

Z północy płynie, w górach się zaczyna

Kończy - na zachód od hal Thranduila

Nie pij, nie kąp się bo pozapominasz!

# Weimar i Brunhilda (Strumień czarny)

Weimar i Brunhilda ku sobie się mieli,  
Weseleśmy im wyprawili,  
Lecz Weimar napił się ze strumienia,  
Co każe o wszystkim pozapominać

Strumień czarny, bystra woda,  
Jeśli wpadniesz - wielka szkoda  
Nie pij wody ze strumienia,  
Krzywda? Troska? Zapomnienie!

Weimar na wesele nie dotarł,  
Leśnym się stał tułaczem,  
O swej Brunhildzie zapomniał,  
W elfie bramy zakołatał

Strumień czarny, bystra woda,  
Jeśli wpadniesz - wielka szkoda  
Nie pij wody ze strumienia,  
Krzywda? Troska? Zapomnienie!

Weimar swe wesele przespał,  
Weimar przespał kawał życia,  
Pospał Weimar trzy księżyce,  
Wspomnień stracił wiele więcej.

Strumień czarny, bystra woda,  
Jeśli wpadniesz - wielka szkoda  
Nie pij wody ze strumienia,  
Krzywda? Troska? Zapomnienie!

Latem było weselicho,  
Jesienią się Weimar ocknął,  
I tak pyta zadziwiony  
Liście wiosną lecą? Cuda!

Strumień czarny, bystra woda,  
Nie pij - życia twego szkoda,  
Pamięć stracisz, życie prześpisz  
Weimara podzielisz los,  
O ile Cię jeszcze znajdą,  
Nim zwęszy Cię warga nos.  
