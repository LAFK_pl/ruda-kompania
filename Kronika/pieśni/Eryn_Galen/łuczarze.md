## Stara pieśń łuczarzy

Jodła nie, zbyt szybko się złamie.  
Kasztan? Nie, nawet gdy umiesz znaleźć,  
Topola słaby będzie mieć naciąg.  
Już lepiej do łuku wziąć jesion.

Cedr, świerk lub sosna? Jakie to miękkie!  
Orzech lub olcha? Wszak trzeba szerszych!  
Dąb, brzoza, wiśnia, wiąz lub cis!  
Oto łuczarza stały ryt.

Dąb będzie niezły jak dobrze trafisz.  
Buk całkiem całkiem, jeśli niemłody,  
Albo niestary, z tym bywa różnie,  
Nieraz buk znaczył robotę na próżno.

Wiąz jest co nieco po słabej stronie,  
Choć lepszy jest od wielu klonów,  
Grab na łeb bije obydwa,  
A brzoza - jeśli w dzień czy dwa.

Królem na łuk jest chmielograb,  
Lub jeszcze rzadszy złotokap,  
Lecz jeśli łuków masz robić wiele,  
Cis twardziel (nie pracuj nad bielmem).

Cisowe słoje biegną jak trza,  
Cis gęste drewno przecież ma,  
Cis częściej znajdziesz niż wiąz czy grab,  
Cis prosto obrobić się da.

Cisowe łuki więc robić trza.  
Cis wiele dobrych gałęzi ma,  
W Eryn Galen cisów nie brak,  
(z jesionem na elfy uważać trza).

## Pieśń młodego łuczarza

Młody łuczarzu, śpiewaj tak  
Z cisu twardzieli łuki masz  
Łuk z bielmem chyba dla giganta,  
Siłacza co inne łuki łamie,  
Wtedy tak - bielmo przysposabiaj,  
(i we dwóch taki łuk obrabiaj)  
Cis miękki jest więc rogu użyj,  
Cięciwa się nie będzie wrzynać.  
Pajęcze sieci? Na psa urok!  
Czy chcesz ten łuk Cieniem przeklinać?!  
Zacznij od łyka wierzbowego!  
Jeno go - błagam - sam nie skręcaj,  
By zgrubień nie dodawać więcej,  
Od nich cięciwa będzie pękać.  
Dobierz odległość od majdanu,  
Na kunszt zważaj strzelającego,  
Blisko majdanu - szybkostrzelny,  
Będzie przyjemnie się napinać,  
Lecz wszystek błędy widać będzie,  
Srodze zawstydzisz nowicjusza  
Gromko Cię potem będzie przeklinać.  
Weteran błędów nie popełni,  
Porażka go aż tak nie wzrusza,  
(poznasz po skórze na ramieniu,  
co przed cięciwy biczem chroni)  
Pozna się myśliwego dusza,  
Na właściwościach takiej broni.  
Młodemu dobierz tak cięciwę,  
By od majdanu była z dala,  
Nie będzie aż tak szybko strzelać,  
Lecz wszystko stoleruje, nawet  
Palce od potu czy krwi śliskie,  
Że go "ktoś trącił wtem łożyskiem",  
Że strzała zeszła, cel się zatrząsł,  
Lub kto nad uchem nagle wrzasnął.  
Przed tym kunszt myśliwego chroni,  
I nie jest to kwestia broni,  
Lecz młody łuczarzu wiedz,  
Bliżej majdanu - trudniej jest.
