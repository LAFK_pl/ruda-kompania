# Topola, jodła (pieśn o Radagaście)

Topola, jodła, dąb, buk, cis,  
Zielonych liści pełen las,  
Schronienie przed złem daje ci,  
O ile Radagasta znasz!  

Po obu stronach Starej Leśnej Drogi,  
unikaj pająka, warga, elfa,  
Lecz nie masz wrogów lub przeszkody  
O ile Radagasta znasz.

Nie krzywdź co rośnie albo żyje,  
Z Cieniem się nie zadawaj,  
Rosgobelskiego nie garbujesz?  
Radagasta nie musisz znać. 
