# Krasnoludzki pan, krasnoludzki kark

pieśń ułożona po rozmowie z Oinem w jego domu. 

## Refren

Krasnoludzki pan, krasnoludzki kark // 8 e e e e 4 d, 8 e fis e d h

Kark który nigdy się łatwo nie zgina // 4 e.e e d.d d e e a a

Jedność tak, rację ja mam // a a 2g, fis fis 2g

Wykonaj me polecenie. // fis fis.fis g 8g 8g d.d

## Wstęp

Oto jest krasnoludzki pan. // 4 e e d d e e c

Dom bogaty czyny wielkie ale on jest sam. // 8 e fis g e d e fis d e fis e d h

Syn udany, mówią wszyscy // 4 h h a a g g a a

Co się ma, nie ceni się. // 4 g g fis.fis e fis d

**Refren**

Wielka wyprawa,  na szali cały ród.

Wśród dziewięciu, on jeden nie jest tchórz?

Erebor razem odzyskali

Ale on jest sam.

**Refren**

Król na mych barkach zostawił to.

Jakiż z niego jest władca?

"Tchórz" pogardliwie mówi pan

Dain to bezpieczna opcja.

## rzewnie - inna melodia!

Za bramą z księżycowego światła, // e e e e e fis fis

tam w mroku wśród mnóstwa cieni, // g g g g g fis fis

w porywach zatęchłego wiatru, // e e e e e d d

tam pan Oin cel swój widzi. // d d d c d c c 



W każdej minucie życia,

odbłysk szafiru z głębi,

pojedynczy przejrzysty kamień,

pana Oina jego cel uwodzi.



Złote ceremonialne berło,

półwachlarzem na sztorc zakończone,

nieważne gdzie i w jakiej chwili,

Pan Oin swój cel zawsze widzi.



Na końcu ścieżki pana Oina

latarni światło statkiem kieruje 

tam wybiega jego umysł, 

w każdej życia chwili.



**Refren**

Zgubą Durina - słabość charakteru

Oraz brak jednomyślności

Morię mogliśmy wtedy odzyskać

Lecz Dain miał chwilę słabości

**Refren**

Kogo chcesz pod obcasem zgnieść?

Gdzie leży Twa lojalność?

(Rzeknij że jego syn ją ma.

Rzuciłeś słowa na darmo.)

**Refren**

Moja wyprawa, moje poruczenie

Pamiętaj kto daje rozkazy!

(Ciężko samemu dźwigać takie brzemię,

Nie będę chował urazy)

## Zakończenie

Oto jest krasnoludzki pan. // 4 e e d d e e c

Syn wspaniały dom bogaty ale on jest sam. // 8 e fis g e d e fis d e fis e d h

Syn z Dol Guldur wrócił żyw. // 4 h h a a g g a a

Co się ma, nie ceni się. // 4 g g fis.fis e fis d



## Kilka słów uzasadnienia

1. Oin jest dla Ernwara postacią bohaterską z racji na odzyskanie Ereboru przez Thorinową kompanię. Ernwar mimo pieśni uważa, że to bohater i że Oin wie rzeczy jakich on nie wie.
2. Ale z kompanii pozostało dziewięciu: zginęli Thorin, Fili, Kili, wrócili do swoich spraw Gandalf i Bilbo. W tym kontekście zaskakuje samotne dźwiganie brzemienia i ryzykowanie swoim całym rodem. Czemu pozostali bohaterowie nie zostali w plan przez Oina wciągnięci? 
3. W pieśni Ernwar powraca do relacji ojca i syna i zasmuca go brak widocznej radości czy dumy z Oriego, o jego poczynaniach i osiągnięciach nie wspominając. Przejawia się to w zakończeniu, refrenie i kilku zwrotkach. To najbardziej chłopaka zasmuca i sprawia, że czuje że Oriego zawiódł.
4. Relacja Oin - Dain dla Belerianda jest zaskakująca. Pogarda Oina jest - zdaniem Ernwara - niezasłużona a nazwanie "słabością charakteru" czy brakiem jednomyślności Zguby Durina, istoty potężnej na tyle by zabić króla krasnoludów w jego własnym domu i wypędzić krasnoludów stamtąd na lata młodzian uważa za krótkowzroczne.
5. Rzewny kawałek opowiada o brzemieniu Oina. Puszczanin dostrzega, że Oin musi zmagać się z nim w każdej chwili życia. To może wręcz pożerać Oina od środka. Ten element zawiera fragmenty wizji.
6. Oin dla młodego staje się postacią tragiczną i nie do końca zrozumiałą. Ernwar nie dostrzega jeszcze, że to jak ojciec postrzega syna jest cokolwiek podobne do tego jak on sam postrzega swoją rodzinę - jako element status quo, stały element świata.  Ale jednocześnie widzi, że Oin skazuje się na samotność. Beleriand uważa, że niepotrzebnie, ale zakłada, że coś za tym stoi.