# Listy z Ereboru

Wysłane gdy Drużyna podejmowana była gościną u pana Oina Gloinssona, ojca Oriego, przed bankietem króla Daina Drugiego tego miana.

## Ernwar do rodziny

List, a właściwie paczka, zawiera dużo rozmaitych rzeczy - około kilkunastu - dla licznej rodziny Ernwara, począwszy od rodziców, dziadków, piątki rodzeństwa oraz wybranych dalszych krewnych. 
Wraz z listem wysłany został też pamiętnik chłopaka. 
Prezenty są właściwie bez wyjątku praktyczne. 

## Ernwar do Rugnissona

Czcigodny panie Rugnisson,

Nie znasz mnie panie, lecz mimo to piszę do Ciebie w prostej sprawie. W mej wędrówce trafiłem na pewne miejsce, które mój towarzysz, Gundin Bardinsson, określił jako należące do Twego rodu. Wyniosłem stamtąd broszę, którą obiecałem - jeśli będzie okazja - przekazać potomkom właścicieli. Rozminęliśmy się w Ereborze panie, lecz poprosiłem mego gospodarza, pana Oina, o przesłanie Ci tego listu, wraz z rzeczoną broszą.

Nie wiedząc ile o tym miejscu mogę powierzyć pergaminowi, pozwolę sobie na skromny opis. Okrutna i złowroga siła zgubą była mieszkańców, których kunszt widać było w budowie. Spore wrażenie zrobiła zwłaszcza bazaltowa kolumna, pojedynczą kolumną światła skąpana i złotą paterą zdobiona.

Wkrótce wyruszam na cokolwiek ryzykowną wyprawę. Nie wiedząc jak drogi nasze się ułożą, kreślę się z szacunkiem,
Ernwar z Beleriandów.
PS. Jeśli życzysz sobie panie, wypytać mnie o coś, służę. Przez czas jakiś pozostanę w Ereborze, liczę też, że jeszcze tu wrócę, zatem to dobre miejsce by kierować listy.

## Ernwar do Adeliny 

Pierwszy i najpoważniejszy z wielu listów Ernwara. W końcu dogadali się, że będą pisać. Zatem:

* adresowany tak, jak się umówili, zakładam, że panna powiedziała jak list mam adresować i wzięła pod uwagę, że listy znikąd i z daleka, adresowany do panny Adeliny, może wzbudzić uwagę. Jeśli nic nie powiedziała, to Ernwar adresuje rzecz otwarcie.
* nie mam pojęcia ile kosztuje przesyłka pocztowa z Ereboru do Gondoru - jak będzie drogo to poproszę by ktoś z kompanów się potargował w mym imieniu :D Znaczy, że możesz wybrać osobę z Drużyny i powiedzieć jej, że Ernwar taki list wysyła.
* Otwarcie: Przyszła wielka podróżniczko Adelino!
* opis Ereboru i jego wspaniałości, informacje o tym jak podróże są okrutne, szkic broszy Rugnissonów z opowieścią o pogrzebanej fortecy i zgrozie jaką człowieka ogarnia na sam widok śladów, pieśń ereborskich rzemieślników (lub inna popularna pieśń jaką Ernwar w Samotnej Górze usłyszał), opowieść o kupowaniu strojów i Smyku gryzącym materiały, pochwalenie Oriego i Myrtle i Valayi (królewski bankiet, wyobrażasz sobie? no tak, Ty na pewno sobie wyobrażasz, nie to co ja), dwa paragrafy o panu Oinie (z których można wyczytać, że to bohater i że czegoś w nim Ernwar po prostu nie pojmuje), zapytanie o Raekhosta (ciekawi mnie postać Niegodnego Barona Raekhosta z Rohanu, czy wiesz coś może o nim?), paragraf o Herefrze (pełen ciepła, bo dla Ernwara to naturalne jak o nią idzie), który miał przekazać, że młodzian będzie się uczył jazdy konnej by następnym razem lepiej się spisać, humorystyczny opis krasnoludzkiej etykiety i pochwała Oriego za jego słowa o tym, że Ernwar może mu się kłaniać jakby miał brodę znacznie dłuższą niż ma
* akompaniament: prezent. Ernwar znowu wybiera coś praktycznego, co przyda się na szlaku: siatkę na włosy, ale praktyczne i piękne bo z delikatnych srebrnych łańcuszków.
