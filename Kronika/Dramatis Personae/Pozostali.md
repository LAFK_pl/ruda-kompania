# Pozostali

Kto, gdzie poznany, frakcje, opis.

## Rozdział I: latarnia Baltiego

1. Król Bard z Jeziora
    1. poznany na Radzie 3 Armii - **to spotkanie zaowocowało całą wyprawą!**
    2. mądry, szlachetny władca, który pokazał, że pomimo swego statusu zachowuje dawną otwartość do tych niższego stanu
2. Malaryk
    1. wygląd? 
    2. reprezentant Beornian na Radzie
    3. wskutek działań drużyny zdecydował zemścić się na Hevie, co doprowadziło do pamiętnej walki w Dol Guldur
3. Heva Tarczowniczka
    1. **wygląd? Kamil?**
    2. uratowana przez Öriego, Myrtle, Brunhildę, Idril oraz Eola
    3. przywódczyni Puszczan
    4. przyobiecała swe towarzystwo Öriemu w Morii
4. Erlant - ambasador króla elfów, elf wysoki

## Rozdział II: Elendilmir

1. Radagast Bury, czarodziej z Rosgobel, patron Puszczan
    1. krzaczaste brwi, szpiczasty kapelusz, kostur, donośny głos
    2. gościł drużynę u siebie
    3. polecił im znaleźć Elendilmir (i wyjaśnić zniknięcie Vigola?)
2. Tom Bombadil, skrzat, rymujący gość Radagasta (r.2.)
3. Vigol ze Spacerowych Smajli (r.2.)
    1. hobbit z brodą i monoklem, przygłuchy acz poczciwy dziadunio wygnany z norki przez borsuki (a tak naprawdę trolle), w konkursie zagadek przegrał na rzecz drużyny Elendilmir, elfickiej roboty koronę
    2. Herefra go nie trawi
    3. jego norka w Spacerowych Smajlach posłużyła drużynie za sanktuarium, pod nią są tunele Rugnissonów z dawnym leżem smoka!
4. panna Adelina Otarin z Gondoru, córa Eradura, majordomusa zamku na Pelargirze
    1. ma rude włosy, bujne loki, młodziutka (15 lat), pełna życia, ma rubinową biżuterię (kolia, kolczyki)
    2. nawykła do posłuchu otaczających ją ludzi, 
    3. ona i Ernwar wyglądają naprawdę podobnie, oboje pochodzą od Ethelma, są zatem spokrewnieni
    4. herb: okręt na szachownicy ze złoceniami: ród posiada tytuł, obecnie Gondor, niegdyś Arnor!
    5. w orszaku jej: Lustig (gorącogłowy), Surnir i Woronc (hamuje Lustiga)

## Rozdział III: Erebor

1. Gardak, majordom Oina, (Dahl, r.3)
    1. Równo przystrzyżona czupryna, szamerunek majordomusa. 
    2. ojcowska figura dla Öriego, z racji na obowiązki ojca nieraz go zastępował.
2. Oin, syn Gróina, ojciec Öriego, brat Gloina, ojca Gimlego.
    1. twarz niczym wykuta w skale, srebrny grzebień we włosach, szafirowe - lodowe - spojrzenie
    2. razi słowami niczym ostrzem
3. Undur Bonsson - znajomy Gundina, spotkany kiedy pracował jako odźwierny w gospodzie którą pan Oin wynajął dla swoich 'lojalnych komu innemu' gości
    1. wyzwał Gundina na picie i jak panowie dotarli do Runicznego Destylatu - padł i stracił pracę
    2. następnego dnia spotkał Myrtle i poszli na pierożki
4. Heruling z Dunharrow
    1. Mistrz Koni z Rohanu poznany w Ereborze, wznosił toast za czyny Oriego i kompanii, czym nieumyślnie zawstydził Herefrę
    2. Krewny Herefry, znał jej rodziców
    3. Wykorzystał Herefrę by zdobyć wejście do króla i poprosić o pozwolenie na wstęp do Khazad Dum - by ratować Kurtiego
5. Król Dain II Żelazna Stopa z Ereboru, 
    1. w momencie kiedy widzimy go po raz pierwszy, ranny w ramię
    2. ma serdeczną zażyłość ze swoją doradczynią Valayą
    3. bohater, zabił Azoga
    4. przeciwny wyprawie do Morii z racji na terror, który tam przebywa
    5. jego ulubionym doradcą jest Balin
    6.
6. krasnolud Tharantrask, szuka kompanii w Ereborze na wyprawę do Morii, ratować "Kamiennego Bęcwała Kurtiego" 
    1. znajomy Valayi z dawnych czasów, 
    2. wuj Kurtiego (syna Urdany, młodszej siostry Gundina), brat Thikkurila (dawnego narzeczonego Valayi)
    3. rosochata broda, starszy od Oriego, ręce jak bochny chleba wygląda na krasnoluda 
7. Arcirias z Minas Tirith, uczony, królewski Poszukiwacz Wiedzy
    1. wg Oina: sługa Sarumana, który ma sposoby by porozumieć się ze swym panem (ujawnione Oriemu)
    2. człowiek ok 40-letni, krótkie włosy, szpakowata i przycięta broda, nosi delikatną srebrną biżuterię
    3. rysy twarzy wskazują na zachodnie pochodzenie: Gondor lub ościenne królestwa
    4. z szat wnosząc - szanowany uczony, ma pozycję ale nie taką by samemu nie podróżować, na pewno nie kupiec, 
    5. wg Valayi: skupiał się ostatnio na ziołach, po tej mistycznej stronie
    6. wg Myrtle: ułożony, skrępowany konwenansami Duży Człowiek na Stanowisku, intensywny, choć do Oina mu brakuje
    7. ścina się z drużyną na uczcie: uraża Myrtle lekceważeniem dla jej opowieści, arogancko traktuje Legolasa z racji na wydobycie odeń jego słowa, natrętnie próbuje wydobyć informacje o "przyczynach sukcesu drużyny"
    8. wykorzystując słowo księcia elfów wystawia go do Sądu Durina przeciw Drużynie
8. Sumri - posłaniec Oina Groinssona, którego Ernwar nieumyślnie wpakował na klątwę Rugnissonów prosząc by list przekazał, Ernwar opatrzył mu rany, kazał mu zapamiętać swe imię i słowa nie mówić o klątwie i sprawie więcej, a jakby to nie pomogło - znaleźć go.
9. Legolas syn Thranduila, książe leśnych elfów, piękny, o jasnym głosie i jasnej cerze i rzadkich u elfów złotych włosach
    1. wykorzystany przez Arciriasa z Gondoru, by dzięki niemu Arcirias uzyskał wstęp na ereborski bankiet - i nie tylko
    2. zrobił dobre wrażenie na Drużynie przekazując im wieści o Idril i jej pozdrowienia
    3. wystawiony przez Arciriasa do Sądu Durina
    4. zrobił wielkie wrażenie na Herefrze... jego biały rumak o mądrych i wielkich oczach
