# Ruda Drużyna!

## Ernwar zwany Dłużnikiem z Beleriandów

Młody puszczanin z rodu z tradycjami.

Powołanie: poszukiwacz skarbów.

Ród z Beleriandu, z Pierwszych Ludzi. Powędrowali do Eryn Galen by rozpowiadać o cudach. Powędrowali na Wyspę. Po edykcie o mowie elfiej zakazanej, wrócili do Eryn Galen. Kronikarze Dziejów. Choć Ernwar sądzi, że rusza na szlak w sekrecie, zachowanie jego matki sugeruje co innego.

![młody puszczanin z ogarem u boku](../grafiki/Woodman-with-Hound-and-LongHaftedAxe.png)

| Postawa         | smukły, niezbyt umięśniony ale nie chuderlawy, wysoki jak na ludzia |
| --------------- | ------------------------------------------------------------ |
| Wzrost / waga   | wyższy niż przeciętny Puszczanin bo ponad 2 rangara (m), szczapowaty |
| Wiek            | naście-dzieścia lat (19),                                    |
| Włosy           | Rude włosy na pazia, skórzana opaska je trzyma               |
| Oczy            | Zielone oczy z brąz plamkami. Ostrożne, ciekawe spojrzenie.  |
| Rysy            | Regularne rysy twarzy, wprawne oko znajdzie rysy Pierwszych Ludzi. |
| Cera            | jasna, lecz opalona od słońca i nieobcy jej wiatr            |
| Ubiór           | Połatany strój w którym wiele należałoby już wymienić (pas, buty). |
|                 | Każdy element BARDZO zadbany i schludny. Ale miejscami łata na łacie. |
|                 |                                                              |
| Zwraca uwagę:   | ogar u boku, niemały! Smyk może hobbitom robić za wierzchowca! |
|                 | Wielki łuk z cisowego bielma                                 |
|                 | Karwasz dębowy                                               |
|                 | skórzany pancerz (po Brunhildzie)                            |
|                 | nietypowy jak na Puszczanina wzrost                          |
|                 | rude włosy, jasna cera, zielone oczy                         |
|                 | topór o długim stylisku                                      |
| Odświętny strój | długi kaftan, jedna z miększych skór jakie Ernwar miał w dłoni, nie nadaje się do podróży bo zbyt łatwo się odkształca, lecz pozwala na szeroki zasięg ruchu. Kaftan nie stroni od klamerek, sprzączek, ma nawet kolorową podszewkę. Nie jest natomiast ozdobiony żadną wstążką. Do tego pasująca para butów. |

## Öri syn Oina

Znane imię, szlachetnie urodzony krasnolud spod Samotnej Góry, bohater z Dol Guldur. Był przez pewien czas samotnikiem, podróżując przekonywał ludzi do wyprawy na Morię i odzyskania prawowitego domu krasnoludów. Odzyskanie Samotnej Góry tchnęło nową nadzieję na ten plan. Wiadomo, że jego dziadek również podzielał to marzenie i pewnie on je zaszczepił wnukowi. Ludzie znają go z pracy w kuźni, lub pomocy, której nieodmiennie udzielał kiedy tylko mógł.

> O umlaut w unikodzie: 00d6 Ö 

| Postawa         | dobrze zbudowany, krępy, ale nad wyraz prosto (dumnie?) się trzyma |
| --------------- | ------------------------------------------------------------ |
| Wzrost / waga   | przeciętna krasnoludzka, jak na kowala przystało - barczysty |
| Wiek            | 80-90                                                        |
| Włosy           | Brązowe włosy, przeważnie boki splecione w warkocze okalające twarz. Góra spięta krasnoludzkimi zapinkami i puszczona w tył. Gruby warkocz brody zdobiony dwiema zapinami. |
| Oczy            | czarne niczym węgiel                                         |
| Rysy            |                                                              |
| Cera            | troszkę bardziej opalony                                     |
| Ubiór           | Schludny strój, nie przesadnie zdobiony, ale np. jego zapinka przy brodzie świadczy o jego statusie. |
|                 | Nosi swe rodowe szaty przy sobie - te odznaczają się bogactwem. |
|                 |                                                              |
| Zwraca uwagę:   | to szlachetny krasnolud!                                     |
|                 | Topór runiczny: przekuty z tradycyjnego wojennego i zmniejszony |
|                 | Niemały ogar u boku! Odo może hobbitom robić za wierzchowca! |
|                 | pancerz: krasnoludzka kolczuga, wyróżnia się hełm z przyłbicą (dar krasnoludzki) |
|                 |                                                              |
| Odświętny strój | jeden z wielu! Więcej zapinek i ładniejszych, maskujących uszczuplenie brody. |
|                 |                                                              |

## Myrtle Tuk z Hobbitonu, z Małych Ludzi

Bohaterka z Dol Guldur. Wytrawna kucharka, potrafi wręcz znikać w niewyjaśnionych okolicznościach i pojawiać się równie nagle. Zawsze uśmiechnięta, przynosi nadzieję na lepsze dni pozostałym. Gawędziara o rękawach pełnych anegdot z Hobbitonu.

| Postawa       | wyprostowana, patrzy cały czas ludziom w oczy nawet jeśli zadziera przy tym głowę |
| ------------- | ------------------------------------------------------------ |
| Wzrost / waga | niska, ciut niższa niż przeciętny hobbit                     |
| Wiek          | ??,                                                          |
| Włosy         | Brązowo-kasztanowe włosy                                     |
| Oczy          | ??                                                           |
| Rysy          | ??                                                           |
| Cera          | ??                                                           |
| Ubiór         | ??                                                           |
|               | Każdy element BARDZO zadbany i schludny. Ale miejscami łata na łacie. |
|               | ** **                                                        |
| Zwraca uwagę: | bardzo stary ale dobrze wykonany łuk, nosi na sobie ślady pracy wielu rzemieślników i nie do końca pasujące zdobienia, jedno ramię zdaje się jaśniejsze od drugiego, co sprawia wrażenie, że ma inne uszko, stąd nazwa Uszatek |
|               | skrzypce                                                     |
|               | mieczyk                                                      |
|               |                                                              |
|               |                                                              |
|               |                                                              |
|               |                                                              |

## Gundin Bardinsson

Ta część zostaje zwykle niewypowiedziana, choć np. Valaya szybko orientuje się, że to ród Bardinssonów. Pozostali dowiadują się nieco później.

![rudobrody krasnolud o cokolwiek wygolonej głowie](../grafiki/Gundin.png)


Odświętny strój: dostojny, lecz skromny. Kaftan, stójka, ciżemki (ale na płaskim obcasie).

## Valaya Drumbasdottir, córka Badoca

 - krasnoludka z Samotnej Góry, wobec której i Gundin i Öri żywią szacunek zabarwiony strachem.

 ![starsza krasnoludka o rudosiwych włosach](../grafiki/Valaya.jpg)

| Postawa         | ??                                                           |
| --------------- | ------------------------------------------------------------ |
| Wzrost / waga   | ??                                                           |
| Wiek            | najstarsza z kompanii?                                       |
| Włosy           | Rude włosy                                                   |
| Oczy            |                                                              |
| Rysy            |                                                              |
| Cera            | ??                                                           |
| Ubiór           | Dobry podróżny strój                                         |
|                 |                                                              |
|                 |                                                              |
| Zwraca uwagę:   | jest to krasnoludka!                                         |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
|                 |                                                              |
| Odświętny strój | błękitna suknia, barwiona krabami z Eryn Muil?, kolor nostalgiczny, przypominający weselną suknię Valayi |
|                 |                                                              |

## Herefra o zacnym lecz skrywanym nazwisku!

![złotowłosa nastolatka o jasnej cerze i niebieskich oczach](http://britishperioddramas.com/wp-content/uploads/2019/02/3524-800x445.jpg)

| Postawa       | waleczna, dobrze zbudowana, wysportowana                     |
| ------------- | ------------------------------------------------------------ |
| Wzrost / waga | coś ponad półtora rangara (165cm) - rangar to niemal metr (98cm) |
| Wiek          | trzynaście lat                                               |
| Włosy         | Długie, proste, złote włosy, lekko kręcone u dołu            |
| Oczy          | Niebieskie                                                   |
| Rysy          | Symetryczne rysy twarzy, wyraźne kości policzkowe, to będzie piękność, już to widać. |
| Cera          | jasna, gładka                                                |
| Ubiór         |                                                              |
|               |                                                              |
|               |                                                              |
| Zwraca uwagę: | żywotność, witalność, konik!                                 |
|               | złote włosy, uroda                                           |
|               | miecz: widać, że rodowy, z pokolenia na pokolenie przekazywany, rohański, Poławiacz Serc |
|               | i tarcza (zwykle zawinięta): za duża na nią! cała czarna, **Kamil na pomoc!** |
|               |                                                              |
|               |                                                              |
|               |                                                              |

