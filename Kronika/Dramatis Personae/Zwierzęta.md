# Zwierzęta:

1. Odo - ogar Brunhildy, teraz Öriego. Szlachetna i dostojna bestia, robiąca wrażenie na wielu. 
2. Arod - koń Herefry i jej ulubieniec, którego rozpieszcza przy każdej okazji.
3. Smyk - ogar Ernwara, poczciwy zwierz, może nie najbystrzejszy ale nieraz ratował sytuację.
4. Loki - mądry, nawet jak na swój gatunek, kruk. Towarzysz Valayi. Jest tajemnicą ile naprawdę potrafi.
