# Epilog

W którym te opowieści o Rudej Drużynie kończą się. 

## Historie i koncepty postaci

czyli ujawnianie tajemnic.

1. Gundin Bardinsson
2. Ori Oinsson
3. Valaya Badocsdottir
4. Herefra o tajemnym nazwisku :P
5. Myrtle Tuk
6. Ernwar z Beleriandów

## Stare dzieje

1. Dzielnica Dale budowana przez Valayę? Kruszony Kamień - dzielnica Ereboru budowana przez Valayę?
1. Co to za upiory były przy karawanie?
1. Rugnissonowie spaleni opodal Spacerowych Smajli - czy przez Gwathrana właśnie?
1. Jak Gondor reaguje na odnalezienie (i utratę) Elendilmira - korony Numenoryjskich królów?

## Inne, zebrane

1. Jak daleko rozeszły się pieśni?
1. Co można było jeszcze (i gdzie) w Morii znaleźć?
1. Balrogowy kąt Morii - jak by to szło jakbyśmy tam poszli?
1. Cofanie zegara - już odpowiedziane: dobry pomysł, zmniejszenie ekipy (ukrycie BNów), poza tym raczej nie
1. Zegar na 9 - co wtedy? Zależnie od miejsca: Księżycogryz, Balrog, coś jeszcze?
1. Kucie w Mitrylowej Kuźni jak by wyglądało? Czy była to pułapka czy szansa na oręż co miałby większe szanse w starciu z Księżycogryzem?

## Gwathran

1. Czy staruszek Vigol żyw? Czy norkę mu trzeba odbudować?
1. Jak obrona Puszczańskiego Dworu przed wrogami?
1. Rodzina do Rhosgobel dotarła? Bezpiecznie?
1. Arcirias - jaką miał misję?
1. Czy Saruman przypomina się z zaproszeniem? Jak?

## Co dalej?

1. Poszukiwania Rugnissonów, by im z ich klątwą dopomóc.
1. Co dalej z Kurtim?
1. Co dalej z przyjacielem Gundina co pierogi z Myrtle chciał lepić?

## Upadek Khazad-dum i losy Bardiego i Oriego

1. Co stało się z dziedzictwem? Ile go przechwycono a ile uratowano?
1. Oba topory zabieram (dziennik Bardiego): czyli...?
1. Po co orki fałszowały list Dortondy do hańby Bardiego wzywając krasnoludy?
1. Jak zginął Thrain, syn Naina? Bo przecież z Morii żywym nie wyszedł?
1. Jakie rozkazy pchnęły bezimiennego przodka Oriego do fałszerstwa rozkazu króla i królobójstwa?
1. Ile wiedział Oin, ojciec Oriego? Czemu nie uciekał przed smokiem?
